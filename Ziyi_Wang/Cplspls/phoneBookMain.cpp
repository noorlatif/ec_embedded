/* Embedded C++ assignment Phonebook
 * Practice object oriented programming by making a class and objects
 * Created by Ziyi Wang 2019-11-14
 */

#include <iostream>
using namespace std;

class PhoneBook{ //create a class
private: 
    /* create a structure for contact, and predefined array of 100 contact objects
     * as the attribute for PhoneBook. Trying to practice create a nested tructure.*/
    struct contact{
        string firstName;
        string lastName;
        unsigned long long phoneNumber;
    } myContacts[100];

public:
    //function add a new contact to a specific indexed place in phonebook
    void addContact(int i, string name1, string name2, unsigned long long number){
        struct contact newContact;
        newContact.firstName = name1;
        newContact.lastName = name2;
        newContact.phoneNumber = number;
        myContacts[i] = newContact;
    }

    /* //function addContacts() adds a contact object to the specified indexed place
       //used when struct contact separately defined out side of class definition
       //when a struct object can be created directly in main()
    void addContact(int i, struct contact newContact){
        myContacts[i] = newContact;
    }*/

    //desplay the ith contact in the phone book
    void printContact(int i){
        cout << myContacts[i].firstName << " " << myContacts[i].lastName << " ";
        cout << myContacts[i].phoneNumber << endl;
    }

    //display the first n contacts in the phone book
    void printPhoneBook(int n){
        for (int i = 0; i < n; i++){
            cout << myContacts[i].firstName << " " << myContacts[i].lastName << " ";
            cout << myContacts[i].phoneNumber << endl;
        }
    }
};

int main() {
    PhoneBook myPhoneBook;
    myPhoneBook.addContact(0, "Amy", "Cooper", 1255534569);

    /*struct contact tempNew;
      //if struct contact separately defined out side of class definition
	  //or as public, then a struct object can be created directly as following
    tempNew.firstName = "Amy";
    tempNew.lastName = "Cooper";
    tempNew.phoneNumber = 1255534569;
    myPhoneBook.addContact(0, tempNew);
    myPhoneBook.printContact(0);*/

    myPhoneBook.addContact(1, "Mike", "Brown", 1638270008);
    myPhoneBook.printContact(1);

    myPhoneBook.addContact(2, "Tom", "Michaels", 1234454479);
    myPhoneBook.printContact(2);

    myPhoneBook.addContact(2, "Lisa", "Michaels", 1234454479);

    myPhoneBook.printPhoneBook(3);

    return 0;
}
/* Embedded C++ assignment: Ohm's law and Joule's law
 * File: calcFunc.cpp
 * Summary: Definitions of functions for calcFunc.h
 * Created by Ziyi Wang 2019-11-16 */

#include <iostream>
#include "calcFunc.h"

void ohmslaw::setU(double u){
    this->u = u;
}

void ohmslaw::setI(double i){
    this->i = i;
}

void ohmslaw::setR(double r){
    this->r = r;
}

double ohmslaw::calcU(){
    return (i * r);
}

double ohmslaw::calcI(){
    return (u / r);
}

double ohmslaw::calcR(){
    return (u / i);
}

void jouleslaw::setU(double u){
    this->u = u;
}

void jouleslaw::setI(double i){
    this->i = i;
}

void jouleslaw::setP(double p){
    this->p = p;
}

double jouleslaw::calcU(){
    return (p / i);
}

double jouleslaw::calcI(){
    return (p / u);
}

double jouleslaw::calcP(){
    return (u * i);
}

/* Function used to safeguard that user input is valid. Here we are expecting user
 * selects one of the two options by entering a single char, either '1' or '2'.
 * When the input is valid, function returns true, otherwise false. */
bool checkSelect(char c){
    if(getchar() != '\n'){ //user input is more than one char, there is leftover on the buffer
        fflush(stdin);//clear up the buffer
        std::cout << "Error! Invalid selection. " << std::endl; //prompt error message
        return false;
    }
    if(c != '1' && c != '2'){ //unexpected input
        std::cout << "Error! Invalid selection. " << std::endl; //prompt error message
        return false;
    }else{ //valid input
        return true;
    }
}

/* Function used to safeguard that user input is valid. Here we are expecting user
 * enter a valid number as input. When the input is valid, function returns true,
 * and automatically update the value through reference, otherwise returns false. */
bool readValue(std::string str, double* ptr){
    int n = str.length();
    for(int i = 0; i < n; i++){
        if(!isdigit(str[i])){
            if(str[i] != '.'){
                std::cout << "Error! Invalid input." << std::endl;
                getchar();
                return false;
                break;
            }
        }else{
            getchar();
            *ptr = stod(str);
            return true;
        }
    }
}

/* Function used to safeguard that user input is valid. Here we are expecting user
 * enter one and only one 0 for the unknown parameter that will be calculated, and
 * providing the other two values for calculation. Any other scenario is invalid.
 * Function returns true when the criteria is fulfilled, otherwise returns false.
 * Function simultaneously updates the value for a selection flag through reference.*/
bool checkInput(double a, double b, double c, int *flag){
    if(a == 0 && b != 0 && c != 0){
        *flag = 1; //a is unknown, calculate a based on b and c
        return true;
    } else if(a != 0 && b == 0 && c != 0) {
        *flag = 2; //b is unknown, calculate b based on a and c
        return true;
    } else if(a !=0 && b != 0 && c == 0){
        *flag = 3; //c is unknown, calculate c based on a and b
        return true;
    } else{
        std::cout << "Error! Invalid input." << std::endl;
        std::cout << "ONE and ONLY ONE parameter MUST have value zero." << std::endl;
        return false;
    }
}
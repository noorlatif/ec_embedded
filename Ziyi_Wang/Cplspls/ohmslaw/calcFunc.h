/* Embedded C++ assignment: Ohm's law and Joule's law
 * File: calcFunc.h for ohmslawMain.cpp
 * Summary: two classes are created, ohmslaw and jouleslaw. Each class
 *          contains 3 private member variables and 6 member functions.
 *          File also contains declaration for other external functions
 *          that are called from ohmslawMain.cpp. Definitions and more
 *          detailed description are included in calcFunc.cpp.
 * Created by Ziyi Wang 2019-11-16 */

#ifndef OHMLAW_CALCFUNC_H
#define OHMLAW_CALCFUNC_H

class ohmslaw{ //creatd class ohmslaw
private: //declare private variables for class
    double u;
    double i;
    double r;
public: //declare public functions
    void setU(double u); //functions to set values
    void setI(double i);
    void setR(double r);
    double calcU(); //functions for calculation
    double calcI();
    double calcR();
};

class jouleslaw{ //create class jouleslaw
private: //declare private variables for class
    double u;
    double i;
    double p;
public: //declare public functions
    void setU(double u); //functions to set values
    void setI(double i);
    void setP(double p);
    double calcU(); //functions for calculations
    double calcI();
    double calcP();
};

bool checkSelect(char c); //function for safeguard user selection
bool readValue(std::string str, double* ptr); //function for safeguard user input
bool checkInput(double a, double b, double c, int *flag); //double check if input are viable for calculation

#endif //OHMLAW_CALCFUNC_H

/* Embedded C assignment: practicing function overloading
 * Create a calculator where the operation is done through separate overloaded
 * functions that take the corresponding data type for calculation.
 * Created by Ziyi Wang 2019-11-15*/

#include <iostream>
#include <string>
#include <iomanip>
#include "calFunc.h"
using namespace std;

int main() {
    //declaration of variables
    char operatSelect;
    char operatSymbol;
    string num1, num2;
    int num1i, num2i;
    double num1d, num2d;


    do {
        do {//operation selection menu
            cout << "Enter corresponding number to select operation" << endl;
            cout << "1. addition + " << endl;
            cout << "2. subtraction - " << endl;
            cout << "3. multiplication * " << endl;
            cout << "4. division / " << endl;
            cin >> operatSelect;
            //fflush(stdin);
        } while (!checkOperat(operatSelect));//loops until the valid input is achieved

        do { //prompt for input a number
            cout << "Enter the first operand: ";
            cin >> num1; //input is read as string
            fflush(stdin);
        } while (checkNumber(num1) == -1); //loops until the valid input is achieved

        //through checkNumber(), the proper type is determined and input is casted to that type
        if (checkNumber(num1) == 1) { //input is determined as an integer
            num1i = stoi(num1); //cast input from string to integer
            //cout << "your input is an integer: " << num1i << endl;
        } else if (checkNumber(num1) == 0) { //input is determined as containing deimal point
            num1d = stod(num1); //cast input from string to double
            //cout << "your input is an double: " << num1d << endl;
        }

        do { //prompt to input the second number
            cout << "Enter the second operand: ";
            cin >> num2; //input is read in as string
            fflush(stdin);
        } while (checkNumber(num2) == -1); //check if the input is valid

        if (checkNumber(num2) == 1) {
            num2i = stoi(num2);
            //cout << "your input is an integer: " << num2i << endl;
        } else if (checkNumber(num2) == 0) {
            num2d = stod(num2);
            //cout << "your input is a double: " << num2d << endl;
        }

        switch (operatSelect) { //switch according to user selection
            case '1': //when selection is '1'
                operatSymbol = '+'; //assign operatSymbol to user selects ie. addition
                cout << num1 << " " << operatSymbol << " " << num2 << " = ";
                if (checkNumber(num1) == 1 && checkNumber(num2) == 1) { //both input are int
                    cout << add(num1i, num2i) << endl;
                } else if (checkNumber(num1) == 1 && checkNumber(num2) == 0) { //one input int, the other double
                    cout << setprecision(num2.length()) << add(num1i, num2d) << endl;
                } else if (checkNumber(num1) == 0 && checkNumber(num2) == 1) { //one input double, the other int
                    cout << setprecision(num1.length()) << add(num1d, num2i) << endl;
                } else { //both input double
                    cout << setprecision(6) << add(num1d, num2d) << endl;
                }
                break;
            case '2': //when selection is '2'
                operatSymbol = '-'; //assign operatSymbol to minus
                cout << num1 << " " << operatSymbol << " " << num2 << " = ";
                if (checkNumber(num1) == 1 && checkNumber(num2) == 1) { //both input are int
                    cout << subtract(num1i, num2i) << endl;
                } else if (checkNumber(num1) == 1 && checkNumber(num2) == 0) { //one input int, the other double
                    cout << setprecision(num2.length()) << subtract(num1i, num2d) << endl;
                } else if (checkNumber(num1) == 0 && checkNumber(num2) == 1) { //one input double, the other int
                    cout << setprecision(num1.length()) << subtract(num1d, num2i) << endl;
                } else { //both input are double
                    cout << setprecision(6) << subtract(num1d, num2d) << endl;
                }
                break;
            case '3': //when selection is '3'
                operatSymbol = '*'; //assign operatSymbol to multiplication
                cout << num1 << " " << operatSymbol << " " << num2 << " = ";
                if (checkNumber(num1) == 1 && checkNumber(num2) == 1) { //both input are int
                    cout << multiply(num1i, num2i) << endl;
                } else if (checkNumber(num1) == 1 && checkNumber(num2) == 0) { //one input int, the other double
                    cout << setprecision(num2.length()) << multiply(num1i, num2d) << endl;
                } else if (checkNumber(num1) == 0 && checkNumber(num2) == 1) { //one input double, the other int
                    cout << setprecision(num1.length()) << multiply(num1d, num2i) << endl;
                } else { //both input are double
                    cout << setprecision(6) << multiply(num1d, num2d) << endl;
                }
                break;
            case '4': //when selection is '4'
                operatSymbol = '/'; //assign operatSymbol to division
                cout << num1 << " " << operatSymbol << " " << num2 << " = ";
                if (checkNumber(num1) == 1 && checkNumber(num2) == 1) { //both input are int
                    cout << setprecision(6) << divide(num1i, num2i) << endl;
                } else if (checkNumber(num1) == 1 && checkNumber(num2) == 0) { //one input int, the other double
                    cout << setprecision(6) << divide(num1i, num2d) <<endl;
                } else if (checkNumber(num1) == 0 && checkNumber(num2) == 1) { //one input double, the other int
                    cout << setprecision(6) << divide(num1d, num2i) << endl;
                } else {
                    cout << setprecision(6) << divide(num1d, num2d) << endl;
                } //both input are double
                break;
            default:
                cout << "Something wrong happened!" << endl;
        }
        cout <<"Press enter to continue. Any other key to exit. "<<endl;
    }while(getchar() == '\n'); //program loops until the user selects to exit
    return 0;
}
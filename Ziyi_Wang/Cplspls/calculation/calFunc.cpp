/* File: calFunc.cpp contains function definitions for calFunc.h
 * Created by Ziyi Wang on 2019-11-15. */

#include <iostream>
#include "calFunc.h"

/* Function checkNumber() checks if the input, saved as string, is valid.
 * User input is expected to be a valid number, either an integer or decimal point number.
 * Function checkNumber returns -1 when the input is invalid, 0 when the input contains
 * decimal point and 1 when the input is integer. */
int checkNumber(std::string str){
    int isint = 0;
    int n = str.length();
    for (int i = 0; i < n; i++){
        if(!isdigit(str[i])){
            if(str[i] != '.') {
                isint = -1; //invalid input when the input contains non-digit, non-decimal point char
                std::cout << "Error! Invalid input." << std::endl;
                break;
            }else { //input contains a '.' decimal point
                isint = 0; // return 0, which later will be assigned to type double
                break;
            }
        }
        isint = 1; //input contains only digits without decimal, determined as integer, return 1
    }
    return isint;
}

/* Function checkOperat() safeguards the input is valid and returns boolean
 * true or false that can be used for conditioning. */
bool checkOperat(char select){
    if(getchar() != '\n') { //input is more than one char as expected e.g. 2.5, abc, 33 ect.
        fflush(stdin);      //clear up the leftover on the buffer
        std::cout<<"Error! Invalid selection." <<std::endl; //prompt error message
        return false;
    }
    if(isdigit(select)){ //when the input is valid digit
        if (select != '1' && select != '2' && select != '3' && select != '4') { //invalid value e.g. input = '5'
            std::cout << "Error! Invalid selection." << std::endl; //prompt error message
            return false;
        } else {
            return true;
        }
    }else{ //is not a valid number e.g. input = 'a'
        std::cout << "Error! Invalid selection." << std::endl;
        return false;
    }
}


//overload function add() for various data type
int add(int a, int b){
    return a+b;
}
double add(int a, double b){
    return a+b;
}
double add(double a, int b){
    return a+b;
}
double add(double a, double b){
    return a+b;
}

//overload function subtract() for various data type
int subtract(int a, int b){
    return a-b;
}
double subtract(int a, double b){
    return a-b;
}
double subtract(double a, int b){
    return a-b;
}
double subtract(double a, double b){
    return a-b;
}

//overload function multiply() for various data type
int multiply(int a, int b){
    return a*b;
}
double multiply(int a, double b){
    return a*b;
}
double multiply(double a, int b){
    return a*b;
}
double multiply(double a, double b){
    return a*b;
}

//overload function divide() for various data type
double divide(int a, int b){
    return a/b;
}
double divide(int a, double b){
    return a/b;
}
double divide(double a, int b){
    return a/b;
}
double divide(double a, double b){
    return a/b;
}

/* File: .h file for calcMain.cpp
 * Created by Ziyi Wang on 2019-11-15. */

#ifndef CALCULATION_CALFUNC_H
#define CALCULATION_CALFUNC_H

#include <iostream>
//check functions to safeguard that user input is valid
int checkNumber(std::string str);
bool checkOperat(char select);

//overload function add() to accommodate various data type
int add(int a, int b);
double add(int a, double b);
double add(double a, int b);
double add(double a, double b);

//overload function add() to accommodate various data type
int subtract(int a, int b);
double subtract(int a, double b);
double subtract(double a, int b);
double subtract(double a, double b);

//overload function add() to accommodate various data type
int multiply(int a, int b);
double multiply(int a, double b);
double multiply(double a, int b);
double multiply(double a, double b);

//overload function add() to accommodate various data type
double divide(int a, int b);
double divide(int a, double b);
double divide(double a, int b);
double divide(double a, double b);

#endif //CALCULATION_CALFUNC_H

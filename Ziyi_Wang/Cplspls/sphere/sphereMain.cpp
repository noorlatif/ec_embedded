#include <iostream>
#include <cmath>
#include "sphere.h"
using namespace std;

int main() {
    Sphere sphere1; //create object sphere1 of type Sphere with default constructor
    sphere1.setRadius(2); //call public function setRadius to set radius to object sphere1
    cout << "For a sphere with radius of ";
    cout << sphere1.getRadius(); //call function getRadius() to get value of radius for sphere1
    cout << ", its volume is " <<sphere1.calcVolume() << "." << endl; //calcVolume() to get volume

    int r = -2;
    Sphere sphere2 (r); //create object sphere2 with parameterized constructor
    cout << "If we create another sphere and set its radius as " << r << "," << endl;
    cout << "the negative radius will be automatically converted to ";
    cout << sphere2.getRadius() << "." << endl;
    cout << "Thus negative radius would always return an area of " << sphere2.calcArea() << '.' << endl;

    sphere2.setRadius(3); //change the value of private property of sphere2 through setRadius()
    cout << "If we change the value to " << sphere2.getRadius() << ", then we can calculate that" <<endl;
    cout << "the area is " << sphere2.calcArea();
    cout << " and the volume is " << sphere2.calcVolume() << '.';

    return 0;
}
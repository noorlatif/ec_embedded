// File: sphere.h for sphereMain.cpp
// Created by Ziyi Wang on 2019-11-18.
//

#ifndef SPHERE_SPHERE_H
#define SPHERE_SPHERE_H


class Sphere {
private:
    int radius; //declare a private property

public:
    Sphere() {} //default empty constructor
    Sphere(int r); //parameterized constructor
    void setRadius(int r); //function to set radius
    int getRadius(); //function that gets radius
    double calcVolume(); //function that calculates volume
    double calcArea(); //function that calculates area
};

#endif //SPHERE_SPHERE_H

// File: function definition for sphere.h
// Created by Ziyi Wang on 2019-11-18.
//

#include <cmath>
#include "sphere.h"

Sphere::Sphere(int r){ //parameterized constructor
    if (r < 0){
        r = 0;
    }
    this->radius = r;
}

//function that sets radius to an sphere object
void Sphere::setRadius(int r){
    if (r < 0){
        r = 0;
    }
    this->radius = r;
}

//function that gets radius
int Sphere::getRadius(){
    return this->radius;
}

//function that calculates volume
double Sphere::calcVolume(){
    return 4/3 * M_PI * pow(this->radius, 3);
}

//function that calculates area
double Sphere::calcArea(){
    return 4 * M_PI * pow(this->radius, 2);
}

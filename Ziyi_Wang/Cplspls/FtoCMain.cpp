/* Embedded C++ week 4
 * Fahrenheit to Celsius Conversion
 * To practice with safeguard user input and prevent program crashing.
 * Created by Ziyi Wang 2019-11-25
 */

#include <iostream>

bool checkInput();
double convertFtoC(double f);

int main() {
    double inF;
    std::cout << "==== Fahrenheit - Celsius Converter ====" << std::endl;

    do {
        do {
            std::cout << "Enter Fahrenheit degree: " << std::endl;
            std::cin >> inF;
        } while (!checkInput()); //loop when the input is invalid

        std::cout << inF << " Fahrenheit is " << convertFtoC(inF) << " Celsius.\n";
        std::cout << "Press Enter to continue. Any other key to exit.";
    } while(getchar() == '\n'); //User chooses to continue or exit.
    return 0;
}

double convertFtoC(double f){ //function that carries out conversion
    return (f-32) * 5 / 9;
}

//Function that checks if user input is valid and prevents crashing
bool checkInput(){ //return true when input is valid
    if(std::cin.fail()) { //cin failed to read a double
        std::cin.clear(); //clear the error flag
        std::cin.ignore(INT_MAX, '\n'); //ignore the remaining on buffer
        std::cout << "Error! Invalid input.\n"; //prompt error message
        return false; //invalid input
    }
    else if(getchar()!= '\n'){ //cin successfully read an input but...
        //Here it is essential to check if there is any leftover data on the buffer.
        //Not only to safeguard input but also for the sake of program's robustness.
        //Just cin successfully reads in input does not necessarily mean that input
        //is valid. Inputs such as "32.5 rt", "42.3.543", "45tt" etc. can all result
        //in successful input but with invalid leftover on the buffer that would
        //subsequently cause problem down the line.
        //std::cin.clear(); //not needed as there is no error flag
        std::cin.ignore(INT_MAX, '\n'); //ignore leftover on buffer
        std::cout << "Error! Invalid input.\n";
        return false; //invalid input
    }
    else{
        return true; //valid input
    }
}
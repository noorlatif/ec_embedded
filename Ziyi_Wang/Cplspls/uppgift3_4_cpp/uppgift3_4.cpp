/* Uppgift 3.4 by Ziyi Wang 2019-11-06
 * Second to the last digit of Swedish personnummer indicates sex of the person.
 * It is odd number for male, and even number for female. This program asks user
 * input for sex and personnummer then determines if they confirms each other.*/

#include <iostream>
using namespace std;

int main() {
    int sex = 0;
    long long int personnummer = 0;
    int sexIndicator;
    cout << "Enter 1 for male, 0 for female: ";
    cin >> sex;
    cout << "Enter your personnummer in format yyyymmddxxxx: ";
    cin >> personnummer;
    sexIndicator = (personnummer/10)%2; //1 for male, 0 for female
    if(sex == 0 && sexIndicator == 0)
        cout << "Correct! You are female." << endl;
    else if(sex == 1 && sexIndicator == 1)
        cout <<"Correct! You are male." << endl;
    else
        cout << "Incorrect! Your personnummer does not match your sex." << endl;
    system("pause");
    return 0;
}
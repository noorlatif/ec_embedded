/* Uppgift multiplication table by Ziyi Wang 2019-11-06
 * Program writes out a multiplication table according to user input.*/

#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    int n = 0, a = 0;
    do{
        do {
            cout << "Enter an integer between 1 to 10 to display multiplication table. ";
            cin >> n;
            if (n <= 0 || n > 10)
                cout << "Error! Input out of range." << endl;
        }while(n <= 0 || n > 10);

        int table[n + 1][n + 1];//declare the 2D array
        //create a nxn multiplication table
        for (int j = 0; j <= n; j++)//for the first row
            table[0][j] = j;
        for (int i = 0; i <= n; i++)//for the first column
            table[i][0] = i;

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                table[i][j] = i * j;
            }
        }

        //display the multiplication table
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= n; j++) {
                if (j < n)
                    cout << setw(4) << table[i][j] << " ";
                else
                    cout << setw(4) << table[i][j] << endl;
            }
        }
        cout << "Press 1 to continue, any other key to exit. ";
        cin >> a;
    }while(a == 1);
    return 0;
}
#include <iostream>

class Parent{
private:
    std::string givenName; //private property won't be inherited

protected:
    std::string familyName = "Simpson"; //protected property can be inherited

public:
    //constructors cannot be inherited
    Parent(){}; //default empty constructor for parent
    Parent(std::string firstName){
        this->givenName = firstName;
    }; //parameterized constructor for parent

    void printFamilyName(){
        std::cout << familyName;
    }

    void setGivenName(std::string name){
        this->givenName = name;
    }

    void printGivenName(){
        std::cout << "Given name: " << givenName << std::endl;
    }

    void printFullName(){
        std::cout << "Name: " << givenName << " " << familyName <<std::endl;
    }
};

class Child: public Parent{ //sub-class child from parent class parent
private:
    int age; //private property specific for child
public:
    Child(){};//default constructor
    Child(std::string firstName, int age){//parameterized constructor for child
        Parent::setGivenName(firstName); //access to private property of parent class through public method
        this->age = age;
    };

    void setAge (int n) { //Child class specific method to set age
        this->age = n;
    }

    void printAge(){ //Child class specific method to print age
        std::cout << "Age: " << age << std::endl;
    }
};

int main() {
    Parent father("Holmer"); //create parent object with parameterized constructor
    Parent mother; //create empty parent object
    mother.setGivenName("Marge"); //set given name to mother object
    Child kid1; //create empty kid object
    kid1.setGivenName("Bart"); //set given name to kid through calling inherited function
    kid1.setAge(10); //set age to kid through child class specific function
    Child kid2 ("Lisa", 8); //create kid through kid class constructor
    Child kid3 ("Maggie", 1);

    father.printFamilyName(); //call parent class function to print family name
    std::cout << " family consists of following members." << std::endl;
    std::cout << "Father - ";
    father.printFullName();
    std::cout << "Mother - ";
    mother.printFullName();
    std::cout << "Children - " << std::endl;
    kid1.printGivenName();
    kid1.printAge();
    kid2.printGivenName();
    kid2.printAge();
    kid3.printGivenName();
    kid3.printAge();
    return 0;
}
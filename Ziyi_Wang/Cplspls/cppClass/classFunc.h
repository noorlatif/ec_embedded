/* Class declaration
 * Created by Ziyi Wang on 2019-11-13 */

#ifndef UNTITLED_CLASSEXC_H
#define UNTITLED_CLASSEXC_H
class Property {
private: //declare private attributes
    std::string type;
    int yearConstruction;
    double totalSqrmArea;
    double totalMillionPrice;

public:
    //declare and define a default constructor
    Property(){
        type = "type";
        yearConstruction = 0;
        totalSqrmArea = 0.0;
        totalMillionPrice = 0.0;
    }
    //declare a parameterized constructor
    Property(std::string name, int year, double area, double price);

    //declare function members
    double getUnitPrice();
    void printInfo();
    void setType(std::string name);
    std::string getType();
    void setYear(int year);
    int getYear();
    void setArea(double area);
    double getArea();
    void setPrice(double price);
    double getPrice();
};
#endif //UNTITLED_CLASSEXC_H
/* Definitions for functions for cppClassMain.cpp classFunc.h
 * Created by Ziyi Wang on 2019-11-13. */

#include <iostream>
#include "classFunc.h"

//Definition for the parameterized constructor
Property::Property(std::string name, int year, double area, double price) {
    type = name;
    yearConstruction = year;
    totalSqrmArea = area;
    totalMillionPrice = price;
}

//Definition for getUnitPrice()
double Property::getUnitPrice(){
    return totalMillionPrice * 1e6 / totalSqrmArea;
}

//Definition for function printInfo()
void Property::printInfo(){
    std::cout << "Type of property: " << type << std::endl;
    std::cout << "Construction year: " << yearConstruction << std::endl;
    std::cout << "Total area (sqr meter): " << totalSqrmArea << std::endl;
    std::cout << "Total price (million SEK): " << totalMillionPrice << std::endl;
}

//Definition for set/get functions
void Property::setType(std::string name){
    type = name;
}
std::string Property::getType(){
    return type;
}

void Property::setYear(int year){
    yearConstruction = year;
}
int Property::getYear() {
    return yearConstruction;
}

void Property::setArea(double area){
    totalSqrmArea = area;
}
double Property::getArea() {
    return totalSqrmArea;
}

void Property::setPrice(double price){
    totalMillionPrice = price;
}
double Property::getPrice() {
    return totalMillionPrice;
}
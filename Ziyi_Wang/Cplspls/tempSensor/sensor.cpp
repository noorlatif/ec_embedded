/* File: sensor.cpp
 * Definitions for member functions of class TempSensor in sensor.h
 * Created by Ziyi Wang on 2019-11-18. */

#include "sensor.h"

int TempSensor::getBatteryConsumption(){
    return this->batteryConsumption;
}

void TempSensor::setBatteryConsumption(int bc){
    this->batteryConsumption = bc;
}

void TempSensor::printModel(){ //Model of the sensor is read-only
    std::cout << "Model: " << this->model << std::endl;
}

void TempSensor::increaseConsumption(){//increase consumption by 2
    this->batteryConsumption += 2;
}


void TempSensor::decreaseConsumption(){//decrease consumption by 2
    if(this->batteryConsumption < 2){
        this->batteryConsumption = 0; //to minimum 0
    }else{
        this->batteryConsumption -= 2;
    }
}

void TempSensor::printSensor() { //print info for sensor
    std::cout << "Sensor ID: " << this->sensorID << std::endl;
    std::cout << "Battery consumption: " << this->batteryConsumption << std::endl;
    std::cout << "Model: " << this->model << std::endl;
}
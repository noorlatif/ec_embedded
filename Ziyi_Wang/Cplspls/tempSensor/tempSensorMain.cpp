/* File: tempSensorMain.cpp
 * Summary: practicing creating class TempSensor
 *          creating objects and manipulating their properties
 *          through member functions of the class
 * Created by Ziyi Wang 2019-11-18 */

#include <iostream>
#include "sensor.h"

int main() {
    TempSensor mySensor1;
    std::cout << "This is the first sensor object created."<< std::endl;
    mySensor1.printSensor();
    mySensor1.setBatteryConsumption(12);
    mySensor1.decreaseConsumption();
    std::cout << "\nAfter setting battery consumption to 12 " << std::endl;
    std::cout << "then calling decreaseConsumption() function," << std::endl;
    std::cout << "now the sensor has specifications..." << std::endl;
    mySensor1.printSensor();

    TempSensor mySensor2;
    mySensor2.setBatteryConsumption(1);
    mySensor2.decreaseConsumption();
    std::cout << "\nHere a second sensor has been create." << std::endl;
    std::cout << "Its battery consumption is initialized as 1 and then decreased by 2." << std::endl;
    std::cout << "Here are sensor2's specifications..." << std::endl;
    mySensor2.printSensor();

    TempSensor mySensor3;
    std::cout << "\nNow that a third sensor has been created." << std::endl;
    std::cout << "Its battery consumption is " << mySensor3.getBatteryConsumption() << std::endl;
    for(int i = 0; i < 10; i++){
        mySensor3.increaseConsumption();
    }
    std::cout << "After looping through increaseConsumption() function," << std::endl;
    std::cout << "now the specifications for the 3rd sensor are ..." << std::endl;
    mySensor3.printSensor();

    TempSensor mySensor4;
    std::cout << "\nThis program provides no mean to set or change the model of the sensor." << std::endl;
    std::cout << "So now the fourth sensor has been created. But its model is still ";
    mySensor4.printModel();
    mySensor4.setBatteryConsumption(5);
    std::cout << "Here are the specifications of sensor4 ..." << std::endl;
    mySensor4.printSensor();


    std::cout << "\nYou reach the end of the program." << std::endl;
    return 0;
}
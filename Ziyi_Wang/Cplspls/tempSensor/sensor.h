/* File: sensor.h for tempSensorMain.cpp
 * Definitions of the member functions are included in sensor.cpp
 * Created by Ziyi Wang on 2019-11-18. */

#ifndef TEMPSENSOR_SENSOR_H
#define TEMPSENSOR_SENSOR_H

#include <iostream>
class TempSensor { //create class TempSensor
private:
    //declare private variables for attributes
    int sensorID;
    int batteryConsumption;
    std::string model;

public:
    TempSensor(){ //default constructor
        static int tracker = 1001; //create a tracker for sensorID
        this->sensorID = tracker;
        this->batteryConsumption = 0;
        this->model = "myFavModel";
        tracker++;
    }

    //declaration of member functions
    int getBatteryConsumption();
    void setBatteryConsumption(int b);
    void printModel();
    void increaseConsumption(); //increase consumption by 2
    void decreaseConsumption(); //decrease consumption by 2
    void printSensor();
};

#endif //TEMPSENSOR_SENSOR_H

/* File: obj3D.h for calcGeomMain.cpp
 * Summary: this file contains the header file for class definitions
 *          Base class: Obj3D
 *          Derived classes: Cube, Sphere, and Cone respectively
 * Created by Ziyi Wang 2019-11-20 */

#ifndef CALCGEOM_OBJ3D_H
#define CALCGEOM_OBJ3D_H

class Obj3D { //base class
private:
    //declaration of attributes
    int objID;
    double radius;
    double height;
    double edge;
    static int objCount;

protected:
    //inherited member functions that show polymorphism in derived classes
    double calcVolume();
    double calcArea();

public:
    Obj3D() = default; //trivial default constructor
    void setObjID();
    void displayObjID();
    void setEdge(double d);
    double getEdge();
    void setRadius(double r);
    double getRadius();
    void setHeight(double h);
    double getHeight();
    static void displayObjCount(); //static function can be accessed without object
};


class Cube : public Obj3D{ //derived class Cube
private:
    //declaration of private attributes for Cube class to track objects
    int cubeID;
    static int cubeCount;
public:
    void setCubeID();
    void displayCubeID();
    static void displayCubeCount();
    double calcVolume();//inherited polymorphic function
    double calcArea();//inherited polymorphic function
};

class Sphere : public Obj3D{
private:
    //declaration of private attributes for Sphere class to track objects
    int sphereID;
    static int sphereCount;
public:
    void setSphereID();
    void displaySphereID();
    static void displaySphereCount();
    double calcVolume();//inherited polymorphic function
    double calcArea();//inherited polymorphic function
};

class Cone : public Obj3D{
private:
    //declaration of private attributes for Cone class to track objects
    int coneID;
    static int coneCount;
public:
    void setConeID();
    void displayConeID();
    static void displayConeCount();
    double calcVolume();//inherited polymorphic function
    double calcArea();//inherited polymorphic function
};

#endif //CALCGEOM_OBJ3D_H

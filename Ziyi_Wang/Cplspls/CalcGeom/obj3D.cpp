/* File: obj3D.cpp for calcGeomMain.cpp obj3D.h
 * Summary: This file contains the function definitions in Obj3D.h
 * Created by Ziyi Wang 2019-11-20 */

#include <iostream>
#include <cmath>
#include "obj3D.h"

void Obj3D::setObjID() { //assign ID to Obj3D object
    objID = ++objCount;
}

void Obj3D::displayObjID(){ //display ID of Obj3D object
    std::cout << "Object ID - " << objID << std::endl;
}

void Obj3D::setEdge(double d){ //to set length for edge
    this->edge = d;
}

double Obj3D::getEdge(){ //to get length of edge
    //std::cout << "Edge length: " << edge << std::endl;
    return edge;
}

void Obj3D::setRadius(double r){ //to set radius
    this->radius = r;
}

double Obj3D::getRadius(){ //to get radius
    //std::cout << "Radius: " << edge << std::endl;
    return radius;
}

void Obj3D::setHeight(double h){ //to set height
    this->height = h;
}

double Obj3D::getHeight(){ //to get height
    //std::cout << "Height: " << edge << std::endl;
    return height;
}

void Obj3D::displayObjCount() { //this is a static function to track number of objects
    std::cout << "You have created " << objCount << " 3D objects." << std::endl;
}

void Cube::setCubeID() { //set ID number to Cube object
    cubeID = ++cubeCount;
}

void Cube::displayCubeID(){ //to display ID number to Cube object
    std::cout << "Cube ID - " << cubeID << std::endl;
}

void Cube::displayCubeCount(){ //this is a static function to track number of Cube objects
    std::cout << "You have created " << cubeCount << " cubes." << std::endl;
}

double Cube::calcVolume() {//inherited polymorphic function that calculates volume of a cube
    double d = getEdge();
    return d*d*d;
}

double Cube::calcArea() {//inherited polymorphic function that calculates surface area of a cube
    double d = getEdge();
    return d*d*6;
}

void Sphere::setSphereID(){ //to set ID to Sphere object
    sphereID = ++sphereCount;
}

void Sphere::displaySphereID(){ //display ID of a Sphere object
    std::cout << "Sphere ID - " << sphereID << std::endl;
}

void Sphere::displaySphereCount(){ //static function to track number of Sphere objects
    std::cout << "You have created " << sphereCount << " spheres." << std::endl;
}

double Sphere::calcVolume(){//inherited polymorphic function that calculates volume of sphere
    double r = getRadius();
    return 4.0/3.0 * M_PI * r * r * r;
}

double Sphere::calcArea(){//inherited polymorphic function that calculates surface area of sphere
    double r = getRadius();
    return 4.0 * M_PI * r * r;
}

void Cone::setConeID(){ //set ID to Cone object
    coneID = ++coneCount;
}

void Cone::displayConeID(){ //display ID to Cone object
    std::cout << "Cone ID - " << coneID << std::endl;
}

void Cone::displayConeCount(){ //static function to track number of Cone objects
    std::cout << "You have created " << coneCount << " cones." << std::endl;
}

double Cone::calcVolume(){ //inherited polymorphic function that calculates volume of cone
    double r = getRadius();
    double h = getHeight();
    return 1.0 / 3.0 * M_PI * (r * r) * h;
}

double Cone::calcArea(){//inherited polymorphic function that calculates area of cone
    double r = getRadius();
    double h = getHeight();
    return M_PI * r * (r + sqrt(r*r + h*h));
}
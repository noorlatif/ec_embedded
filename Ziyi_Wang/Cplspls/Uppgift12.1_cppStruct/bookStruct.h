// structure type book
// Created by Ziyi Wang on 2019-11-11.
//

#ifndef BOOKSTRUCT_BOOKSTRUCT_H
#define BOOKSTRUCT_BOOKSTRUCT_H

//declare a structure type called book and define it
//with four data members of various basic types
struct book{
    std::string title;
    std::string author;
    int totalPage;
    double price;
};
#endif //BOOKSTRUCT_BOOKSTRUCT_H


//Embedded C++ redo uppgift12.1 C från början by Ziyi Wang 2019-11-12
#include <iostream>
#include "bookStruct.h"

using namespace std;

//Function that displays information of an object of book type
void printBook(struct book bookName){//structure passed as argument
    cout << bookName.title << " by " << bookName.author << " Total page: "
         << bookName.totalPage << " Price: $" << bookName.price << endl;
}

int main(){
    struct book bookA; //declare an object bookA of type book
    bookA.title = "Stories You've Never Heard Before";//define data member title of object bookA
    bookA.author = "Mysterious Witch";//define data member author of object bookA
    bookA.totalPage = 328;//define data member totalPrice of object bookA
    bookA.price = 14.89;//define data member price of object bookA

    //declare and define object bookB of type book
    struct book bookB = {"Master The Magic", "Wizard Power", 524, 74.95};
    printBook(bookA);//call function printBook to display information about object bookA
    printBook(bookB);//call function printBook to display information about object bookB
    return 0;
}
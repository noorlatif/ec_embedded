/*Embedded C excercise 6.2 by Ziyi Wang
Functions that calculate moms and total price 
based on user input of moms % and price w/o moms.*/

#include <stdio.h>
#include <windows.h>

//Function momsCalc() calculates moms based on moms% and price excluding moms
double momsCalc(double prisExkMoms, double momsProcent){
	return prisExkMoms * momsProcent/100;
}

//Function prisCalc() calculates total price with moms
double prisCalc(double prisExkMoms, double momsProcent){
	return prisExkMoms * (1 + momsProcent/100);
}

//Function printKvitto() prints out receipt
void printKvitto(double momsProcent, double netto, double moms, double total){
	printf("%5s %6s %6s %6s\n", "Moms%", "Netto", "Moms", "Total");
	printf("%5.0f %6.2f %6.2f %6.2f\n", momsProcent, netto, moms, total);
}

int main(){
	SetConsoleOutputCP(1252);
	double prisExkMoms = 0;
	double momsProcent = 0;
	double moms = 0;
	double totalpris = 0;
	int n = 0;
	while(1){
		printf("Price without moms: ");
		scanf("%lf", &prisExkMoms);
		printf("Procentage of moms: ");
		scanf("%lf", &momsProcent);
		moms = momsCalc(prisExkMoms, momsProcent);
		totalpris = prisCalc(prisExkMoms, momsProcent);
		printKvitto(momsProcent, prisExkMoms, moms, totalpris);
		printf("Press 1 to continue, 0 to end session: ");
		scanf("%d", &n);
		if(n == 0)
			break;
	}
	return 0;
}

/*Print SOS in morse (diod blinking simulator) by Ziyi Wang

How morse works?
Morse consists of following five elements.
1. dot (or short mark) represented by "." lasts one time unit long.
2. dash (or long mark) represented by "-" lasts three time units long.
3. inter-element gap: one time unit, between the dots and dashes within a character
4. short gap: three time units, between letters
5. medium gap: seven time units, between words
In this program, I arbitrarily define one single time unit as 0.25 second.
*/

#include <stdio.h>
#include <time.h>

void delay(int milliseconds);//declare function delay()

int main()
{
	int i = 0, n = 0;//create index counter i and n for user input
	int dot = 0, dash = 0;//define dot and dash

	printf("Give how many times you want to send the SOS signal: ");
	scanf("%d", &n);

	while(i < n){
		//print three consecutive dots that code for "S"
		for(dot = 0; dot < 3; dot++){
			printf(". ");
			delay(250);//dot lasts one time unit
		}

		delay(750);//short gap between letters

		//print three consecutive dashes that code for "O"
		for(dash = 0; dash < 3; dash++){
			printf("- ");
			delay(750);//dash lasts three time units
		}

		delay(750);//short gap between letters

		//print three consecutive dots that code for "S"
		for(dot = 0; dot < 3; dot++){
			printf(". ");
			delay(250);//dot lasts one time unit
		}
		putchar('\n');
		i++;

		delay(1750);//medium gap between words
	}
	return(0);
}

//function delay() pauses the execution for a defined short period of time
void delay(int milliseconds)
{
	long pause;
	clock_t now,then;

	pause = milliseconds*(CLOCKS_PER_SEC/1000);
	now = then = clock();
	while( (now-then) < pause )
		now = clock();
}
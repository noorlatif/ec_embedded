/*Embedded C week 3 homework 2DArray by Ziyi Wang
file func.h*/
//Function that calculates the mean of one dimensional array of integers
double calMean(int arr[], int k);
//Function that finds the median among an array of integers
double findMedian(int arr[], int k);
//Function needed as argument for qsort() 
int comp(const void *a, const void *b);
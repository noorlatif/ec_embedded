#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "automobil.h"

void printAuto(struct automobil *car){
	printf("%s %s %d ", car->brand, car->model, car->year);
	if(car->manual)
		printf("manual\n");
	else
		printf("automatic\n");
}

int main(){
	struct automobil new;
	struct automobil car1 = {"Volvo", "XC60", 2016, 1};
	struct automobil car2 = {"Tesla", "Model X", 2019, 0};
	new = car2;
	strcpy(new.model, "Model Y");
	printAuto(&car1);
	printAuto(&new);
	return 0;
}
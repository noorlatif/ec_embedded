/* File: main.cpp together with el.cpp, manyval.cpp, el.h, menyval.h.
 Summary: a program that calculates with electricity
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-28
 -----------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include "menyval.h"
#include "el.h"

using namespace std;

int main() {
    //to use � � �
    system("chcp 1252");
    system("cls");

    bool run = true;
    //while-loop who runs until the user enter 9 in the menu (run = false)
    while (run) {
        cout << "\nV�lj vilka storheter du vill ber�kna:\n"
             << "V�lj 1 f�r: OHMS LAG\n"
             << "V�lj 2 f�r: RTOT\n"
             << "V�lj 3 f�r: EFFEKTLAGEN ENKEL\n"
             << "V�lj 4 f�r: SKENBAR EFFEKT ENFAS\n"
             << "V�lj 5 f�r: AKTIV EFFEKT/MEDELEFFEKT ENFAS\n"
             << "V�lj 6 f�r: SKENBAR EFFEKT 3-FAS\n"
             << "V�lj 7 f�r: AKTIV EFFEKT 3-FAS\n"
             << "V�lj 8 f�r: PRIS F�R F�RBRUKAD EL\n"
             << "V�lj 9 f�r: F�R ATT AVSLUTA\n";
        int val = 0;
        val = valet();

        switch (val) {
            case 1:
                menyVal1();
                break;
            case 2:
                menyVal2();
                break;
            case 3:
                menyVal3();
                break;
            case 4:
                menyVal4();
                break;
            case 5:
                menyVal5();
                break;
            case 6:
                menyVal6();
                break;
            case 7:
                menyVal7();
                break;
            case 8:
                menyVal8();
                break;
            case 9:
                run = false;
                break;
            default:
                cout << "\nfel, v�lj en siffra fr�n menyn (1-9)\n";
                break;
        }
    }
    return 0;
}
//
// Created by Elin on 2019-11-28.
//

#include <iostream>
#include <iomanip>
#include "el.h"

using namespace std;

//if the user enters something other than numbers in the menu you end up here.
int valet(){
    int valet = 0;
    cout << "\nv�lj nummer fr�n menyn (1-9)";
    //if the user enters more than one number, only the first one is used,
    // the others are ignored
    cin >> setw(1) >> valet;

    //if cin fails
    while (cin.fail()){
        cin.clear();
        cin.ignore(INT_MAX, '\n');
        cout << "\nfel, v�lj ett nummer ur menyn (1-8).\n";
        cout << "v�lj nummret p� alternativet fr�n menyn som du vill anv�nda: \n";
        cin >> setw(1) >> valet;
    }
    //clear stream
    cin.clear();
    cin.ignore(INT_MAX, '\n');

    return valet;
}
//case 1 in the menu
void menyVal1(){

    cout << "Ohms lag sp�nningen(volt/V) bet�ckning U lika med"
            "Resistansen(Ohm) bet�ckning R \n"
         << "g�nger Str�mmen(Ampere) med bet�ckningen I. Kort U=R*I. \n\n";
    double resistans = 0, strom = 0;
    cout << "Skriv resistans R < 20 000ohm: \n ";
    cin >> resistans;
    while(resistans > 20000)
    {
        varning();
        cout << "Skriv resistans R < 20 000ohm: \n ";
        cin >> resistans;
    }

    cout << "Skriv str�m I < 440 Ampere: \n";
    cin >> strom;
    while(strom > 440)
    {
        varning();
        cout << "Skriv str�m I < 440 Ampere: \n";
        cin >> strom;
    }
    cout << "Svar: " << multipliceraVarden(resistans, strom) << "V\n";
}
//case 2 in the menu
void menyVal2(){
    cout << "Resistans sammankopplade i parallella kretsar �r lika med 1"
            " delat Resistans R total �r lika med\n"
         << "Resistans 1/R1 + 1/R2 + 1/R3 d� vi h�gst anv�nder tre resistanser.\n\n";
    double resistans1 = 0, resistans2 = 0, resistans3 = 0;
    cout << "Skriv in resistansv�rdet 1 (R < 20 000ohm): \n ";
    cin >> resistans1;
    cout << "Skriv in resistansv�rdet 2 (R < 20 000ohm): \n ";
    cin >> resistans2;
    cout << "Skriv in resistansv�rdet 3 (R < 20 000ohm): \n ";
    cin >> resistans3;

    while(resistans1 > 20000 || resistans2 > 20000 || resistans3 > 20000) {
        varning();
        cout << "Skriv in resistansv�rdet 1 (R < 20 000ohm): \n ";
        cin >> resistans1;
        cout << "Skriv in resistansv�rdet 2 (R < 20 000ohm): \n ";
        cin >> resistans2;
        cout << "Skriv in resistansv�rdet 3 (R < 20 000ohm): \n ";
        cin >> resistans3;
    }
    cout << "Svar: " << res_tot(resistans1, resistans2, resistans3) << "Ohm\n";
}
//case 3 in the menu
void menyVal3(){
    cout << "Effektlagen enkel f�r likstr�m �r effekten P i Watt (W)"
            "lika med sp�nningen U i volt(V)\n"
         << "g�nger str�mmen I i Ampere(A): \n\n";
    double spanning = 0, strom2 = 0;
    cout << "Skriv sp�nnngen U i volt(V): \n ";
    cin >> spanning;

    cout << "Skriv str�m Ampere I < 440A: \n";
    cin >> strom2;
    while(strom2 > 440) {
        varning();
        cout << "Skriv str�m Ampere I < 440A: \n";
        cin >> strom2;
    }
    cout << "Svar: " << multipliceraVarden(spanning, strom2) << "W\n";
}
//case 4 in the menu
void menyVal4(){
    cout << "Skenbar effekt enfas r�knas med storheten VA(VoltAmpere)"
            "som �r lika med sp�nningen U i volt(V)\n";
    cout << "g�nger str�mmen I i ampere(A)\n\n";
    double spanning2 = 0, strom3 = 0;
    cout << "Skriv Sp�nningen U i volt: \n ";
    cin >> spanning2;

    cout << "Skriv str�m I < 440A: \n";
    cin >> strom3;
    while(strom3 > 440) {
        varning();
        cout << "Skriv str�m I < 440A: \n";
        cin >> strom3;
    }
    cout << "Svar: " << multipliceraVarden(spanning2, strom3) << "VA\n";
}
//case 5 in the menu
void menyVal5(){
    cout << "Aktiv medelefdekt enfas �r lika med effekt P"
            "i watt(W) lika med sp�nningen U i volt(V) g�nger str�mmen I \n"
         << "i Ampere g�nger cosinus fi/efkektfaktor < 1:\n\n";
    double spanning3 = 0, strom4 = 0, cos = 0;
    cout << "Skriv sp�nning U i volt: \n ";
    cin >> spanning3;

    cout << "Skriv str�m I: \n";
    cin >> strom4;
    while(strom4 > 440) {
        varning();
        cout << "Skriv str�m I: \n";
        cin >> strom4;
    }

    cout << "Skriv in effektfaktorn cos > 0 && cos < 1:\n";
    cin >> cos;
    while(cos < 0 && cos > 1) {
        cout << "Fel v�rde, f�rs�k igen\n";
        cout << "Skriv in effektfaktorn cos > 0 && cos < 1:\n";
        cin >> cos;
    }
    cout << "Svar: " << aktiv_eff(spanning3, strom4, cos) << "W\n";
}
//case 6 in the menu
void menyVal6(){
    cout << "3-fas skenbar effekt �r v�xelsp�nning �r skenbar effekt S"
            "i voltampere(VA) lika med sp�nningen U i volt(V) \n"
         << "g�nger str�mmen I i ampere(A) g�nger roten ur 3 SQRT(3).\n\n";
    double spanning4 = 0, strom5 = 0;
    cout << "Skriv sp�nning U i volt(V) < 400V: \n ";
    cin >> spanning4;
    while(spanning4 > 400) {
        varning();
        cout << "Skriv sp�nning U i volt(V) < 400V: \n ";
        cin >> spanning4;
    }

    cout << "Skriv str�m I i ampere < 440: \n";
    cin >> strom5;
    while(strom5 > 440) {
        varning();
        cout << "Skriv str�m I i ampere < 440: \n";
        cin >> strom5;
    }
    cout << "Svar: " << sken_3fas(spanning4, strom5) << "VA\n";
}
//case 7 in the menu
void menyVal7(){
    cout << "3-fas aktiv effekt �r effekten P i Watt(W) lika med"
            "sp�nningen U i volt(V) g�nger str�mmen I i ampere(A)\n"
         << "g�nger cos < 1 && cos > 0 g�nger roten ur 3 SQRT(3).\n\n";
    double spanning5 = 0, strom6 = 0, cos2 = 0;
    cout << "Skriv Sp�nningen U i volt(V): \n ";
    cin >> spanning5;
    while(spanning5 > 400) {
        varning();
        cout << "Skriv Sp�nningen U i volt(V): \n ";
        cin >> spanning5;
    }

    cout << "Skriv str�m I i ampere(A): \n";
    cin >> strom6;
    while(strom6 > 440) {
        varning();
        cout << "Skriv str�m I i ampere(A): \n";
        cin >> strom6;
    }

    cout << "Skriv in effektfaktorn cos > 0 && cos < 1: \n";
    cin >> cos2;
    while(cos2 < 0 && cos2 > 1) {
        varning();
        cout << "Skriv in effektfaktorn cos > 0 && cos < 1: \n";
        cin >> cos2;
    }
    cout << "Svar: " << aktiv_3fas(spanning5, strom6, cos2) << "W\n";
}
//case 8 in the menu
void menyVal8(){
    cout << "pris f�r f�rburkad el r�knas ut genom kw-pris * f�rbrukad kw.\n\n";
    double pris = 0, forbrukning = 0;
    cout << "ange kw-priset: \n";
    cin >> pris;
    cout << "ange den f�rbrukade kw: \n";
    cin >> forbrukning;

    cout << "Svar: " << multipliceraVarden(pris, forbrukning) << ":-\n";
}
/* File: shape.cpp
 Summary: a program that creates a text file where the user can 
    choose whether to read or add text to the file.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-28
 -----------------------------------------------------------
 */
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//function to read the file
void readfile(){
    string text;
    // Read from the text file
    ifstream readFile("filename.txt");
    // Use a while loop together with the getline() function to read the file line by line
    if(readFile.is_open()){
        cout << "\nThe contents of the file: \n\n";
        while (getline(readFile, text)) {
            // Output the text from the file
            cout << text;
        }
        // Close the file
        readFile.close();
    }
    else{
        //if something went wrong when opening the file you end up here.
        cout << "\nError, could not open the file\n";
    }
}

//function to writ in the file
void addToFile(){
    //ignore the answer from menu before the file opens and the user can write in it
    cin.ignore();
    fstream addFile;
    //open the file for the user to add text.
    addFile.open("filename.txt", addFile.app);

    if (addFile.is_open()){
        //if the file opens you end up here
        cout << "\nopen file succeed\n";
        cout << "\nwrite what you want to add to your file: ";
        string add;
        getline(cin, add);
        addFile << "\n" << add << "\n";
        addFile.close();
    }
    else{
        //if something went wrong when opening the file you end up here.
        cout << "\nError, could not open the file\n";
    }
}

int main() {
    //create a text file
    ofstream file("filename.txt");
    //add a text to the file.
    file << "Hello! this is a file \n";
    file.close();

    bool run = true;
    //while-loop who runs the menu until the user press 3 (quit program = false)
    while (run){
        cout << "\t------Menu------"
                "\nWhat do you want to do with the file?\n"
                "alternative [1] for read the file\n"
                "alternative [2] for add to file\n"
                "alternative [3] for quit the program\n"
                "choose alternative number(1-3): ";
        int alternative;
        cin >> alternative;

        //if cin fails
        while (cin.fail()){
            cin.clear();
            cin.ignore(INT_MAX, '\n');
            cout << "\nWrong, you must select a number from the menu (1-3).\n";
            cout << "Enter the number of the option from the menu you want to use: ";
            cin >> alternative;
        }
        //switch with 3 cases.
        switch (alternative){
            case 1:
                system ("CLS");
                readfile();
                cout << "\n\n";
                break;
            case 2:
                system ("CLS");
                addToFile();
                break;
            case 3:
                system ("CLS");
                run = false;
                break;
            default:
                cout << "\nwrong, enter a number from the menu, 1-3 \n\n";
        }
    }
    return 0;
}
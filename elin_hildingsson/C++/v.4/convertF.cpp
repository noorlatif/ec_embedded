/*File: celsius.c
 Summary: This program converts fahrenheit to celsius and continues 
    until the user types n/N = no on the follow up question.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-25
 -----------------------------------------------------------
 */
#include <iostream>

using namespace std;

//class for calculate calsius.
class Convert
{
private:
    //variable.
    double celsius = 0;
public:
    //constructors.
    Convert() = default;
    Convert(double celsius)
    {
        this->celsius = celsius;
    }

    //methods.
    double convertToCelsius(double fahrenheit)
    {
        this->celsius = (fahrenheit - 32) * 5/9;
        return this->celsius;
    }
    void getCelsius()
    {
        cout << "The temperature in celsius: " << this->celsius << "\n\n";
    }
};
    //main function, where the program starts.
    int main()
    {
        //one object to convert class.
        Convert object;
        //variables to work with.
        string answer = "";
        double fahrenheit = 0;

        bool run = true;
        //loop that continues to the user enter n or N.
        while(run) {
            cout << "\nEnter the day temperature in Fahrenheit: ";
            cin >> fahrenheit;
            //if the user enters something other than numbers, you end up here.
            while (cin.fail()) {
                cin.clear();
                cin.ignore(INT_MAX, '\n');
                cout << "Wrong, you must enter the temperature with numbers.\n";
                cout << "\nenter the temperature in fahrenheit whit numbers: ";
                cin >> fahrenheit;
            }
            //puts the value for fahrenheit to the convert-class to calculate with.
            object.convertToCelsius(fahrenheit);
            object.getCelsius();

            cout << "Do you want to try again? (yes = y / no = n): ";
            cin >> answer;

            if (answer == "y" || answer == "Y")
            {
                continue;
            }
            else if (answer == "n" || answer == "N")
            {
                cout << "Bye.\n";
                run = false;
            }
            else
                {   
                //if the user enter somthing other than y/Y or n/N, you endup here and the loop continues until
                //the user enter y/Y or n/N.
                    do {
                        cout << "something went wrong, Enter Y for yes or N for no.";
                        cout << "\n\nDo you want to try again? (yes = y / no = n): ";
                        cin >> answer;
                        if(answer == "n" || answer == "N")
                        {
                            cout << "Bye.\n";
                            return 0;
                        }
                    } while (answer != "y" && answer != "Y" && answer != "n" && answer != "N");
                }

        }
        return 0;
    }

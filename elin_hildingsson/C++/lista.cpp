/* File: lista.cpp
 Summary: program who prints out the size and the contents 
    of a list namned list.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-24
 -----------------------------------------------------------
 */

#include <iostream>
#include <list>

	using namespace std;
	int main()
	{
	    //new list named list.
		list<int> list = {1, 2, 3, 4, 5};
		
		//adds 6 last in the list
		list.push_back(6);
		//adds 74 first in the list
		list.push_front(74);

		cout << "\nsize of the list: " << list.size() << "\n";
		
		cout << "The contents of the list:";
		for (auto it = list.begin(); it != list.end(); ++it) 
        cout << ' ' << *it; 
		return 0;
	}
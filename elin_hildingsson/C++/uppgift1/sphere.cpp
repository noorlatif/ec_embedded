//Owner: Elin Hildingsson
//Date: 2019-11-18

//to use PI
#include <cmath>
#include "sphere.h"

	using namespace std;

		//calculate the area of the sphere.
		double Sphere::getArea()
		{
			//M_PI = pi (3,1415...)
			//pow = radius^2
			area = 4 * M_PI * pow(this->radius, 2);
			return area;
		}
		//calculate the volume og the sphere.
		double Sphere::getVolume()
		{
			//M_PI = pi (3,1415...)
			//pow = radius^3
			volume = 4 * M_PI * pow(this->radius, 3) / 3;
			return volume;
		}

//Owner: Elin Hildingsson
//Date: 2019-11-18

//class named sphere
	class Sphere
	{
	private:
		int radius; 
		double volume;
		double area;
	public:
		//Default Constructor, Sphere. 
		Sphere() {}

		//parameterized Constructor, sphere. 
		Sphere(int radius2)
		{
			if (radius2 < 0)
			{
				radius = 0;
			}
			//this = pointer for objects. Pointer to radius.
			this->radius = radius2;

		}
		void setRadius(int radius2)
		{
			if (radius2 < 0)
			{
				radius2 = 0;
			}
			this->radius = radius2;
		}
		int getRadius()
		{
			return this->radius;
		}
		double getArea();
		double getVolume();
	};
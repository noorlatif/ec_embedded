/* File: ohm.cpp
 Summary: program to calculate ohm and power with.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-16
 -----------------------------------------------------------
 */
#include <iostream>
#include <cstdlib>

	using namespace std;

    //class for calculate ohm.
	class CalculateOhm
	{
	public:
		double sum, x, y;
		double getResistance(double x, double y)
		{
			sum = x / y;
			return sum;
		}
		double getVoltage(double x, double y)
		{
			sum = x * y;
			return sum;
		}
		double getCurrent(double x, double y)
		{
			sum = x / y;
			return sum;
		} 
	};
    
    //class to calculate power
	class CalculatePower
	{
	public:
		double sum, x, y;
		double getPower(double x, double y)
		{
			sum = x * y;
			return sum;
		}
	};

	int main()
	{
		CalculateOhm calc;
		CalculatePower calc2;

		double resistance, current, voltage;
		bool run = true;
        
        //while-loop who runs the menu until the user press 5 (quit program = false)
		while(run)
		{
			cout << "\nWhat do you want to calculate?\n"
				"alternative [1] for resistance (ohm)\n"
				"alternative [2] for voltage (V)\n"
				"alternative [3] for current (A)\n"
				"alternative [4] for power (W)\n"
				"alternative [5] quit the program\n"
				"choose alternative number(1 - 5): ";
			int alternative;
			cin >> alternative;
		
            //switch with 5 cases.
			switch (alternative)
			{
				case 1:
					system ("CLS");
					cout << "\nEnter your voltage (V): ";
					cin >> voltage;
					cout << "Enter your current (A): ";
					cin >> current;

					cout << "\nThe resistance are: " << calc.getResistance(voltage, current) << "Ohm\n\n";
					break;
				case 2:
					system ("CLS");
					cout << "\nEnter your current (A): ";
					cin >> current;
					cout << "Enter your resistance (ohm): ";
					cin >> resistance;

					cout << "\nThe voltage are: " << calc.getVoltage(current, resistance) << "V\n\n";
					break;
				case 3:
					system ("CLS");
					cout << "\nEnter your voltage (V): ";
					cin >> voltage;
					cout << "Enter your resistance (ohm): ";
					cin >> resistance;

					cout << "\nThe current are: " << calc.getCurrent(voltage, resistance) << "A\n\n";
					break;
				case 4:
					system ("CLS");
					cout << "\nEnter your voltage (V): ";
					cin >> voltage;
					cout << "Enter your current (A): ";
					cin >> current;

					cout << "\nThe power are: " << calc2.getPower(voltage, current) << "W\n\n";
					break;
				case 5:
					run = false;
					break;
				//if the user enter somthing other than nr 1-5 the program goes here 
				//and prints out the text below.
				default:
					system ("CLS");
					cout << "\nwrong, enter a number 1-5 \n\n";
			}
		}
		return 0;
	}



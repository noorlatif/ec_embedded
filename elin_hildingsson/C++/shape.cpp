/* File: shape.cpp
 Summary: a program with a main class where the other classes inherit from the 
 	main class and can calculate the area and volume of different figures.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-24
 -----------------------------------------------------------
 */
	#include <iostream>
	#include <cmath>
	#include <string>
	
	using namespace std;

	//class shape = main class
	 class Shape
	 {
	 public:
	 	//variables
	 	string name;
	 	string colour;
	 	
	 	//constructor
	 	Shape() {}
	 	double getArea() { return 0; }
	 	double getVolume() { return 0; }  
	 };

	 //class sphere = "child" to shape class.
	 class Sphere : public Shape
	 {
	 public:
	 	//variable
	 	double radius;

	 	//constructors
	 	Sphere() {}
	 	Sphere(double radius, string name, string colour)
	 	{
	 		this->radius = radius;
	 		this->name = name;
	 		this->colour = colour;
	 	}

	 	//methods
	 	double calculateArea()
	 	{
	 		double area;
	 		area = 4 * M_PI * pow(radius, 2);
			return area;
	 	}
	 	double calculateVolume()
	 	{
	 		double volume;
	 		volume = 4 * M_PI * pow(radius, 3) / 3;
			return volume;
	 	}
	 };

	 //class box = "child" to shape class.
	 class Box : public Shape
	 {
	 public:
	 	//variables
	 	double height;
	 	double length;
	 	double width;

	 	//constructors
	 	Box() {}
	 	Box(double height, double length, double width, string name, string colour)
	 	{
	 		this->height = height;
	 		this->length = length;
	 		this->width = width;
	 		this->name = name;
	 		this->colour = colour;
	 	}

	 	//methods
	 	double calculateArea()
	 	{
	 		double sum = 2 * pow(width, 2) * pow(length, 2) * pow(height, 2);
	 		return sum;
	 	}
	 	double calculateVolume()
	 	{
	 		double sum = height * length * width;
	 		return sum;
	 	}
	 };

	//the program starts here.
	int main()
	{
		//object sphere1 with values.
		Sphere sphere1(5, "Sphere", "green");
		double area1 = sphere1.calculateArea();
		double volume1 = sphere1.calculateVolume();
		cout << "\nName: " << sphere1.name
			 << "\nColour: " << sphere1.colour 
			 <<	"\nArea: " << area1
			 << "\nVolume: " << volume1 << "\n";

		//object box1 with values.
		Box box1(2, 2, 2, "Box", "blue");
		double area2 = box1.calculateArea();
		double volume2 = box1.calculateVolume();
		cout << "\nName: " << box1.name
			 << "\nColour: " << box1.colour 
			 << "\nArea: " << area2
			 << "\nVolume: " << volume2 << "\n"; 
		return 0;
	}
//owner: Elin Hildingsson
//Date: 2019-11-25

//the main class from which the other classes inherit

#ifndef SHAPE_H
#define SHAPE_H

#include <string>
	
	using namespace std;

	 class Shape
	 {
	 public:
	 //variables
	 	string name;
	 	string colour;
	 	
	 	//constructor
	 	Shape() = default;
	 	
	 	//methods
	 	double getArea() { return 0; }
	 	double getVolume() { return 0; }  
	 };

	 #endif
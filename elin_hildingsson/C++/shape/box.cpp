//Owner: Elin Hildingsson
//Date: 2019-11-25

//includes the main-class (shap.h) and the "child" class (box.h)
#include "box.h"
#include "shape.h"
#include <cmath>

using namespace std;

        //methods in the box-class
	 	double Box::calculateArea()
	 	{
	 		double sum1 = 2 * pow(this->width, 2) * pow(this->length, 2) * pow(this->height, 2);
	 		return sum1;
	 	}
	 	double Box::calculateVolume()
	 	{
	 		double sum2 = this->height * this->length * this->width;
	 		return sum2;
	 	}
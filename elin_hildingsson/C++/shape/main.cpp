/* File: main.cpp together with shape.cpp, box.cpp and shape.cpp.
 Summary: a program with a main class where the other classes inherit from the 
 	main class and can calculate the area and volume of different figures.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-25
 -----------------------------------------------------------
 */
	#include <iostream>
	#include <string>
	#include "box.h"
	#include "sphere.h"
	

	using namespace std;

    //the program starts here.
	int main()
	{
	    //object sphere1 with values.
		Sphere sphere1(5, "Sphere", "green");
		double area1 = sphere1.calculateArea();
		double volume1 = sphere1.calculateVolume();
		cout << "\nName: " << sphere1.name
			 << "\nColour: " << sphere1.colour 
			 <<	"\nArea: " << area1
			 << "\nVolume: " << volume1 << "\n";

        //object box1 with values.
		Box box1(2, 2, 2, "Box", "blue");
		double area2 = box1.calculateArea();
		double volume2 = box1.calculateVolume();
		cout << "\nName: " << box1.name
			 << "\nColour: " << box1.colour 
			 << "\nArea: " << area2
			 << "\nVolume: " << volume2 << "\n"; 
		return 0;
	}
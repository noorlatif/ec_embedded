//Owner: Elin Hildingsson
//Date: 2019-11-25

//includes the main-class (shap.h) to inherit from
#include "shape.h"
#include <string>

using namespace std;

 class Sphere : public Shape
	 {
	 public:
	 	//variable
	 	double radius;

	 	//constructors
	 	Sphere() {}
	 	Sphere(double radius, string name, string colour)
	 	{
	 		this->radius = radius;
	 		this->name = name;
	 		this->colour = colour;
	 	}
	 	//methods
	    double calculateArea();
	    double calculateVolume();
	 };
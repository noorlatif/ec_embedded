/* File: ohm.cpp
 Summary: program with constructor that prints out 2 persons name and age.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-18
 -----------------------------------------------------------
 */
#include <iostream>
#include <string>

using namespace std;

//class for the person info.
class Person
{
private:
	string name;
	int age;
public:
	//Default Constructor with the same name as the class.
	Person() {}

	//parameterized Constructor, Person. 
	Person(string name2, int age2)
	{
		name = name2;
		age = age2;
	}
	void PersonInformation()
	{
		//print out the persons name and age
		cout << "\nPersons name: " << name 
			 << ".\nPersons age: " << age 
			 << ".\n";
	}
};

//main, here the program starts.
int main()
{
	//define object. 
	Person person1("Olle", 18);
	//call the method PersonInformation and print out the first
	//persons name and age.
	person1.PersonInformation();
	
	//define object.
	Person person2("Jenny", 45);
	//call the method PersonInformation and print out the second
	//persons name and age.
	person2.PersonInformation();
	
	return 0;
}
/*File: uppgift12.1.cpp
 Summary: the program uses structure "book" to print out information about 
 	different books (book 1 and 2)
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-11
 -----------------------------------------------------------
 */
#include <iostream>
#include <string>

using namespace std;
	
//structure with headings for the structure book
struct book
{
	string title;
	string author;
	int pages;
	double price;
};

int main()
{
	//defines and initiates b1 and b2
	struct book b1 = {"Sagan om ringen", "J.J.R Tolkien", 483, 70};
	struct book b2 = {"Eldvittnet", "Lars Kepler", 562, 50};

	//prints out all information about book 1 (b1) and 2 (b2).
	cout << "\n\tFirst book:\n\ttitle: "<< b1.title << "\n\tauthor: " << b1.author << 
			"\n\tpages: "<< b1.pages << "\n\tprice: "<< b1.price << ":-\n";
	cout << "\n\tSecond book:\n\ttitle: "<< b2.title << "\n\tauthor: " << b2.author << 
			"\n\tpages: "<< b2.pages << "\n\tprice: "<< b2.price << ":-\n";
	return 0;
}

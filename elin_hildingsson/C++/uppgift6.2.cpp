/*File: uppgift6.2.cpp
 Summary: if you enter your social security number and gender the program
 	checks if your gender matches your social security number.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-06
 -----------------------------------------------------------
 */
#include <iostream>

using namespace std;
//a function that calculates the price with taxes.
double tax(double price)
{
	return price * 1.25;
}

//main function, the program starts here.
int main()
{
	cout << "\twhat's the price (whitout the tax)? ";
	double price;
	cin >> price;

	cout << "\n\tIf the price is: " << price << "\n\tPrice including tax will be: " 
		<< tax(price) << "\n\tTax is +25%.\n";
	return 0;
}
#include <stdio.h>
#include "calculate.h"

//function which calculate mean
double mean(int school[])
{
	double summa = 0;

	for (int i = 0; i < 10; ++i)
	{
		summa += school[i];
	}
	return summa /= 10;
}

//function which calculate median
double median(int school[])
{
	 double median = 0;
	 //e = elements in the array
     int e = 10; 
    
    // if number of elements are even
    if(e%2 == 0)
    {
        median = (school[(e-1)/2] + school[e/2])/2.0;
    }
    // if number of elements are odd
    else
    {
        median = school[e/2];
    }
    return median;
}
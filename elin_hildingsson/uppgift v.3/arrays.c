#include <stdio.h>
#include "calculate.h"

//main function were the program starts
int main()
{
	//arrays of results for the schools to calculate mean and median with.
	int class1[] = {15, 16, 18, 20, 20, 20, 21, 21, 22, 24};
	int class2[] = {11, 16, 18, 18, 18, 20, 20, 21, 21, 24};

	//prints the mean and the median for the different schools. 
	printf("\n\tMean    	 class 1: %.2f		class 2: %.2f", mean(class1), mean(class2));
	printf("\n\tMeadian 	 class 1: %.2f		class 2: %.2f\n", median(class1), median(class2));
	return 0;
}


/*==================================================================================================

	//2d array with the classes test results. first row = class 1, second row = class 2
	int result[2][10] = { {15, 16, 18, 20, 20, 20, 21, 21, 22, 24}, //<-- class 1
			      {11, 16, 18, 18, 18, 20, 20, 21, 21, 24} }; //<-- class 2

	//variabels to calculate mean with
	double sum1 = 0;
	double sum2 = 0;

	//for-loop to calculate mean for class 1
	for (int i = 0; i < 1; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			sum1 += result[i][j];
		}
		sum1 /=10; //10 = elements in the array
	}
	//for-loop to calculate mean for class 2
	for (int i = 1; i == 1 ; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			sum2 += result[i][j];		
		}
		sum2 /=10;
	}
	
	//variabels to calculate median with
	double median1 = 0;
	double median2 = 0;
	int element = 10;
    
    //for-loop to calculate median for class 1
    for (int i = 0; i < 1; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			if(element%2 == 0)
			{
				median1 = (result[i][(element-1)/2] + result[i][element/2])/2.0;
			}
			else
    		{
        		median1 = result[i][element/2];
    		}
		}
	}
	
	//for-loop to calculate median for class 2
	for (int i = 1; i == 1; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			if(element%2 == 0)
			{
				median2 = (result[i][(element-1)/2] + result[i][element/2])/2.0;
			}
			else
    		{
        		median2 = result[i][element/2];
    		}
		}
	}
	//prints mean and median for both classes
	printf("Mean 		class 1: %.2f 		class 2: %.2f\n", sum1, sum2);
	printf("Median 		class 1: %.2f 		class 2: %.2f\n", median1, median2);*/





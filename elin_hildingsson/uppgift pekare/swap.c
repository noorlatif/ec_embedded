#include "swap.h"

//function that swap the pointers value with each other
void swapLocation(int *a, int *b)
{
    int location = *a;
    *a = *b;
    *b = location;
    return;
}
#include <stdio.h>
#include "swap.h"

int main()
{
    //variables to work with
    int value1 = 10;
    int value2 = 100;
    int *pointer1 = &value1;
    int *pointer2 = &value2;

    //prints the pointers value.
    printf("%d\t%d\n", value1, value2);

    //getss the new swapt value for the pointers and print it out.
    swapLocation(&value1, &value2);
    printf("%d\t%d\n", value1, value2);

}
#include <stdio.h>

//a function that calculates the price with taxes.
double tax(double price)
{
	return price * 1.25;
}

//main function, the program starts here.
int main()
{
	printf("\twhat's the price (whitout the tax)? ");
	double price;
	scanf("%lf", &price);

	printf("\n\tIf the price is: %.2f \n\tPrice including tax will be: %.2f \n\tTax is +25%%.\n", price, tax(price));
	return 0;
}
#include <stdio.h>	

int main ()
{
	//kör talen 1-12
	for (int i = 1; i <= 12; ++i)
	{
		//skriver ut talet samt talet i kvadrat och i kubik.
		printf("tal: %2d, i kvadrat = %4d m^2 och i kubik = %5d m^3\n" ,i ,i*i, i*i*i);
	}
}
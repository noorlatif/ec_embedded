#include <iostream>

using namespace std;

int main()
{
    long long int personNum;
    int lastDig, remDig, gender;
    cout << "Please enter your gender(1 for male, 2 for female): ";
    cin >> gender;
    cout << "Now enter your personal number with all 12 numbers: ";
    cin >> personNum;
    remDig = personNum / 10; //removes the last digit
    lastDig = remDig % 10;   //returns the last number which was the next last number before.

    if (lastDig % 2 == 0)
    {
        if (gender == 1)
        {
            cout << "The number is Even and you're a male so you entered the wrong number" << endl;
        }
        else
        {
            cout << "Correct number since it matches your gender!" << endl;
        }
    }
    else
    {
        if (gender == 1)
        {
            cout << "Correct number since it matches your gender" << endl;
        }
        else
        {
            cout << "The number you've entered are odd, and since you're a female its wrong." << endl;
        }
    }
}
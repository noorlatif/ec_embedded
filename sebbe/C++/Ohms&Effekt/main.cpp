// =====================================
// AUTHOR       : Sebastian Mossberg
// CREATE DATE  : 2019-11-15
// PURPOSE      : To apply Ohm's law formula and Effect's law.
// SPECIAL NOTES: None.
// ===============================
// Change History: None to date.
//
//======================================

#include <iostream>
using namespace std;
#include "powerclasses.h"

float iVal, rVal, vVal, pVal;

int main()
{
    cout << "=================" << endl;
    cout << "Ohm's Law formula\n";

    cout << "Type the values you have, if you don't know the value input 0 and i will get you that value." << endl;
    cout << endl;
    
    cout << endl << "Enter the Current (I) - ström(ampere): ";
    cin >> iVal;
    cout << endl << "Enter the Ressistance (R) -  resistans(ohm): ";
    cin >> rVal;
    cout << endl << "Enter the Voltage (V) - spänning(volt): ";
    cin >> vVal;

    //ohmslaw into object missingValue.
    ohmslaw ohms;
    //set the values according to function setValues
    ohms.setValues(iVal, rVal, vVal);

    //effectlaw into object missingP;
    effectlaw effect;
    //set the values
    effect.setValues(iVal, rVal, vVal);

    if(iVal == 0 && rVal > 0 && vVal > 0) //IF Current (I) - ström(ampere) = 0 and rest is more THEN 0
    {
        // i = v / r;
        cout << endl;
        ohms.missingI(vVal, rVal);
        cout << endl;
        effect.effectMissingI(vVal, rVal);
        cout << endl;
        system("pause");
    }
    else if (rVal == 0 && iVal > 0 && vVal > 0) //Ressistance (R) -  resistans(ohm) = 0 and rest is more THEN 0
    {
        // r = v / i;
        cout << endl;
        ohms.missingR(iVal, vVal);
        cout << endl;
        effect.effectMissingR(iVal, vVal);
        cout << endl;
        system("pause");
    }
    else if(vVal == 0 && iVal > 0 && rVal > 0) //Voltage (V) - spänning(volt) = 0 and rest is more THEN 0
    {
        // v = i * r;
        cout << endl;
        ohms.missingV(rVal, iVal);
        cout << endl;
        effect.effectMissingV(rVal, iVal);
        cout << endl;
        system("pause");
    } else {
        cout << "[ERROR]: Wrong Format Please Try Again! \nEnter at least 2 values greater then 0." << endl;
    }
    cout << "Thank you for using this..." << endl;
    system("pause");
    system("cls");
    return 0;
}
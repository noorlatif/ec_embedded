// =====================================
// SEE MAIN.cpp HEADER COMMENTS
// =====================================

#include <iostream>
#include "powerclasses.h"
using namespace std;

void ohmslaw::setValues(float paramI, float paramR, float paramV)
{
    i = paramI;
    r = paramR;
    v = paramV;
}//use like this ohmslaw.setValues(variable i, variable r, variable v)

void ohmslaw::missingI(float v, float r)
{
    i = v / r;
    cout << "Result: " << i << " Ampere";
}

void ohmslaw::missingR(float i, float v)
{
    r = v / i;
    cout << "Result: " << r << " Ohm";
}

void ohmslaw::missingV(float r, float i)
{
    v = i * r;
    cout << "Result: " << v << " Volt";
}

//Ohms Law Above
//===========================================
//Effect Law below

void effectlaw::setValues(float i, float r, float v)
{
    i = i;
    r = r;
    v = v;
}

void effectlaw::effectMissingV(float i, float r)
{
    p = (i*i) * r;
    cout << endl << "You need: " << p << " Watt";
}

void effectlaw::effectMissingR(float v, float i)
{
    p = v*i;
    cout << endl << "You need: " << p << " Watt";
}

void effectlaw::effectMissingI(float v, float r)
{
    p = (v*v) / r;
    cout << endl << "You need: " << p << " Watt";
}
/*
 * Created on Wed Nov 20 2019
 *
 * Copyright (c) 2019 Sebastian Mossberg
 */
#include "shape.h"
#include "sphere.h"
#define _USE_MATH_DEFINES
//for some reason i need this to be able to make M_PI work when including cmath
#include <cmath>

double Sphere::volumeOfSphere()
{
    double volume = 4 * M_PI * pow(radius, 3) / 3;
    return volume;
}
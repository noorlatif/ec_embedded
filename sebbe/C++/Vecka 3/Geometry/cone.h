/*
 * Created on Wed Nov 20 2019
 * Copyright (c) 2019 Sebastian Mossberg
 * Github @ Lemorz56
 */
#include "shape.h"
class Cone : public Shape
{
public:
    double volumeOfCone();
};

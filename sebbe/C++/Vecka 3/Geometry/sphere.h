/*
 * Created on Wed Nov 20 2019
 *
 * Copyright (c) 2019 Sebastian Mossberg
 */

#include "shape.h"

class Sphere : public Shape
{
public:
    double volumeOfSphere();
};
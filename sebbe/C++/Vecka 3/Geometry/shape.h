/*
 * Created on Wed Nov 20 2019
 *
 * Copyright (c) 2019 Sebastian Mossberg
 */
#ifndef SHAPE_H
#define SHAPE_H

class Shape
{
public:
    double height;
    double radius;
    Shape() = default;

    void setHeight(double height){
        this->height = height;
    }
    void setRadius(double radius){
        this->radius = radius;
    }
    
};


#endif //SHAPE_H

#include <iostream>
#include <string>
#include "Sensor.h"

int main()
{
    //Create new object sensor1 and give it 30 as batteryConsumption and model name
    //Rest of the functions explain themselves in here.
    TemperatureSensor sensor1(30, "Tempus325b");
    sensor1.printInfo();
    cout << endl;
    sensor1.increaseConsumption();
    sensor1.printInfo();
    cout << endl;
    sensor1.reduceConsumption();
    sensor1.printInfo();
    cout << endl;
    sensor1.increaseConsumption();
    sensor1.increaseConsumption();
    sensor1.increaseConsumption();
    sensor1.increaseConsumption();
    sensor1.printInfo();
    cout << sensor1.printProtectedGetModelFunction();

    cout << endl << "================" << endl;
    ////Create new object sensor2 and give it 10 as batteryConsumption and model name
    TemperatureSensor sensor2(10, "Temp975c");
    sensor2.printInfo();
    sensor2.increaseConsumption();
    sensor2.printInfo();
    sensor2.reduceConsumption();
    sensor2.printInfo();
    sensor2.increaseConsumption();
    sensor2.printInfo();
    cout << sensor2.printProtectedGetModelFunction();
    return 0;
}
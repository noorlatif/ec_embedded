#ifndef SENSOR_H
#define SENSOR_H
using namespace std;
#include <iostream>

class TemperatureSensor
{
private:
    //creating some empty variables
    static int sensorID;
    int batteryConsum;
    string modelName;
    //Creating a private method to get the model name.
    string getModel()
    {
        return this->modelName;
    }

public:
    //A method to print out (and be able to reach) the private method above.
    string printProtectedGetModelFunction()
    {
        return getModel();
    }
    //Default constructor
    TemperatureSensor()
    {
        this->modelName = "Favoritmodell";
        this->batteryConsum = 0;
        sensorID++;
    }
    //Parameter Constructor to be able to quickly create objects and enter values right away.
    TemperatureSensor(int batteryConsum, string modelName)
    {
        this->batteryConsum = batteryConsum;
        this->modelName = modelName;
        sensorID++;
        //to use this and create, type for ex
            /*
            First: batteryConsumption arg1, then Model name as arg2.
            TemperatureSensor temp1(4, "Tempus325");
            TemperatureSensor temp2(5, "Tempus425b");
            */
    }

    //simple method that checks if consumtion is at 0 and if not reduce it by 2.
    void reduceConsumption()
    {
        if(batteryConsum == 0)
        {
            this->batteryConsum = 0;
        }else
        {
            this->batteryConsum -= 2;
            cout << "Reducing Battery By: 2" << endl;
        } 
    }
    //simple method to increase consumption by 2.
    void increaseConsumption()
    {
        this->batteryConsum += 2;
        cout << "Increasing Battery By: 2" << endl;
    }
    //Printing all the information.
    void printInfo()
    {
        cout << "The Model Name: " << this->modelName << endl;
        cout << "This Sensor Has The Id: " << sensorID << endl;
        cout << "The Battery Rate It is Consuming Is: " << this->batteryConsum << endl;
    }
};

#endif // SENSOR_H
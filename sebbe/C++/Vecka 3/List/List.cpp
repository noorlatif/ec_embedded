#include <iostream>
#include <list>
using namespace std;

int main()
{
    list<double> l = {1, 2, 3, 4, 5};
    l.push_back(6);
    //unlike vectors you can push front in list
    l.push_front(10);

    //you can pop front or back aswell in list
    //l.pop_back();
    //l.pop_front();

    cout << l.front() << endl;
    cout << l.back() << endl;
    cout << l.size() << endl;

    for (list<double>::iterator i = l.begin(); i != l.end(); i++)
    {
        cout << *i << endl;
    }

    return 0;
}
// #ifndef UNTITLED26_SPHERE_H
// #define UNTITLED26_SPHERE_H

class Sphere
{
private:
    int radius;
public: 
    Sphere() {}
    
    Sphere(int radius)
    {
        if(radius < 0) 
        {
            radius = 0;
        }
        this->radius = radius;
    }

    void SetRadius(int radius)
    {
        if (radius < 0)
        {
            radius = 0;
        }
        this->radius = radius;
    }

    int GetRadius(int radius);
    // int GetRadius()
    // {
    //     return this->radius;
    // }

    double CalculateVolume();
    double CalculateArea();
};
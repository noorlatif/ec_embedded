#include <iostream>
#include <cmath>
#include "Sphere.h"
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

double Sphere::CalculateVolume() 
{
    double volume;

    volume = 4 * M_PI * pow(this->radius, 3) / 3;

    return volume;
}

double Sphere::CalculateArea()
{
    double area;

    area = 4 * M_PI * this->radius*this->radius;

    return area;
}

//
int Sphere::GetRadius(int radius)
{
    return this->radius;
}
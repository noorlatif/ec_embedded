#include <iostream>
#include "Sphere.h"

using namespace std;

int main()
{
    Sphere newSphere;
    newSphere.SetRadius(2);
    double volume = newSphere.CalculateVolume();
    double area = newSphere.CalculateArea();
    cout << "Volume is " << volume << " Area is " << area << endl;

    Sphere newSphere2;
    newSphere2.SetRadius(5);
    volume = newSphere2.CalculateVolume();
    area = newSphere2.CalculateArea();

    cout << "Volume is " << volume << " Area Is: " << area << endl;
}
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int main() 
{
    printf("Do you want to turn on the power? Type 1 for Yes or 2 for No.\n");
    int answer;
    scanf("%d", &answer);

    if(answer == 1){
        system("cls");
        printf("Starting power");
        Sleep(1000);
        printf(".");
        Sleep(1000);
        printf(".");
        Sleep(1000);
        printf(".");
        Sleep(1000);
        printf(".");
        Sleep(1000);
        printf(".");
        Sleep(2000);
        system("cls");
        printf("\nPower: ON\n");
        Sleep(500);
        system("cls");
        Sleep(500);
        printf("\nPower: ON\n");
        Sleep(500);
        printf("Press CTRL + C to close.");
        Sleep(3000);
        system("cls");
        do{
            printf("- - - ");
            Sleep(500);
            printf("_ _ _  ");
            Sleep(500);
            printf("- - - \n");
            Sleep(500);
            system("cls");
        } while (1);
        
    }else{
        return 0;        
    }
    return 0;
}
//-----------------------
//|     Created by:     |
//|  Sebastian Mossberg |
//|     2019-10-04      |
//|                     |
//-----------------------

#include <stdio.h>

double taxPerc(double tax) //created a function called taxPerc where we count how much percentage of tax is in the price
{
    return (tax/100)+1;
    //For exapmle above if we would let tax be 25 it would divide 25 / 100 which = 0.25 
    //and then make it + 1 = 1.25 which we
    // Later multiply with the price to get the price WITH tax.
}

int main(void)
{
        double tax;
        double price;
        /* Here we declared the variables we will store the input from below inside
        then we will send TAX to the function above. */
        printf("What is the price for the product?(in SEK) ");
        scanf("%lf", &price);
        printf("Now please enter what tax rate should be applied (in %%, without the %% sign!): ");
        scanf("%lf", &tax);
        /* Here we have saved the information in TWO variables called TAX and PRICE. Down below we display the TAX, PRICE and TOTAL. */
        printf("The tax rate you've entered is: %.2f%%\nThe price without taxes is: %.2f SEK\nAnd the total of those two are: %.2f SEK", tax, price, price*taxPerc(tax));
        printf("\n");


    return 0;
}
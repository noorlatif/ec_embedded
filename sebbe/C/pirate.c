#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "pirate.h"

//LEFT printf & other print stuff still commented for future debugging.
char kons[] = "bcdfghjklmnpqrstuvwxz";
char word[100];
FILE *svenskafil;
FILE *rovarsprak;
char transword[100];

void piratetranslator()
{
    svenskafil = fopen("svenska.txt", "r"); //filen innehåler förskrivna rader, inte komplicerat att bytta ut mot en variabel som åker in i filen om man hellre vill det.
    rovarsprak = fopen("rovarsprak.txt", "w");

    printf("Starting to translate now.\n");
    char lines[100];
    //Store string in array lines, maximum 100, read/take from svenskafil defined above.
    //AS long as its not NULL.
    while (fgets(lines, 100, svenskafil) != NULL)
    {
        //printf("%s", lines);
        /*  Pirate translating stuff below  */
        for (int i = 0; lines[i] != '\0'; i++)
        {
            //putchar(lines[i]);
            fputc(lines[i], rovarsprak);
            char c = tolower(lines[i]);
            if (strchr(kons, c) != NULL)
            {
                //putchar('o');
                fputc('o', rovarsprak);
                //putchar(c);
                fputc(c, rovarsprak);
            }
        }
        fputs("\n", rovarsprak);
        /*  End Of Pirate - EOP ;)  */
    }
    fclose(svenskafil);
    fclose(rovarsprak);
}

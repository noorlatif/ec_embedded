#include <stdio.h>
#include <string.h>
#include "mathf.h"


int main(){
    // inform user to enter names without spaces and eof will stop first part of the code.
    printf("enter the name of the class without spaces, when done enter EOF. max classes 5: ");

    //places holder for class names
    char classesName[5][15];

    // count amount of class name entered, if the user entered less or more than hard coded amount.
    int countClass = 0;
    while (countClass<5)
    {
        if(scanf("%s",&classesName[countClass])!=1)
        break;

        countClass++;
    }

    //creating placeholder for mode and average
    double testTable[2][countClass]; 

    
    // loops trough all classes and askes user to enter result
    for (int i = 0; i < countClass; i++)
    {
        //create temp array where all values are saved when entered, and create variable that will keep count
        double tempArray[30];
        int entryCount = 0;
        while (entryCount < 30)
        {
            printf("Enter eof to end, enter result for the %s: ",classesName[i]);

            if(scanf("%lf",&tempArray[entryCount])!=1){
                // give value of 0 if user didnt enter any value
                if(entryCount==0){
                    tempArray[entryCount] = 0;
                    entryCount++;
                }
                break;
            }

            //check if the user didnt enter values more than 24
            if (tempArray[entryCount]>24)
            {
                printf("please enter value less than 25\n");
                continue;
            }
            
            entryCount++;
        }


        // get average and median, and save it to testTable
       testTable[0][i] = average(tempArray,entryCount);

       testTable[1][i] = median(tempArray,entryCount);

       
        
    }

    // print the result
    for (int y = 0; y < 2; y++)
    {
        if (y==0)
            printf("aver ");
        else
            printf("medi ");
        
        
        for (int x = 0; x < countClass; x++)
        {
            printf("%s: %5.2f\t",classesName[x],testTable[y][x]);
        }

        printf("\n");
        
    }

    return 0;
}
#include <stdio.h>

//gets the price and taxes, and returns a new price with tax included
double priceWithTax(double price, double tax){
    return price + price*tax;
}


void main(){

printf("enter the price of the good and tax, example 150 0.25.: ");

double price,tax;

scanf("%lf%lf",&price,&tax);

printf("new price with tax is: %0.2f",priceWithTax(price,tax));

}
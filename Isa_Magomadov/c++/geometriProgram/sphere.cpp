#include <iostream>
#include <cmath>
#include "sphere.h"

Sphere::Sphere(double radius){
    setRadius(radius);
}

void Sphere::setRadius(double radius){
    if( radius < 0)
    this->radius = 0;

    this->radius = radius;
}

double Sphere::getRadius(){
    return radius;
}


// V=(4/3)*(πr**3)
double Sphere::calculateVolume(){
    return (4)*((PI*pow(radius,3)/3));
}
// A=4*(πr**2)
double Sphere::calculateArea(){
    return (4)*(PI*pow(radius,2));
}
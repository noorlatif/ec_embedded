#define PI 3.14159265359

class Sphere{
    private:
        double radius;
    public:
        Sphere(void);
        Sphere(double);
        void setRadius(double);
        double getRadius();
        double calculateVolume();
        double calculateArea();
};
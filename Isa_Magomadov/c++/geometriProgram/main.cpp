#include <iostream>
#include "sphere.h"



int main(){
    double temp = 0;
    std::cout << "Enter radius for first sphere: ";
    std::cin >> temp;
    Sphere s1(temp);
    std::cout << "Enter radius for second sphere: ";
    std::cin >> temp;
    Sphere s2(temp);

    std::cout << "first sphere volume: " << s1.calculateVolume() << std::endl;
    std::cout << "first sphere area: " << s1.calculateArea() << std::endl;
    std::cout << "second sphere volume: " << s2.calculateVolume() << std::endl;
    std::cout << "second sphere area: " << s2.calculateArea() << std::endl;
    return 0;
}
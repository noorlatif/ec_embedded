/*
File: 3.4.cpp
Summary: This program checkes if entered gender and entered social number
         equals to the same gender.
Owner: Isa
*/
#include <iostream>

int main(){
    long long int socialNumber;
    int gender;
    std::cout << "Enter your gender(Male=0/Female=1): ";
    std::cin >> gender;
    std::cout << "Enter your social number: ";
    std::cin >> socialNumber;
    
    socialNumber = socialNumber/10;
    
    // check if social number is odd and gender is = 0 then person is male.
    // check if socail number is even and gender is = 1 then person is female
    if ((socialNumber%2 == 0 && gender == 1) || (socialNumber%2 != 0 && gender == 0)){
        std::cout << "Everthing seems to be correct";
    } else
        std::cout << "You might have entered wrong gender/social number";
    
    return 0;
}
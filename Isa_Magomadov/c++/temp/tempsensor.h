/*
File: tempsensor.h
Summary: this class will able to generate binary id for temp sensor
        and have assigning values for the objects
Owner: Isa
*/

#include <iostream>
#include <bitset>

class TempSensor{
    private:
        int consumption;
        std::string model;
        static int counterSensore; 
        std::bitset<4> idbinary;
    public:
        TempSensor(void);
        TempSensor(int, std::string);
        void setConsumption(int);
        void increaseConsumption(void);
        void decreaseConsumption(void);
        void printInfo(void);
        int getConsumption(void);
        std::string getModel(void);
};


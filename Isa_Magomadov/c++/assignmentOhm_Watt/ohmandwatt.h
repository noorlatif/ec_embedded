/*
File: ohmandwatt.h
Summary: class for ohm and watt calcutlations.
Owner: Isa
*/
#include <iostream>

class OhmAndWatt{
    private:
        double i = 0, r = 0, w = 0, v = 0;
        bool status = true;
        
    public:
    void setWatt();
    void setAmp();
    void setResistor();
    void setVoltage();
    void printValues();
    void calculateWatt();
    void calculateAmp();
    void calculateResistor();
    void calculateVoltage();
    void resetValues();
    void changeStatus(bool);
    bool getStatus();
};
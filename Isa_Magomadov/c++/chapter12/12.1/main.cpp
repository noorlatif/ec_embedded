#include <iostream>
#include <string>

struct createBook
{
    std::string title;
    std::string writer;
    int totalPage;
    double price;
};


void print(createBook book){
    std::cout << "title: " << book.title <<std::endl;
    std::cout << "writer: " << book.writer <<std::endl;
    std::cout << "price: " << book.price <<std::endl;
    std::cout << "total Page: " << book.totalPage <<std::endl;
}



int main(){
    createBook book1;
    std::cout << "enter title of the book: ";
    std::cin >> book1.title;
    std::cout << "enter name of the writer: ";
    std::cin >> book1.writer;
    std::cout << "enter price of the book: ";
    std::cin >> book1.price;
    std::cout << "enter total page: ";
    std::cin >> book1.totalPage;

    print(book1);
    return 0;
}
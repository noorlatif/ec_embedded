#include <iostream>

double add(double a, double b){
    return a+b;
}

int add(int a, int b){
    return a+b;
}

int main(){
    std::cout << "\nin c++ functions with same name can have diffrent parameter\n";
    std::cout << "\ndouble add(double a, double b){\n";
    std::cout << "\t return a+b;\n" << "}\n\n";
    std::cout << "int add(int a, int b){\n";
    std::cout << "\t return a+b;\n" << "}\n";

    std::cout << "add(15,15) = " << add(15,15) << std::endl;
    std::cout << "add(15.5,14.5) = " << add(15.5,14.5);

    return 0;
}
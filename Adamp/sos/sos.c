/* 

    The length of a dot is 1 time unit.

    A dash is 3 time units.

    The space between symbols (dots and dashes) of the same letter is 1 time unit.

    The space between letters is 3 time units.

    The space between words is 7 time units.

	1 time unit = Based on how many signals the person can send.
	In the program 1 time unit = 0.5 secs

	Only word to be be printed = SOS

	Output = 

	S = ...

	O = _ _ _

*/ 

#include <stdio.h>
#include <windows.h> 

void SOS();

void main()
{

    SOS();

}

void SOS()
{
	
	while(1) // evighetsloop
	{
		for(int i = 0; i < 3; i++)
		{
			printf("...");
			Beep(523,500); 
			Sleep(1);

			printf("_ _ _");
			Beep(323,400); 
			Beep(323,400); 
			Beep(323,400); 
			Sleep(1);

			printf("...");
			Beep(523,500); 
			Sleep(2);

			system("cls");
		}
	}
}
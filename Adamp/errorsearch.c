#include <stdio.h>

double main()  
{
	// Define and initialize variables
	double hoursPerWeek = 35;
	double hourlyWages = 83;
	
	// Calculate weekly wage
	double weeklyWages = hoursPerWeek * hourlyWages;

	// Show result
	printf("%s %f %s\n", "Given an hourly wage of ", hourlyWages, " kr");
	printf("%s %f %s\n", " the weekly wages will be: ", weeklyWages, " kr");

	return 0;
}

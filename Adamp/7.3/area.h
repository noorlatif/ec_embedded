#define PI 3.14159

double calculateRectangle(double b, double h);
double calculateTriangle(double b, double h);
double calculateCircle(double r);

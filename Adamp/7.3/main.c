#include <stdio.h>
#include "area.h"

#define PI 3.14159

int main()
{
    double b, l, r;
    int option;
    
    printf("Choose function: (1 = Rectangle, 2 = Triangle, 3 = Circle)\n");
    scanf("%d", &option);
    
    if(option == 1 || option == 2)
    {
        printf("Enter the base:");
        scanf("%lf", &b); 
         printf("Enter the height:");
        scanf("%lf", &l);    
    }
    if(option == 3)
    {
        printf("Enter radius of the circle:");
        scanf("%lf", &r);
    }
    
    if(option == 1)
        printf("The area of the rectangle is: %lf ", calculateRectangle(b,l));
    else if(option == 2)
        printf("The area of the triangle is: %lf ", calculateTriangle(b,l));
    else if(option == 3)
        printf("The area of the circle is: %lf ", calculateCircle(r));
    else
        printf("Error... \n");
}

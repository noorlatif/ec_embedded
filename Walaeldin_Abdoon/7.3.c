/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                                                                                        |
| OBS:  the Header file "7.3.h" found in the same path.                                  |
| This C program file contain a multiple prototypes functions for areas operations       |
|                                                                                        |
| function declaretion:                                                                  |
| double calccircle(double radies);                                                      | 
| double calcrectangel(double rwidth,double rheight);                                    | 
| double calctriangel(double rwidth,double rheight);                                     |
|_______________________________________________________________________________________*/ 


#include "7.3.h"
double calccircle(double radies)                    {return radies*radies*PI;}
double calcrectangel(double rwidth, double rheight) {return rwidth * rheight;}
double calctriangel(double twidth, double theight)  {return (twidth*theight) * 0.5;}
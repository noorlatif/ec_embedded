/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                           20 October 2019                                              |
| OBS:                                                                                   |
| This C header file contain a multiple prototypes function for rövarspråk operation     |
|                                                                                        |
| function declaretion:                                                                  |
| int checkedchar(char ch);                                                              |
| void ReadFromfiles();                                                                  |
| void WriteTofiles();                                                                   |
| void Fileoperations(int modes);                                                        |
|_______________________________________________________________________________________*/ 
 
void ReadFromfiles();
void WriteTofiles();
int checkedchar(char ch); 
void Fileoperations(int modes);
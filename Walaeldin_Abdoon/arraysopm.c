/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 16 October 2019                                            |
| OBS:  the Header file "Arraysop.h" found in the same path.                             |
| This C program file contain a multiple functions for 1D and 2D array operations        |
| Function prototypes :                                                                  |
|                                                                                        |
| One dimation array function definions:                                                 |
|                                                                                        |
|   void generaterandom1D(int aSize, int a[aSize])                                       |
|   void maximumordering(int aSize )                                                     |
|   void minimumordering(int aSize )                                                     |
|   void print1D(int aSizeint, a[aSize] )                                                |
|   void pascaltrianglematrices(int rownumber)                                           |
|   void sortordering1D(int aSize)                                                       |
|   void distinct1D(int aSize)                                                           |
|                                                                                        |
| Tow dimation array function definions:                                                 |
|                                                                                        |
|   void generatermatrices(int aSize, int aFri[aSize][aSize], int aSec[aSize][aSize])    |   
|   void multiplysquarematrices(int aSize)                                               |
|   void additionsquarematrices(int aSize)                                               |
|   void subtractsquarematrices(int aSize)                                               |
|   long factorial(int acount)                                                           |
|   void printmatrices(int aSize, int aFrimat[aSize][aSize],int aSecmat[aSize][aSize],   |
|                      int aResmat[aSize][aSize])                                        |
|_______________________________________________________________________________________*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "arraysop.h"

int main()
{
     int Selections, asizes;
     system("color 3");
     while(1) //loop som inte bryts förens användaren väljer att avsluta.
       { 
           printf("\nplease choose one of option from the list below");
              printf("\n\n\tfrist list for one dimension array operations :");
              printf("\n\t    0:  For Ascending order and get the maximum element an array");
              printf("\n\t    1:  For Decending order and get the minimum element an array");
              printf("\n\t    2:  For sort element an array");
              printf("\n\t    3:  For printing distinct elements an array");

              printf("\n\n\tsecond list for two dimations array operations:");
              printf("\n\t    4:  For printing Pascal triangle array");
              printf("\n\t    5:  For calculate multiply of tow square matrices");
              printf("\n\t    6:  For calculate addition of tow square matrices");
              printf("\n\t    7:  For calculate subtract of tow square matrices");
              printf("\n\t    8:  For Exit the program\n");
        if(scanf("%d", &Selections) != 1) //Om användaren anger "eof", avslutas programmet
         {
            break;
         }
         if (Selections == 8)
               {
                 break;                   
               }
        else if (Selections >= 0 && Selections <= 3)
               {
                 printf("\nplease enter the number of the columns:\n");
                 scanf("%d",&asizes);
                    switch (Selections)
                        {
                         case 0:  {maximumordering(asizes);        break;} 
                         case 1:  {minimumordering(asizes);        break;} 
                         case 2:  {sortordering1D(asizes);         break;} 
                         case 3:  {distinct1D(asizes);             break;}   
                         default:                                  break;
                        }
               }
        else if (Selections >= 4 && Selections <= 7)
               {
                 printf("\nplease enter the size of the squre matrices elements:\t");
                 scanf("%d",&asizes);
                    switch (Selections)
                        {
                         case 4:  {pascaltrianglematrices(asizes);    break;} 
                         case 5:  {multiplysquarematrices(asizes);    break;} 
                         case 6:  {additionsquarematrices(asizes);    break;} 
                         case 7:  {subtractsquarematrices(asizes);    break;}   
                         default:                                     break;
                        }
               }
        else printf("\nplease enter the number bitween 0 to 8\n");             
       }
}


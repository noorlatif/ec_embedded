/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 16 october 2019                                            |
|OBS:                                                                                    |
| This C Header file contain the multiple function defintions for 1D/2D array operations |
|                                                                                        |
| Function prototypes :                                                                  |
|                                                                                        |
| One dimation array Functions:                                                          |
|                                                                                        |
|   void generaterandom1D(int a[], int aSize)                                            |
|   void maximumordering(int aSize )                                                     |
|   void minimumordering(int aSize )                                                     |
|   void print1D(int a[], int aSize)                                                     |
|   void pascaltrianglematrices(int rownumber)                                           |
|   void sortordering1D(int aSize)                                                       |
|   void distinct1D(int aSize)                                                           |
|                                                                                        |
| Tow dimation array Functions:                                                          |
|                                                                                        |
|   void generatermatrices(int aFri[][], int aSec[][], int aSize)                     |   
|   void multiplysquarematrices(int aSize)                                               |
|   void additionsquarematrices(int aSize)                                               |
|   void subtractsquarematrices(int aSize)                                               |
|   long factorial(int acount)                                                           |
|   void printmatrices(int aFrimat[][],int aSecmat[][],int aResmat[][], int aSize)  |
|_______________________________________________________________________________________*/

   void generaterandom1D(int a[], int aSize);                                            
   void maximumordering(int aSize);                                                     
   void minimumordering(int aSize);                                                     
   void print1D(int a[], int aSize);                                                     
   void pascaltrianglematrices(int rownumber);                                           
   void sortordering1D(int aSize);                                                       
   void distinct1D(int aSize);  
   /*void generatermatrices(int aFri[][], int aSec[][], int aSize);                        
   void multiplysquarematrices(int aSize);                                               
   void additionsquarematrices(int aSize);                                               
   void subtractsquarematrices(int aSize);                                               
   long factorial(int acount);                                                           
   void printmatrices(int aFrimat[][],int aSecmat[][],int aResmat[][], int aSize);*/

   void generatermatrices(int aSize, int aFri[aSize][aSize], int aSec[aSize][aSize]);                        
   void multiplysquarematrices(int aSize);                                               
   void additionsquarematrices(int aSize);                                               
   void subtractsquarematrices(int aSize);                                               
   long factorial(int acount);                                                           
   void printmatrices(int aSize, int aFrimat[aSize][aSize],int aSecmat[aSize][aSize],int aResmat[aSize][aSize]);

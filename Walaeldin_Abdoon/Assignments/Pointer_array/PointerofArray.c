#include<stdio.h>
#include <stdlib.h>
#include <time.h>
/*_______________________________________________________________________________________*/
int getrandom (int upperLimit, int lowerLimit)
{ int Number;
    Number = ((rand() % (upperLimit - lowerLimit + 1)) + lowerLimit);
    return Number;
}

void generaterarry(int upperLimit, int lowerLimit, int ASize, int points[ASize])
{   time_t t;                           //locale variable declaration.
    srand((unsigned) time(&t));         // Initialization random function.
    for (int aIndex=0; aIndex <= ASize; aIndex++)
        points[aIndex] =  getrandom(upperLimit, lowerLimit); // get new random integer number.
}

int Summationresult(int ASize, int (*points))
{
    int sum=0; int aIndex = 0;//locale variables declaration.
    while (aIndex <= ASize){
        sum  += *(points + aIndex);// calculate the summation of all elements an array.
        aIndex ++; //Increment the a count after calculate.
    }
    return sum;
}

double factorialresult(int ASize, int (*points)) {
    double Fact = 1;//locale variable declaration.
    for (int aIndex = 0; aIndex <= ASize; aIndex++)
        Fact *= *(points + aIndex);// calculate the factorial of all elements an array.
    return Fact;
}

void Ascendingsort(int ASize, int *points)
{
    int Max, aIndex=0, bIndex; //locale variable declaration.
     ASize +=1; //Increment the variable for get correct size.
    printf("\n Ascending sort the numbers using array of pointer\n");
    do {
        bIndex = aIndex +1; // the second loop start from second element.
        while (bIndex < ASize){
            if (*(points + aIndex) < *(points + bIndex)){//compering between tow values
                Max = *(points + aIndex); //set the first value into temp variable.
                *(points + aIndex) = *(points + bIndex); //set the second elements into first.
                *(points + bIndex) = Max;//swapping between first and second values.
            } bIndex ++; //Increment the second loop after process.
        } aIndex ++; //Increment the first loop after process.
    } while (aIndex < ASize);
    for (aIndex = 0; aIndex < ASize; aIndex++)
        printf("%d\t", *(points + aIndex));//Print the array of pointer after sorting.
    printf("\n");
}

void Descendingsort(int ASize, int *points)
{
    int Min, aIndex=0, bIndex; //locale variable declaration.
    ASize +=1; //Increment the variable for get correct size.
    printf("\n Descending sort the numbers using array of pointer\n");
    do {
        bIndex = aIndex +1; // the second loop start from second element.
        while (bIndex < ASize){
            if (*(points + aIndex) > *(points + bIndex)){//compering between tow values
                Min = *(points + aIndex); //set the first value into temp variable.
                *(points + aIndex) = *(points + bIndex);//set the second elements into first.
                *(points + bIndex) = Min;//swapping between first and second values.
            } bIndex ++; //Increment the second loop after process.
        } aIndex ++; //Increment the first loop after process.
    } while (aIndex < ASize);

    for (aIndex = 0; aIndex < ASize; aIndex++)
        printf("%d\t", *(points + aIndex));//Print the array of pointer after sorting.
    printf("\n");
}

int getmaxvalue(int ASize, int *points)
{
    int Max= *(points + 0); int aIndex=0; //locale variable declaration.
    while (aIndex <= ASize) {
        if (Max < *(points + aIndex))//compering between tow values
            Max = *(points + aIndex);//swapping between maximum and element an array values.
        aIndex ++; //Increment the loop after process.
    }
    return Max;// return the maximum element an array.
}
int getminvalue(int ASize, int *points)
{
    int Min= *(points + 0); int aIndex=0; //locale variable declaration.
    while (aIndex <= ASize) {
        if (Min > *(points + aIndex))//compering between tow values
            Min = *(points + aIndex);//swapping between minimum and element an array values.
        aIndex ++;//Increment the loop after process.
    }
    return Min;// return the minimum element an array.
}

float averageresult(int ASize, int *points)
{// calling Summationresult function for get the sum of all elements.
    return (float) Summationresult(ASize, points) / ASize;
}

float medianresult(int ASize, int *points)
{// calling max and min value function for get the sum maximum and minimum of all elements and divide 2.
    return 0.5* (getmaxvalue(ASize, points) + getminvalue(ASize, points));
}

#include<stdio.h>
#include "PointerofArray.c"

int main() {
    int aSize,upperLimit, lowerLimit;//locale variable declaretion.
    printf("enter the number of elements an array of pointer:\n");
    scanf("%d",&aSize); // Read the size of elements an the array.
    printf("enter the upper value of element:\n");
    scanf("%d",&upperLimit);// Read the upper value for setting random number an array.
    printf("enter the lower value of element:\n");
    scanf("%d",&lowerLimit);// Read the lower value for setting random number an array.
    int P[aSize];//locale variable declaretion.
    generaterarry(upperLimit,lowerLimit,aSize,P);// set aSize random number on an array.
    printf("Orginal array of pointer\n");
    for (int aIndex = 0; aIndex <= aSize ; ++aIndex)
    printf("%d\t",P[aIndex]);//Print orginal order and values element an array.

    printf("\n");
    Descendingsort(aSize, P);// call descending sort function.
    Ascendingsort(aSize, P);// call aescending sort function.

    printf("The minimum is   : %d\n",getminvalue(aSize,&P));// Print minimum element an array.
    printf("The maximum is   : %d\n",getmaxvalue(aSize,&P));// Print maximum element an array.
    printf("The summation is : %d\n", Summationresult(aSize, &P));// Print summation of all elements an array.
    printf("The factorial is : %lf\n", factorialresult(aSize, &P));// Print factorial of all elements an array.
    printf("The median is    : %lf\n",medianresult(aSize,&P));// Print median of all elements an array.
    printf("The average is   : %lf\n",averageresult(aSize,&P));// Print average of all elements an array.
    return 0;
}
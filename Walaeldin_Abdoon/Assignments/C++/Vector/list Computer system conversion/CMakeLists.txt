cmake_minimum_required(VERSION 3.15)
project(Convertsystem)

set(CMAKE_CXX_STANDARD 14)

add_executable(Convertsystem main.cpp Convertlib.cpp Convertlib.h)
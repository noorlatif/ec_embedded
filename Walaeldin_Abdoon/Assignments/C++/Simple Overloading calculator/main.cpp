/*
+________________________________  Programming information   ______________________________+
|  *  File name               : main.cpp                                                   |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/17/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               Simple calculator use Overloading functions                |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/

#include <iostream>
#include <cctype>
#include "Operationlib.h"

using namespace std;
int main() {
    // Declare local variables.
    bool Turnoff= true;
    string  fnum, snum;char Beslut;

    while (Turnoff) {
        system("CLS");
        system("color 3");
        cout <<endl<<endl<< "\t\t...Simple calculator program...."<<endl<<endl;
        cout << "Enter the first operand......."<<endl<<endl;
        cin>>fnum;
        cout << "Enter the second operand......."<<endl<<endl;
        cin>>snum;
        Opreitions(fnum,snum);//call function from header file.
        // control the program from the user.
        cout << "To continue press Y or N to exit......."<<endl<<endl;
            do{
                Beslut = tolower(getchar());
            }while( !((Beslut == 'y') || (Beslut == 'n')) );
    Turnoff =  (Beslut == 'n')? false:true;
    if (!Turnoff) break;
    }
    return 0;
}
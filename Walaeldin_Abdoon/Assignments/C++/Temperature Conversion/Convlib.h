//
// Created by walaeldinabdoonsaee on 12/1/2019.
//

#ifndef F2C_COUVERSION_CONVLIB_H
#define F2C_COUVERSION_CONVLIB_H
class temperature
{
private:
    float Celcius;
    float Fahrenheit;
public:
    void setCelcius(float deg){ this->Celcius = deg;}
    void setFahrenheit(float deg){ this->Fahrenheit = deg;}

    float getCelcius(){return (this->Celcius);}
    float getFahrenheit(){return (this->Fahrenheit);}

    temperature(float cdeg);

/* declare member function to convert Fahrenheit temperature to Celsius*/
    double F2C();
/* declare member function to convert Celsius temperature to Fahrenheit */
    double C2F();
};

#endif //F2C_COUVERSION_CONVLIB_H

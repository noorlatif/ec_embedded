//
// Created by walaeldinabdoonsaee on 12/1/2019.
//

#include "Convlib.h"

temperature::temperature(float cdeg) {
    this->Celcius = cdeg;
    this->Fahrenheit = C2F();
}

/* define member function to convert Fahrenheit temperature to Celsius
 * Note:*
 * F° to C°: Fahrenheit to Celsius Conversion Formula:(°F-32)x.5556=°C,(5/9)=.5556*/
double temperature::F2C() { return (this->Fahrenheit -32.0)*0.5556;}

/* define member function to convert Celsius temperature to Fahrenheit
 * Note:
 * C° to F°: Celsius to Fahrenheit Conversion Formula:(°Cx1.8)+32=°F,(9/5)=1.8*/
double temperature::C2F() { return ((this->Celcius * 1.8) + 32.0);}

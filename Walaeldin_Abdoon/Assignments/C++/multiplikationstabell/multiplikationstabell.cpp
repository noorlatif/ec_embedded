#include <iostream>
#include <iomanip>

using namespace std;
void multiplikationstabell(int aDigital);

int main() {
    int Digital;

    cout << "Please enter the one number for at calculates...!" <<endl;

    do
        cin>>Digital;
    while ( !((Digital >= 1) && (Digital <= (99999999/12))) );

    for ( int acount =1; acount <=Digital;acount++)
        multiplikationstabell(acount);

    return 0;
}

void multiplikationstabell(int aDigital)
{

    for (int acount =1; acount <=12;acount++) {
        if (((aDigital * 12) >= 10) && ((aDigital * 12) <= 99)) {
            cout << setw(2) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(2) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 100) && ((aDigital * 12) <= 999)){
            cout << setw(3) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(3) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 1000) && ((aDigital * 12) <= 9999)){
            cout << setw(4) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(4) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 10000) && ((aDigital * 12) <= 99999)){
            cout << setw(5) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(5) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 100000) && ((aDigital * 12) <= 999999)){
            cout << setw(6) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(6) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 1000000) && ((aDigital * 12) <= 9999999)){
            cout << setw(7) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(7) << aDigital * acount << endl;
        }
        else if (((aDigital * 12) >= 10000000) && ((aDigital * 12) <= 99999999)){
            cout << setw(8) << aDigital << " * " << setw(2) << acount << " = "
                 << setw(8) << aDigital * acount << endl;
        }
    }
    cout<<endl<<endl;
}

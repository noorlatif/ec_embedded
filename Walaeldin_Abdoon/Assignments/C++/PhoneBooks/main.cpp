/*
+________________________________  Programming information   ______________________________+
|  *  File name               : main.cpp                                                   |
|  *  Header File             : BookClass.h                                                |
|  *  Source File             : BookClass.cpp                                              |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/13/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program based on our class of person and have a multi data |
|                               and functions members,that in the order to input/output in |
|                               the record of phone book and save them temporarily until   |
|                               the terminates the application.                            |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#include <iostream>
#include "BookClass.h"

using namespace std;

int main() {
    int Blength;
    string EnteryFirstname;
    string EnteryLastname;
    string EnteryPhone;
    string EnteryEmail;
    string EnteryFacebook;
    string EnteryPostort;
    string EnteryPostnummer;


    cout <<endl<<endl<< "Please enter the number of persons you won't to save into database"<<endl;
    cin  >> Blength;
    ClassBook Objs[Blength];//define array of objects of class ClassBook

    if (Blength <= 0 )
        cout << endl << endl << "The data record is not accessible...." << endl;
    else {

        for (int Inercount = 0; Inercount < Blength; Inercount++) {

            cout << endl << "Personal ID  " << Inercount + 1 << endl;
            // Save the personal Index into class member.
            Objs[Inercount].setIndex(Inercount + 1);

            cout << "Please enter the first name" << endl;
            cin >> EnteryFirstname;
            cout << "Please enter the last name" << endl;
            cin >> EnteryLastname;
            // Save the personal first & last name into class member.
            Objs[Inercount].setName(EnteryFirstname, EnteryLastname);

            cout << "Please enter phone number" << endl;
            cin >> EnteryPhone;
            // Save the personal phone number into class member.
            Objs[Inercount].setPhone(EnteryPhone);

            cout << "Please enter the e-mail" << endl;
            cin >> EnteryEmail;
            // Save the personal E-mail into class member.
            Objs[Inercount].setEmail(EnteryEmail);

            cout << "Please enter the facebook account" << endl;
            cin >> EnteryFacebook;
            // Save the personal facebook account into class member.
            Objs[Inercount].setFacebook(EnteryFacebook);

            cout << "Please enter the post address" << endl;
            cin >> EnteryPostort;
            // Save the personal a home address into class member.
            Objs[Inercount].setpostort(EnteryPostort);

            cout << "Please enter the Zip" << endl;
            cin >> EnteryPostnummer;
            // Save the personal a zip number into class member.
            Objs[Inercount].setPostnummer(EnteryPostnummer);

            /*//define array of objects of class ClassBook
            Objs[Inercount]{Inercount,EnteryFirstname,EnteryLastname,
                                    EnteryPhone, EnteryEmail,EnteryFacebook,
                                    EnteryPostort,EnteryPostnummer};*/

            cout << endl << "End ...  " << Inercount + 1 << " of " << Blength << " Persons" << endl;
        }
        cout << endl << "The phone book record we will show below ...";

        cout << endl << endl << "_____** Begin of List ***_____" << endl;
        for (int Outercount = 0; Outercount < Blength; Outercount++) {

            cout << "Personal data " << Outercount + 1 << " of " << Blength << " Persons" << endl << endl;
            cout << "Personal ID: " << Objs[Outercount].getIndex() << endl;
            cout << "Name       : " << Objs[Outercount].getName() << endl;
            cout << "Phone      : " << Objs[Outercount].getPhone() << endl;
            cout << "E-mail     : " << Objs[Outercount].getEmail() << endl;
            cout << "Facebook   : " << Objs[Outercount].getFacebook() << endl;
            cout << "Address:   : " << Objs[Outercount].getpostort() << endl;
            cout << "Zip Post   : " << Objs[Outercount].getPostnummer() << endl;
            cout << endl << " **** " << endl;
        }
        cout << endl << endl << "_____** End of List ***_____" << endl;
    }
    return 0;
}
cmake_minimum_required(VERSION 3.15)
project(Classtest)

set(CMAKE_CXX_STANDARD 14)

add_executable(Classtest main.cpp Newclass.cpp Newclass.h)
#include <iostream>
#include "Newclass.h"
using namespace std;
int main() {
    string Fname;
    string Lname;
    float  Ages;
    float  Salarys;
    Person Obj("","",0.0,0.0);

    cout << "Please enter the first name: "  << endl;
    cin >>Fname;
    cout << "Please enter the second name: " << endl;
    cin >> Lname;
    cout << "Please enter the age: "         << endl;
    cin >>Ages;
    cout << "Please enter the Salary: "      << endl;
    cin >>Salarys;

 // Assigment the class member in the Person class throw Obj object.
   /*
    Obj.setFristname(Fname);//call member function to set data.
    Obj.setSecondname(Lname);//call member function to set data.
    Obj.setAge(Ages);//call member function to set data.
    Obj.setSalary(Salarys);//call member function to set data.
                           */
    Obj={Fname,Lname,Ages,Salarys};//call constructor function to set all data.

    // Assigment the class member in the Person class throw Obj object.

    cout << "All the data into Obj class!"<< endl<< endl;
    // //call member functions to display all the data
    cout << "The person data is\n"                       << endl
         << "The first name is: " <<  Obj.getFristname() << endl
         << "The last name is : " << Obj.getSecondname() << endl
         << "The age is       : " <<  Obj.getAge()       << endl
         << "The salary is    : " << Obj.geSalary()<<" Kr."<< endl;

    return 0;
}
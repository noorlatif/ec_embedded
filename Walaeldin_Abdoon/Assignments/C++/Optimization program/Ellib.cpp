//
// Created by walaeldinabdoonsaee on 12/1/2019.
//
#include <ctime>
#include "Ellib.h"

double el::Voltage() {
    return  Ohm * Amper;
}
double el::Current() {
    try {return Division(Volt,Ohm);}
    catch (const char *er) {cerr << er ;}
}
double el::Resistance() {
    try {return Division(Volt,Amper);}
    catch (const char *er) {cerr << er ;}
}
double el::TotalResistance(float Fres, float Sres, float Tres) {
    //1/a + 1/b + 1/c >> (bc+ac+ab)/(abc)
    double Total= (Sres*Tres + Fres*Tres + Fres*Sres);
    //if abc != 0 total /= abc else show massage div by 0.
    try {Total = Division(Total,(Fres*Sres*Tres));}
    catch (const char *er){ cerr << er ;}
    return Total;
}
double el::sken_3fas() {return sken_eff() * sqrt(3);}
double el::aktiv_3fas(double cos) {aktiv_eff(cos)*sqrt(3);}
double el::sken_eff() {return eff_enk();}
double el::aktiv_eff(double cos) {return eff_enk()*cos;}
double el::eff_enk() {return getvoultage() * getcurrent();}

float Repeats(float Val) {
    float scan;
    if (Val == -1) {
        // initialize the random number generator
        srand(time(NULL));
        float temp = float(rand()) / (float(RAND_MAX) + 1.0);
        cout << "random generated is " << temp << endl;
        return temp;
    }
    try {
        if (Val == 0)
            throw "You have entered wrong input:\n";
        cin >> scan;
        while (cin.fail() || scan > Val) {
            cin.clear();
            cin.ignore(INT_MAX, '\n');
            cout <<  "Too high value or You have entered wrong input, try again:"<<endl;
            cin >> scan;
        }
        return scan;
    }
    catch (const char *ermsg) {
        cout << ermsg;
    }
}

float el::Scaninput(float comparewith,const string & msg) {
    try {
        cout<<msg<<endl;
        if(msg.empty()==0){
            float   Scaning = Repeats(comparewith);
            return Scaning;
        } else throw "error input...";
    }
    catch (const char *ermsg) {
        cout << ermsg; }
}

double el::Division(float aparam, float bparam) {
    if (bparam ==0)
        throw "Division by Zero";
    return  (aparam/bparam);
}

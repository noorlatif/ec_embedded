#include <iostream>
#include "Ellib.h"

int main() {
    el Power;
    float Resistance;
    float Current;
    float Voltage;
    float theta;
    int Val;
    bool TrunOff= false;
    //system("chcp 1252");
    //system("cls");

    while (!TrunOff){

        do {
            cout <<endl<<endl<< "Choose the quantities you want to calculate:"<<endl
                 << "\tselect 1 for: Ohms law............................"<<endl
                 << "\tselect 2 for: total Resistance...................."<<endl
                 << "\tselect 3 for: simple power lows..................."<<endl
                 << "\tselect 4 for: single phase power lows............."<<endl
                 << "\tselect 5 for: Active/medium power single phase...."<<endl
                 << "\tselect 6 for: apparent power 3-phase.............."<<endl
                 << "\tselect 7 for: Active power 3-phase................"<<endl
                 << "\tselect 0 for: terminate program..................."<<endl;
            cin>>Val;
        }while (!(7 >= Val && Val >= 0));

        switch (Val) {
            case 1: {
                cout << "Ohms law voltage (volt / V) coverage V equal to Resistance (Ohm) coverage R times\n"
                     <<   "The current (Ampere) with coverage I. Short V = R * I." << endl<< endl;

                Resistance = Power.Scaninput(20000, "write resistance less than 20 000 Ohm");
                Current = Power.Scaninput(440, "write current less than 440 Amp");
                Power.setcurrent(Current); Power.setresistance(Resistance);
                cout<< "the voltage is "<< Power.Voltage()<< " Volt" <<endl;
                break;
            }
            case 2:{
                cout << "Resistance interconnected in parallel circuits equals 1 split.\nResistance R total"
                     << " equals Resistance 1 / R1 + 1 / R2 + 1 / R3\nwhen we use a maximum of three resistors."
                     << endl<< endl;

                float Fristresistance,Secoundtresistance,Thirdresistance;
                Fristresistance    = Power.Scaninput(20000, "write resistance 1 less than 20 000 Ohm");
                Secoundtresistance = Power.Scaninput(20000, "write resistance 2 less than 20 000 Ohm");
                Thirdresistance    = Power.Scaninput(20000, "write resistance 3 less than 20 000 Ohm");
                Resistance         = Power.TotalResistance(Fristresistance,Secoundtresistance,Thirdresistance);
                cout<< "the total resistances is "<< Resistance <<" Ohms" <<endl;
                break;
            }
            case 3:{
                cout << "Active single phase single phase equals power P in watts (W)\n"
                     << "equals voltage V in volts (V) times current I"<< endl << endl;

                Voltage = Power.Scaninput(INT_MAX, "write the voltage per (volt)");
                Current = Power.Scaninput(440, "write current less than 440 Amp");
                Power.setvoultage(Voltage); Power.setcurrent(Current);
                cout<< "the active power is "<< Power.eff_enk() <<" Watt" <<endl;
                break;
            }
            case 4:{
                cout << "Apparent power single phase is calculated with the magnitude VA (VoltAmpere)\n"
                     << " equal to the voltage V in volts (V) times the current I in amperes (A)"
                     << endl<< endl;
                Voltage = Power.Scaninput(INT_MAX, "write the voltage(rms) per (volt)");
                Current = Power.Scaninput(440, "write current(rms) less than 440 Amp");
                Power.setvoultage(Voltage); Power.setcurrent(Current);
                cout<< "the apparent power is "<< Power.sken_eff() <<" VA" <<endl;
                break;
            }
            case 5:{
                cout << "Active single phase single phase equals power P in watts (W)\n"
                     << "equals voltage V in volts (V) times current I "
                     << "in Ampere times \nthe cosine power / power factor <1"<< endl << endl;

                theta   = Power.Scaninput(-1,"program well generate cosine(vai/PFC).");
                Voltage = Power.Scaninput(INT_MAX, "write the voltage per (volt)");
                Current = Power.Scaninput(440, "write current less than 440 Amp");
                Power.setvoultage(Voltage); Power.setcurrent(Current);
                cout<< "the active power is "<< Power.aktiv_eff(theta) <<" Watt" <<endl;
                break;
            }
            case 6:{
                cout<< "3-phase apparent power is AC voltage, apparent power S in voltampere (VA)\n"
                    << " equal to voltage V in volt (V) times the current I in ampere (A)\n"
                    << " times the root of 3 SQRT (3)"<< endl << endl;

                Voltage = Power.Scaninput(400, "write the voltage(rms) less than 400 volt.");
                Current = Power.Scaninput(440, "write current(rms) less than 440 Amp");
                Power.setvoultage(Voltage); Power.setcurrent(Current);
                cout<< "the apparent power is "<< Power.sken_3fas() <<" VA" <<endl;
                break;
            }
            case 7:{
                cout << "3-phase active power is the power P in Watt (W)\n"
                     <<   " equal to the voltage V in volts (V) times the current I \n"
                     <<    "in amperes (A) times cos <=1 && cos >= 0 times the root of 3 SQRT (3).\n"
                     << " times the root of 3 SQRT (3)"<< endl << endl;

                theta   = Power.Scaninput(-1,"program well generate cosine power/power factor.");
                Voltage = Power.Scaninput(400, "write the voltage less than 400 volt.");
                Current = Power.Scaninput(440, "write current less than 440 Amp.");
                Power.setvoultage(Voltage); Power.setcurrent(Current);
                cout<< "the active power is "<< Power.aktiv_3fas(theta) <<" Watt" <<endl;
                break;
            }
            case 0: {TrunOff= true; break;}
            default: {cout<<"Wrong alternative, try again..."<<endl; break;}
        }
    }
   return 0;
}
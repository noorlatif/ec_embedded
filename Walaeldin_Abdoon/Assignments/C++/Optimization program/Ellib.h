//
// Created by walaeldinabdoonsaee on 12/1/2019.
//

#ifndef ELPROGRAMMET_ELLIB_H
#define ELPROGRAMMET_ELLIB_H

#include <iostream>
#include <limits>
#include <cmath>

using namespace std;

class el{
private:
    double Ohm;
    double Amper;
    double Volt;
public:
    void setcurrent(double Amp)   {Amper = Amp;}
    void setvoultage(double Vol)  {Volt = Vol;}
    void setresistance(double Res){ Ohm = Res;}

    double getcurrent()   { return Amper;}
    double getvoultage()  { return Volt;}
    double getresistance(){ return Ohm;}
    double Division(float aparam, float bparam);
    double Voltage();
    double Current();
    double Resistance();
    double TotalResistance(float Fres, float Sres, float Tres);

    double eff_enk();
    double sken_eff();
    double aktiv_eff(double cos);
    double sken_3fas();
    double aktiv_3fas(double cos);

    float Scaninput(float Val,const string & msg);
};
#endif //ELPROGRAMMET_ELLIB_H

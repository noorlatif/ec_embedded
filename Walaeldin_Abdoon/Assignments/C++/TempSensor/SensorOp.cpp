/*
+________________________________  Programming information   ______________________________+
|  *  File name               : SensorOp.cpp                                               |
|  *  Header File             : SensorOp.h                                                 |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/19/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :  the program calculate and processing the battery level    |
|                                and generated random serial number,use static variable for|
|                                ID number, have one class called TempSensor               |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
 */
#include "SensorOp.h"

//Define function for increasing consumption by 2
void TempSensor::Increase(){
    if(this->Consumption<=98)//if Consumption equal or grater than 98
        ++++this->Consumption;
}
//Define function for decreasing consumption by 2
void TempSensor::Decrease(){
    if(this->Consumption>=2)//if Consumption equal or grater than 2
        ----this->Consumption;
}

//Define class function for print information for sensor Model
string TempSensor::printModel(){
    return this->Model;
}
//Define class function for print information for sensor
void TempSensor::printSensor() {
    cout << "Sensor ID: " << this->SensorID << endl;
    cout << "Battery consumption: " << this->Consumption <<" %"<< endl;
    cout << "Model: " << this->Model << endl;
}
//Define function to generate random serial Model number.
string RandModel(){
    char ctemp[]="AB-00-HD-00A00";
    string stemp="";
    // initialize the random number generator
    srand (time(NULL));
    for (int Index = 0; Index < ctemp[Index] !='\0'; ++Index) {
        if (ctemp[Index] != '-'){
            if (ctemp[Index] == '0')
                /* generate a random number from 0-9 and casting to string
                 * then assigned to last position into stemp */
                stemp += to_string(RANDOM(9, 0));
            else if (ctemp[Index] != '0')
                /*generate a random number and convert to a character
                 * from A-Z then assigned to last position into  stemp*/
                stemp += (char(RANDOM(90,67)));
        }
        else stemp +=char('-');//no change.
        }
    return stemp;//return finally serial model as string.
}
/*
+________________________________  Programming information   ______________________________+
|  *  File name               : SensorOp.h                                                 |
|  *  Source File             : SensorOp.cpp                                               |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/19/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :  the program calculate and processing the battery level    |
|                                and generated random serial number,use static variable for|
|                                ID number, have one class called TempSensor               |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
 */

#ifndef SENSOR_SENSOROP_H
#define SENSOR_SENSOROP_H

#include <ctime>   // include for (time in srand)
#include <cstdlib>// include for (rand and srand)
#include <iostream>// include for (cout and cin, other)
#include <string>// include for string operation.
// Declare Random function for generate random (0-9 or A-Z) char
#define RANDOM(U,L)  (rand() % (U - L + 1) + L)

//Declare global static variable.
//static int Serialnumber;

using namespace std;

string RandModel();

//create class TempSensor
static int Serialnumber;

class TempSensor {
private:
    //Declare various private data members
    int SensorID;
    int Consumption;
    //Declare protected data members
protected:
    string Model;
public:
    // Declare default constructor function for initializes the data members.
    TempSensor(){
        Serialnumber = 1001; //create a Serialnumber for SensorID
        this->SensorID     = Serialnumber;
        this->Consumption  = 0;
        this->Model        = "favorite Model";
        Serialnumber++;
    }
    //Declare Overloading constructor function for initializes the data members.
    TempSensor(int consumption) {
        this->SensorID     = Serialnumber;
        this->Consumption  = consumption;
        this->Model        = RandModel();
        Serialnumber++;
    }
    // Declare destructor function for destroyed the class data members.
    ~TempSensor(){
        cout<<endl<<"\t\t\t\t\t\t the "<< this->Model << " - destructor it's called...."<<endl;
    }

    /*********Declare various properties function members*********/

    // Declare function for read Consumption value.
    int getConsumption(){
        return this->Consumption;
    }
    // Declare function for write Consumption value.
    void setConsumption(int consumption){
        if ((consumption<=100)&&(consumption>=0))
        this->Consumption = consumption;
        else cout<<endl << "please insert number between 0 to 100" << endl;
    }
    //Declare function for increasing consumption by 2
    void Increase();
    //Declare function for decreasing consumption by 2
    void Decrease();
    //Declare function for print information for sensor Model
    string printModel();
    //Declare function for print information for sensor
    void printSensor();
};


#endif //SENSOR_SENSOROP_H

/*
+________________________________  Programming information   ______________________________+
|  *  File name               : main.cpp                                                   |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/19/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :  the program calculate and processing the battery level    |
|                                and generated random serial number,use static variable for|
|                                ID number, have one class called TempSensor               |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
 */
#include <iostream>
#include <iomanip>
#include "SensorOp.h"
using namespace std;

void MainPrint(TempSensor Obj, int id);
int main() {

    // initialize the random number generator
    srand (time(NULL));

    system("color 3");

    cout<<endl<<endl<<"\t\t___First Object initialize by default construct___"<<endl;
    TempSensor newtempsensor1;
    int Value = RANDOM(100,0);
    newtempsensor1.setConsumption(Value);
    MainPrint(newtempsensor1,1);

    cout<<endl<<endl<<"\t\t___Second Object initialize by overloading construct___"<<endl;
    Value = RANDOM(100,0);
    TempSensor newtempsensor2(Value);
    MainPrint(newtempsensor2,2);

    cout<<endl<<endl<<"\t\t___Third Object initialize by default construct again___"<<endl;
    Value = RANDOM(100,0);
    TempSensor newtempsensor3;
    newtempsensor3.setConsumption(Value);
    MainPrint(newtempsensor3,3);

    cout<<endl<<endl<<"\t\t___Fourth Object initialize by overloading construct again___"<<endl;
    Value = RANDOM(100,0);
    TempSensor newtempsensor4(Value);
    MainPrint(newtempsensor4,4);
    return 0;
}


void MainPrint(TempSensor Obj, int id){
    cout <<"\t\t" "object id is "<< id <<" and Model is '" << Obj.printModel()
         << "' information data "<< endl << endl;
    Obj.printSensor();
    cout<<endl<<endl<<"\t 50 times Discharging Battery (Consumption)_____________"<<endl<<endl;
    for(int count = 50;count>=0;count--) {
        Obj.Decrease();
        cout.width(6);
        cout << right << Obj.getConsumption()<<" %";
    }
    cout<<endl<<endl<<"\t50 times Charging Battery (Consumption)_____________"<<endl<<endl;
    for(int count = 50;count>=0;count--) {
        Obj.Increase();
        cout.width(6);
        cout << right << Obj.getConsumption()<<" %";
    }
    cout<<endl<<endl;
}
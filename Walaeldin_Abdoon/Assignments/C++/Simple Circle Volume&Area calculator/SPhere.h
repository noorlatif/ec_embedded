/*
+________________________________  Programming information   ______________________________+
|  *  File name               : SPhere.h                                                   |
|  *  Source File             : SPhere.cpp                                                 |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/20/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               simple Circle volume/area calculator using full class      |
|                                option                                                    |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#ifndef SPHERECLASS_SPHERE_H
#define SPHERECLASS_SPHERE_H

#include <iostream>
using namespace std;

class Sphere{//Declare Sphere class
private:
    //Declare various private data members
    float radius;
public:
    // Declare default constructor function for initializes the data members.
    Sphere(){
        this->radius = 0;
    }
    // Define overloading constructor function for initializes the data members.
    Sphere(float radius){
        if (radius < 0)
            radius = 0;
        this->radius = radius;
    }
    // Define destructor function for destroyed the class data members.
    ~Sphere(){
        cout<<endl<<"\t\t\t the destructor it's called...."<<endl;
    }
// Define function for write positive radius value.
    void Setradius(float radius){
        if (radius < 0)
            radius = 0;
        this->radius = radius;
    }
// Define function for read radius value.
    double Getradius(){
        return this->radius;
    }
// Declare function for calculate volume.
    double Volume();
// Declare function for calculate area.
    double Area();
};

void PrintSphere(Sphere Obj);
#endif //SPHERECLASS_SPHERE_H

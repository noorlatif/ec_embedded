/*
+________________________________  Programming information   ______________________________+
|  *  File name               : SPhere.cpp                                                 |
|  *  Header File             : SPhere.h                                                   |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/20/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               simple Circle volume/area calculator using full class      |
|                                option                                                    |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#include "SPhere.h"
#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;
//Define class member function for calculate volume.
double Sphere::Volume(){
    1.33*M_PI*pow(this->radius,3); //  >>>> 4/3 = 1.333333333
}
//Define class member function for calculate volume.
double Sphere::Area(){
    4*M_PI*pow(this->radius,2);
}
//Define function for print the result.
void PrintSphere(Sphere Obj){
    //Declare local variables.
    float radius,volume,area;

    system("CLS");
    system("color 3");
    cout << endl << endl<< "this program calculate the Volume/Area of the"<<
         "circuit if the radius" << endl << " it's positive or negative," <<
         " but the negative radius the result became zero" << endl << endl
         << "please enter the radius!..." << endl;
    cin>>radius;

    // Set new floating point setting format.
    cout.setf (ios :: fixed, ios :: floatfield);
    // Set three decimals point format.
    cout.precision (3);
    if (radius > 0){

        Obj.Setradius(radius);
        volume = Obj.Volume();
        area   = Obj.Area();
        cout << "the radius is positive .. it's equal "<< right << Obj.Getradius() << endl;
        cout << "the volume is "<< right <<volume <<" the area is " << right << area << endl
             << "when the real radius is "<< radius  << endl<< endl;
        cout << "Calculate inverse radius is (-radius) " << endl<< endl;
        Obj.Setradius(-1*radius);
        volume = Obj.Volume();
        area   = Obj.Area();
        cout << "the radius is negative .. it's equal " << right << Obj.Getradius() << endl;
        cout << "the volume is "<< volume <<" the area is " << area << endl
             << "when the inverse real radius "<< radius << endl;

        } else {

        Obj.Setradius(radius);
        volume = Obj.Volume();
        area   = Obj.Area();
        cout << "the radius is negative .. it's equal " <<Obj.Getradius() << endl;
        cout << "the volume is "<< volume <<" the area is " << area << endl
             << "when the real radius is "<< radius << endl<< endl;
        cout << "Calculate inverse radius is (-radius) "<< endl<< endl ;
        Obj.Setradius(-1*radius);
        volume = Obj.Volume();
        area   = Obj.Area();
        cout << "the radius is positive .. it's equal " <<Obj.Getradius() << endl;
        cout << "the volume is "<< volume <<" the area is " << area << endl
             << "when the inverse real radius "<< radius << endl<< endl;
    }
}
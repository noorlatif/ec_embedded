/*
+________________________________ Programming information 6.2 _____________________________+
|  *  File name               : 6.2.c                                                      |
|  *  Version                 : 1.2                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/06/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program have a one function that calculates what a product |
|                               costs, including VAT. As parameters, the function must be  |
|                               partly price · excluding VAT and partly the VAT rate       |
|                                expressed as a percentage.                                |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#include <iostream>
// behövs för att använda manipulatorer med parametrar (precision, bredd).
#include <iomanip>

// jag bruk std Objeckt för att skriva litte kod med (cin/cout).
using namespace std;

//Deklaration funktionen för att räkna receipt.
double Calculate(double Netprice, double percentage, double *Vatprice);
void Printreceipt(double Netprice, double percentage, double Vatprice, double Totalprice);

int main() {
    // Det här variabeler för att räkna receipt.
    double percentage, Netprice, Vatprice, Totalprice;
    // Det här variabeler för att avsluta programet.
    char Ch = 'N';

    for(bool trunOff= false;;)
    {
        // skriva ut ett meddelande på användarskärmen för händelse.
        cout  << endl << "Enter a net price : " << endl;
        // läsa Netprice från tangentbord
        cin >> Netprice;

        cout << "Enter the percentage(%) of tax from 0 to 100: " << endl;
        /* om I alla fall att användarinmatningen fel väljer programmet
         * tvinga användaren att skriva bara 0% till 100%.*/
        do
            //läsa percentage från tangentbord.
            cin >> percentage;
        while (!( percentage >= 0 || percentage <=100));

        // Det här ekvation för att räkna receipt.
        Totalprice = Calculate(Netprice, percentage, &Vatprice);
        // Det här ekvation för att skriva ut ditt receipt.
        Printreceipt(Netprice, percentage, Vatprice, Totalprice);

        // Det här uttalanden för att  avsluta programet.
        cout << "Press Y to continue or N to exit: " << endl << endl;
        /* om I fall att användarinmatningen fel väljer programmet
         * * tvinga användaren att skriva bara y or n alfabet.*/
        do
            cin >> Ch;//läsa "Ch" alfabet från tangentbord.
        while (!( toupper(Ch) == 'Y' || toupper(Ch) == 'N' ));
        //När användar tryka Y, stänga programet.
        trunOff = (toupper(Ch) == 'Y') ? true : false;
        if (trunOff) break; else continue;
    }
    return 0;
}
//definition funktionen för att räkna receipt.
double Calculate(double Netprice, double percentage, double *Vatprice){
    percentage *= 0.01;  //räkna Moms% percentage.
    *Vatprice = Netprice * percentage;//räkna Moms från Netto och Moms%.
    return Netprice * (1 + percentage );//räkna Värde från Netto och Moms% percentage.
}

void Printreceipt(double Netprice, double percentage, double Vatprice, double Totalprice){

    // spara de aktuella inställningarna
    ios::fmtflags old_settings = cout.flags(); // spara tidigare formatflaggor.
    int old_precision = cout.precision(); // spara tidigare precisionsinställningar.

    /* ändra utgångsformatinställningar med medlemsfunktioner*/
    cout.setf (ios :: fixed, ios :: floatfield); // Ställ in fast flytande format.
    cout.precision (2); // för fast format, två decimaler.

    /* Det här uttalanden för att skriva ut receipt som format {00000000.00}.
     * dubblar i fast format, precision 2, bredd 8*/

    cout << "Net Price    : " << setw(8) << Netprice   << " Kr" << endl;
    cout << "Percentage   : " << setw(8) << percentage << " %"  << endl;
    cout << "Vat Price    : " << setw(8) << Vatprice   << " Kr" << endl;
    cout << "Total Price  : " << setw(8) << Totalprice << " Kr" << endl << endl;

    cout << "Control      : "<< "WASA123456789012" << endl
         << "Orgnr        : 123456-0010" << endl << "Transaction  : 33221"<< endl
         << "Control code : "<<"OTX56656JMVBXDTYMMJKN;WASA2019"<< endl << endl
         << "Thanks for visiting and welcome again!" << endl
         << "Save the customer's receipt!" <<endl <<endl<< endl;

    // återställa utdataflaggor och precision
    cout.flags (old_settings);
    cout.precision (old_precision);

}
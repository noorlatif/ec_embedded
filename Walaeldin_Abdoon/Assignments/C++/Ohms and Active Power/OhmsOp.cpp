/*
+________________________________  Programming information   ______________________________+
|  *  File name               : OhmsOp.cpp                                                 |
|  *  Header File             : OhmsOp.h                                                   |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/14/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program based on our classes of Ohms and Powers, 2 classes |
|                               have a multi private/public  "data/functions" members and  |
|                               constructor, destructor class functions.                   |
|                                                                                          |
|                               the base class it's Ohms class have three data members and |
|                               9 member functions, that data members are private and all  |
|                               member functions are public so the power class it's shared |
|                               all the public member functions from Ohms class, the power |
|                               class also has a three functions extra inside public part. |
|                               additional both classes have a tow constructor/destructor  |
|                               functions.                                                 |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
// Preprocessor directives
#include "OhmsOp.h"
#include <math.h>

/*Define member functions outside the Ohms/Power classes.*/

//Define class function member to set current into private data member.
void Ohms::Setcurrent(double Curr){
    Current = Curr;
}
//Define class member function to set Voltage into private data member.
void Ohms::Setvoltage(double Volt){
    Voltage = Volt;
}
//Define class member function to set Resistance into private data member.
void Ohms::Setresistance(double Resi){
    Resistance = Resi;
}
// class function member to get Voltage from private data member.
double Ohms::Getcurrent(){
    return Current;
}
// class function member to get Voltage from private data member.
double Ohms::Getvoltage(){
    return Voltage;
}
// class function member to get Resistance from private data member.
double Ohms::Getresistance(){
    return  Resistance;
}
//Define class member function to calculates Voltage from private data member.
double  Ohms::Voltage_calculate(){
    return Getcurrent() * Getresistance(); // V = I*R
}
//Define class member function to calculates Resistance from private data member.
double  Ohms::Resistance_calculate(){
    double Temp= Getvoltage() / Getcurrent(); // R = V/R
    /* checked if the operation divided by zero or not,
     * because all the values divided by zero equal infinite
     * except 0/0 equal zero, the answer be equal zero in all cases.*/
    return (Getcurrent() != 0.0)? Temp:0.0;
}
//Define class member function to calculates Current from private data member.
double  Ohms::Current_calculate(){
    double Temp=Getvoltage() / Getresistance(); // I = V/R
    /* checked if the operation divided by zero or not,
     * because all the values divided by zero equal infinite
     * except 0/0 equal zero, the answer be equal zero in all cases.*/
    return  (Getresistance() != 0.0)? Temp:0.0;
}

// Define constructor function for initializes the data members.
 Ohms::Ohms(double Current, double Voltage, double Resistance){
    Setcurrent(Current);        // set current value.
    Setvoltage(Voltage);       // set voltage value.
    Setresistance(Resistance); //set Resistance value.
}
// Define destructor function to destroyed the class.
 Ohms::~Ohms(){
    Current    = 0;// set current value.
    Voltage    = 0;// set voltage value.
    Resistance = 0;//set Resistance value.
}

/* ________________________Powers Class member functions________________________*/

//Define class member function to calculates active power from private data member.
double  Powers::ActivePower_I_med_V(){
    return  Getvoltage()*Getcurrent();// P = I*V
}
//Define class member function to calculates active power from private data member.
double  Powers::ActivePower_I_med_R(){
    // P = I*V men V=I*R Så P=(I*I)*R
    double Temp=pow(Getcurrent(),2)*Getresistance();//P = (I*I)*R.
    /* checked if the operation divided by zero or not,
     * because all the values divided by zero equal infinite
     * except 0/0 equal zero, the answer be equal zero in all cases.*/
    return  (Getresistance() != 0.0)? Temp:0.0;
}
//Define class member function to calculates active power from private data member.
double  Powers::ActivePower_V_med_R(){
    // P = I*V men I=V/R Så P=(V*V)/R
    double Temp=pow(Getvoltage(),2)/Getresistance();// P = (V*V)/R.
    /* checked if the operation divided by zero or not,
     * because all the values divided by zero equal infinite
     * except 0/0 equal zero, the answer be equal zero in all cases.*/
    return  (Getresistance() != 0.0)? Temp:0.0;
}
// Define constructor function for initializes the data members.
Powers::Powers(double Current,double Voltage,double Resistance){
    // set current value into private data member in Ohms class.
    Setcurrent(Current);
    // set voltage value into private data member in Ohms class.
    Setvoltage(Voltage);
    // set Resistance value into private data member in Ohms class.
    Setresistance(Resistance);
}
// Define destructor function for destroyed class.
Powers::~Powers(){
    Setcurrent(0);
    Setvoltage(0);
    Setresistance(0);
}
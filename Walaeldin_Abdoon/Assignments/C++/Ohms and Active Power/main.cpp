/*
+________________________________  Programming information   ______________________________+
|  *  File name               : main.cpp                                                   |
|  *  Header File             : OhmsOp.h                                                   |
|  *  Source File             : OhmsOp.cpp                                                 |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/14/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program based on our classes of Ohms and Powers, 2 classes |
|                               have a multi private/public  "data/functions" members and  |
|                               constructor, destructor class functions.                   |
|                                                                                          |
|                               the base class it's Ohms class have three data members and |
|                               9 member functions, that data members are private and all  |
|                               member functions are public so the power class it's shared |
|                               all the public member functions from Ohms class, the power |
|                               class also has a three functions extra inside public part. |
|                               additional both classes have a tow constructor/destructor  |
|                               functions.                                                 |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
// Preprocessor directives.
#include <iostream>
#include <iomanip>
#include "OhmsOp.h"

using namespace std;
int main() {
    double current; // Declare current it's a local variable.
    double voltage; // Declare voltage it's a local variable.
    double resistance; // Declare resistance it's a  local variable.

    ios::fmtflags old_settings = cout.flags(); // Save old float point formatting.
    int old_precision = cout.precision(); //  Save old precision point formatting.

    // looping forever until the user press N.
    for (char Ch='Y';!(toupper(Ch) == 'N');)
    {
        cout << endl<< "This program calculates the Ohms and Active Power Lows..."
             << endl << endl<< endl;

        // read data from user and assigned into class member used object.

        cout << "enter a value of Current!" << endl;
        cin >> current;//read the user entry and set into local variable.
        cout << "enter a value of Voltage!" << endl;
        cin >> voltage;//read the user entry and set into local variable.
        cout << "enter a value of Resistance!" << endl;
        cin >> resistance;//read the user entry and set into local variable.

        // Set new floating point setting format.
        cout.setf (ios :: fixed, ios :: floatfield);
        // Set three decimals point format.
        cout.precision (3);

        // Declare |Ohms_Obj| object  and initialize objects.
        Ohms   Ohms_Obj {current,voltage,resistance};   // Constructor Called
        // Declare |Power_Obj| object and initialize a values.
        Powers Power_Obj {current,voltage,resistance};   // Constructor Called

        cout  << endl<< "the Ohms low calculation is ....."
        << endl<< "_________________________________" << endl << endl;

         // calculates ohms low from class member and display to user screen.
        cout << "Finally Resistance is " << setw(8) << Ohms_Obj.Resistance_calculate()
             << " ohms when Current is " << setw(8) << Ohms_Obj.Getcurrent()
             << " amper and Voltage is      " << setw(8) << Ohms_Obj.Getvoltage()
             << " volt." << endl;
        // calculates ohms low from class member and display to user screen.
        cout << "Finally Current is   " << setw(8) << Ohms_Obj.Current_calculate()
             << " amper when Resistance is " << setw(8) << Ohms_Obj.Getresistance()
             << " Ohms and Voltage is " << setw(8) << Ohms_Obj.Getvoltage()
             << " volt." << endl;
        // calculates ohms low from class member and display to user screen.
        cout << "Finally Voltage is   " << setw(8) << Ohms_Obj.Voltage_calculate()
             << " volt when Current is    " << setw(8) << Ohms_Obj.Getcurrent()
             << " amper and Resistance is  " << setw(8) << Ohms_Obj.Getresistance()
             << " ohms." << endl;

        cout  << endl<< "the Power low calculation is ....."
        << endl << "__________________________________" << endl << endl;

        // calculates the power low from class member and display to user screen.
        cout << "Finally Active Power is "<< setw(8) << Power_Obj.ActivePower_I_med_V()
             << " watt when Current is  "<< setw(8) << Power_Obj.Getcurrent()
             << " amper and Voltage is      "<< setw(8) << Power_Obj.Getvoltage()
             << " volt."<< endl;
        // calculates the power low from class member and display to user screen.
        cout << "Finally Active Power is "<< setw(8) << Power_Obj.ActivePower_V_med_R()
             << " watt when Voltage is  "<< setw(8) << Power_Obj.Getvoltage()
             << " volt and Resistance is   " << setw(8) << Power_Obj.Getresistance()
             << " ohms." << endl;
        // calculates the power low from class member and display to user screen.
        cout << "Finally Active Power is "<< setw(8) << Power_Obj.ActivePower_I_med_R()
             << " watt when Current is  "<< setw(8) << Power_Obj.Getcurrent()
             << " amper and Resistance is   " << setw(8) << Power_Obj.Getresistance()
             << " ohms." << endl;

         /* if In case the user entry is incorrect pressed the program
          * force the user to try only y or n alphabets. */
        cout  << endl << "Press Y to continue or N to exit: " << endl << endl;
        do
            cin >> Ch;//get the user entry and set into Ch local variable.
        while (!( toupper(Ch) == 'Y' || toupper(Ch) == 'N' ));
    }/*Destructors Called for |Ohms_Obj|, |Power_Obj| objects from the memory
      * without memory leaked >> Called Ohms_Obj.~Ohms() && Power_Obj.~Powers().
      * The objects reference is destroyed when its scope ends, which is generally after
      * the closing curly bracket } for the code block in which it is created.
      *
      * The object |Ohms_Obj,Power_Obj| is destroyed when the for loop block ends
      * because it was created inside the for loop block. */

    // reset the floating point format.
    cout.flags (old_settings);
    cout.precision (old_precision);

    return 0;
}
/*
+________________________________  Programming information   ______________________________+
|  *  File name               : OhmsOp.h                                                   |
|  *  Source File             : OhmsOp.cpp                                                 |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/14/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|  * Summary                  :                                                            |
|                               program based on our classes of Ohms and Powers, 2 classes |
|                               have a multi private/public  "data/functions" members and  |
|                               constructor, destructor class functions.                   |
|                                                                                          |
|                               the base class it's Ohms class have three data members and |
|                               9 member functions, that data members are private and all  |
|                               member functions are public so the power class it's shared |
|                               all the public member functions from Ohms class, the power |
|                               class also has a three functions extra inside public part. |
|                               additional both classes have a tow constructor/destructor  |
|                               functions.                                                 |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#ifndef OHMSLAG_OHMSOP_H
#define OHMSLAG_OHMSOP_H

 //Declare a base class
class Ohms
{
private:
    /* Private data.
     * Private keyword, means that no one can access
     * the class members declared private, outside that class.*/

    // class data members are private.
    double Current; // Declare current data.
    double Voltage; // Declare Voltage data.
    double Resistance; // Declare Resistance data.
public:
    /* Public members.
     *Public keyword, means all the class members declared under public will be
     * available to everyone. The data members and member functions
     * declared public can be accessed by other classes too. */

    // class member functions are public.
    double Getcurrent(); // Declare function for read current value.
    double Getvoltage(); // Declare function for read a voltage value.
    double Getresistance(); // Declare function for read a Resistance value.

    void Setcurrent(double Curr); // Declare function for write a current value.
    void Setvoltage(double Volt);// Declare function for write a voltage value.
    void Setresistance(double Resi); // Declare function for write a resistance value.

    double Voltage_calculate(); // Declare function for calculates a current value.
    double Resistance_calculate();// Declare function for calculates a voltage value.
    double Current_calculate(); // Declare function for calculates a resistance value.

    // Declare constructor function for initializes the data members.
    Ohms(double Current = 1E-10, double Voltage = 1E-10, double Resistance = 1E-10);
    // Declare destructor function for destroyed the class.
    ~Ohms();//Destructors will never have any arguments.
};

//Declare a class with shared public member functions from base class |Ohms|.
class Powers: public Ohms
{
public:
    /* Public members.
     *Public keyword, means all the class members declared under public will be
     * available to everyone. The data members and member functions
     * declared public can be accessed by other classes too. */

    // class functions member are public.

    // Declare function to calculates active power from private data member.
    double  ActivePower_I_med_V(); //member function for calculates and display the ActivePower.
    // Declare function to calculates active power from private data member.
    double  ActivePower_I_med_R(); //member function for calculates and display the ActivePower.
    // Declare function to calculates active power from private data member.
    double  ActivePower_V_med_R(); //member function for calculates and display the ActivePower.

    // Declare constructor function for initializes the data members.
    Powers(double Current=1E-10,double Voltage=1E-10,double Resistance=1E-10);
    /*Constructors are special class functions which performs initialization of every object.
     * The Compiler calls the Constructor whenever an object is created. Constructors initialize
     * values to object members after storage is allocated to the object.*/

    // Declare destructor function for distorted the class.
    ~Powers();//Destructors will never have any arguments.
    /*Destructor is a special class function which destroys the object as soon as the scope
     * of object ends. The destructor is called automatically by the compiler when the object
     * goes out of scope.*/
};
#endif //OHMSLAG_OHMSOP_H

/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                           20 October 2019                                              |
| OBS:  the Header file "10.7.h" found in the same path.                                 |                                                                                 |
| This C program file contain a main function for rövarspråk operation                   |
|                                                                                        |
| function declaretion:                                                                  |
| int checkedchar(char chr);                                                             |
| void ReadFromfile();                                                                   |
| void WriteTofile();                                                                    |
| void openfile(char Fname[],int modes);                                                 |
|_______________________________________________________________________________________*/

#include<stdio.h> 
#include<stdlib.h>
#include "10.7.h"

void main() 
{ 
    int Selections;
     system("color 3");
     while(1) 
       { 
           printf("\nplease choose one of option from the list below");
              printf("\n\t1:  For exit the program"); 
              printf("\n\t2:  For writing new massege as r\x94varspråk.");
              printf("\n\t3:  For reading old massege and transelate r\x94varspråk.");
              printf("\n\t4:  For delete a file\n");
              if(scanf("%d", &Selections) != 1)
                {
                  break;
                }
              else if (Selections == 1)
                {
                 break;                   
                }
               else if (Selections == 2)
                    {
                        printf("\nplease prass Ctrl + Z and Enter key after finsh the text\n");           
                        Fileoperations(1);
                    }
               else if (Selections == 3)
                    {
                        Fileoperations(0);  
                    }
               else if (Selections == 4)
                    {      
                        Fileoperations(2);    
                    }
                else printf("\nplease enter the number bitween 1 to 3\n");
       }
}
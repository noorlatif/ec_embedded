/*____________________Programmeringsuppgifter 6.2 |________________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                    |
|__________________________________________________________________________________|
|Skriv en funktion som beräknar vad en vara kostar, inklusive moms.                |
|Som parametrar skall funktionen fa dels prise· exklusive moms och dels momssatsen |
| uttryck i procent.                                                               |
|_________________________________________________________________________________*/

#include <stdio.h>
double inklusivemoms(double price, double percent){ return (price + price *(percent * 0.01) );}
void main()
{
    double price,percent,totalt; //locale variables declaretion.
    printf("Ange pris exklusive moms :\n");
    scanf("%lf", &price);//read the price from keybord
    printf("Ange momssatsen :\n");
    scanf("%lf", &percent);//read the tax from keybord
    totalt =  inklusivemoms(price, percent);//call the inklusivemoms function
    printf("Priset f\x94rra moms blev %0.3f Kr och Priset efter moms blir %0.3f Kr inklusive moms.",
    price, totalt);//print the price result befor and after tax
}
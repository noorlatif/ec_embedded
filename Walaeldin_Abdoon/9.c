/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 18 october 2019                                            |
| OBS:  the Header file "9.h" found in the same path.                                |
| This C program file contain a multiple functions for studant & course operations       |
| Function prototypes :                                                                  |
|                                                                                        |
| students operations function definions:                                                |
|                                                                                        |
|   void maximumpoints(int course,int students, int points[][])                          |
|   void minimumpoints(int course,int students, int points[][])                          |
|   float average(int course, int students, int points[][])                              |
|   float median(float Smax, float Smin)                                                 |
|   void printing(int course,int students, int points[][])                               |
|_______________________________________________________________________________________*/

#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include "9.h"
/*_______________________________________________________________________________________*/
int getrandom ()
{
  int upperLimit=100, lowerLimit=0, Number;
  Number = ((rand() % (upperLimit - lowerLimit + 1)) + lowerLimit);
  return Number;
}

void generatermatrices(int course,int students, int points[students][course]) 
{ 
    int inercount;
    for (int outercount=0; outercount < course; outercount++) 
    {
       for (inercount=0; inercount < students-2; inercount++)
             points[inercount][outercount] =  getrandom(); // get new random integer number.             
        points[inercount+1][outercount] = 0.0;  // set 0.0 before calculate the maximum point
        points[inercount+2][outercount] = 0.0;  // set 0.0 before calculate the minimum point
    }
}

//C Function to find the maximum or the largest element present in an student points array.
void maximumpoints(int course,int students, int points[students][course])
{ 
    int mcoun,gcoun; float maximum;//locale variables declaretion.
  for (mcoun = 0; mcoun < course; mcoun++) 
    {
      maximum = points[0][mcoun];//get frist element an array.
      /* Compare all the elements an array with the maximum */  
      for (gcoun = 0; gcoun < students-2; gcoun++) 
       if (points[gcoun][mcoun] > maximum) /*if the element an array les than maximum*/
       maximum  = points[gcoun][mcoun]; /*Swapp bitween maximum and element*/
        points[gcoun+2][mcoun] = maximum;         
    }
}
/*_______________________________________________________________________________________*/
//C Function to find minimum or the smallest element in an student points array.
void minimumpoints(int course,int students, int points[students][course])
{ 
    int mcoun,gcoun; float minimum;//locale variables declaretion.
  for (mcoun = 0; mcoun < course; mcoun++) 
    {
       minimum = points[0][mcoun];//get frist element an array.
       /* Compare all the elements an array with the minimum */  
       for (gcoun = 0; gcoun < students-2; gcoun++) 
        if (points[gcoun][mcoun] < minimum) /*if the element an array les than minimum*/
        minimum  = points[gcoun][mcoun]; /*Swapp bitween minimum and element*/
        points[gcoun+1][mcoun] = minimum;   
    }
}
/*_______________________________________________________________________________________*/
//definition function to calculate the average value for students in all course.
float average(int course, int students, int points[students][course])
{
   float sum=0;
   for (int acounts = 0; acounts < students-2; acounts++)
       sum += points[acounts][course];
   return sum / (students-2);
}
/*_______________________________________________________________________________________*/
float median(float Smax, float Smin)
{ 
   return (Smax + Smin ) / 2;
}
/*_______________________________________________________________________________________*/
void printing(int course,int students, int points[students][course])
{ 
    
     time_t t;                           //locale variable declaretion.
     srand((unsigned) time(&t));         // Initailzation random function.
     float mediator,maximum,minimum,avg;
     generatermatrices(course,students,points);
     
     maximumpoints(course,students,points);
     minimumpoints(course,students,points);
     int outercount,inercount;
    printf("\nlists for all the student points and courses is:\n");
	for (outercount = 0; outercount < course; outercount++)
	{   
        printf("\ncourse number  is : %d",outercount+1);
        printf("\ncourse maximum is : %d points", 100);
        printf("\ntotal  student is : %d Students", students-2);
        printf("\nstudent points is :\n\t\t\t");
		for (inercount = 0; inercount < students-2; inercount++)
             printf("%d\t",points[inercount][outercount]);
             printf("\n");

             avg      = average(outercount, students, points);
             maximum  = points[inercount+2][outercount];
             minimum  = points[inercount+1][outercount] ;
             mediator = median(maximum,minimum); 

             printf("\nthe minimum point on the course [%d] is :  %3.2f", outercount+1, minimum);
             printf("\nthe maximum point on the course [%d] is :  %3.2f", outercount+1, maximum);
             printf("\nthe median  point on the course [%d] is :  %3.2f", outercount+1, mediator);
             printf("\nthe average point on the course [%d] is :  %3.2f", outercount+1, avg);
             printf("\n");
    }

}
/*_______________________________________________________________________________________*/
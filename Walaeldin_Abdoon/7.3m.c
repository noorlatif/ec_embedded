
/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                                                                                        |
| OBS:  the Header file "7.3.h" found in the same path.                                  |
| This main C program file for calculate circle,triangle, rectangle                      |
|                                                                                        |
|_______________________________________________________________________________________*/ 

#include<stdio.h>
#include "7.3.h"

int main()
{
    int Selections; 
    double radies, twidth, theight, rwidth, rheight, areas;

     while(1) //loop som inte bryts förens användaren väljer att avsluta.
       { 
           printf("\nYou can choose one of this options from the list below to calculate the areas of :\n");
           printf("1:  For exit application\n2:  For calculate circle area\n");
           printf("3:  For calculate triangle area\n4:  For calculate rectangle area\n");
        if(scanf("%d", &Selections) != 1) //Om användaren anger "eof", avslutas programmet
         {
            break;
         }
        else if (Selections == 1)
               {
                 break;                   
               }
         
        else if (Selections == 2)
               {
                 printf("\nPlease enter the radies of the circle?\n");
                 scanf("%lf",&radies);
                 areas = calccircle(radies);
                 printf("Circle area is: %lf\n",areas);
               }
        else if (Selections == 3)
               {
                 printf("\nPlease enter the width and height of triangle?\n");
                 scanf("%lf",&twidth);
                 scanf("%lf",&theight);
                 areas = calctriangel(twidth,theight);
                 printf("Triangle area is: %f\n",areas);
               }
        else if (Selections == 4)
               {
                 printf("\nPlease enter the width and height of rectangle?\n");
                 scanf("%lf",&rwidth);
                 scanf("%lf",&rheight);
                 areas = calcrectangel(rwidth,rheight);
                 printf("Rectangle area is: %lf\n",areas);
               }
        else printf("\nPlease enter the number bitween  this range 1 to 4\n");
       }
  return 0;
}

/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 18 october 2019                                            |
| OBS:  the Header file "9.h" found in the same path.                                |
| This C program file contain a multiple functions for studant & course operations       |
|                                                                                        |
| 1. generatermatrices function to make generate random student point.                   |
| 2. maximumpoints function to get the maximum student points.                           |
| 3. minimumpoints function to get the minimum student points.                           |
| 4. average function to calculete the average points for all students at the same corse.|
| 5. median function to calculete the median points for all students at the same corse.  |
| 6. printing function to print the result for all student at the same class.            |
|                                                                                        |
|_______________________________________________________________________________________*/ 

#include<stdio.h>
#include <stdlib.h>
#include "9.h"

void main()
{
    system("color 3");
    int students, course; //locale variables declaretion.

    printf("\nthis application well be generate automatic random students points and courses.\n");

    printf("\nplease enter the total number of students is:\t");
    scanf("%d",&students);//get student data from keybord.
    printf("\nplease enter the total number of  course is:\t");
    scanf("%d",&course);//get course data from keybord.
    if (students <= 1)//check the student well be more than one.
     {
         printf("\nplease enter more than one student:\t");
         scanf("%d",&students);
     }
    int points[students+2][course]; // pluse 2 at store maximum, minimum an the 2 last elements.
    printing(course,students+2, points); // pluse 2 at store maximum, minimum an the 2 last elements.
}
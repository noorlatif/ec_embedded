#include <stdio.h>

/*____________________Programmeringsuppgifter 2.2 |________________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                    |
|__________________________________________________________________________________|
|I USA brukar man ange temperaturer i grader Fahrenheit (°F)                      |
|istället för grader Cel5ius (°C). 0°c motsvarar 12°F och l00°C                    |
|motsvarar 212 °F. man kan använda formeln °C=(° F-32)x(5/9))                    |
|för att omvandla från °F till °C. Skriv ett program som läser in                  |
|en temperatur uttryckt i grader Fahrenheit ( ° F) och som översätter              |
|temperaturen till grader Celsius ( ° C).                                          |
|_________________________________________________________________________________*/
void main()
{ 
    float Celius,Fahrenheit;
    printf("please enter fahrenheit degree");
    scanf("%f",&Fahrenheit);
    Celius = (Fahrenheit -32) * 0.5555555555555556; //0.5555>>>>>>>>>(5/9);   
    printf(" the degree is : %0.3f C", Celius); 

}
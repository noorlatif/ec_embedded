#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, kv , kub;
    printf("\n\ttalet\t\tkvadrat\t\tkubik\n");
    for (i=1;i<13;i++)
    {
        kv=i*i;
        kub=i*i*i;
        printf("\n\t%d\t\t%d\t\t%d",i,kv,kub);
    }
}

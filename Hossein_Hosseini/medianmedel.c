#include <stdio.h>
#include <stdlib.h>
double averagef (int list[], int N);    // average prototype
double medianf(int list[], int N);      // median prototype

int main() // Hossein Hosseini
{
    int klass_a[]={12,15,22,24,30,31,35,36,39,42}; //***************** getting the numbers for a and b
    int klass_b[]={10,13,16,18,23,26,28,34,37,49};

    double all[4][2];

    double median_a, median_b;
    double average_a = averagef(klass_a,10); //*************************calling the average function
    double average_b = averagef(klass_b,10);

    all[0][0]= average_a;
    all[0][1]= average_b;

    printf("\nresults of class A : 12,15,22,24,30,31,35,36,39,42");
    printf("\nresults of class B : 10,13,16,18,23,26,28,34,37,49\n\n");

    printf("\naverage of klass A is %lf  and average of klass B is %lf\n",all[0][0],all[0][1]);

    median_a = medianf(klass_a,10);         //*************************calling the median function
    median_b = medianf(klass_b,10);

    all[1][0]= median_a;
    all[1][1]= median_b;

    printf("\nmedian of klass A is %lf   and median of klass B is %lf\n",all[1][0],all[1][1]);
}


//*************************************************************median function
double medianf(int list[], int N)
{
if (N%2 == 0)
{
    return (  (list[N/2]+list[(N/2)-1] )/2   );
}
else
{
    int round= ceil(N/2);
    return (list[round-1]);
}
}
//*****************************************************************************



//************************************************************** average function
double averagef (int list[], int N)
{
double sum;
int i;
for (i=0; i<N ; i++)
{
    sum = sum + list[i];
}
return (sum/N);
}
//********************************************************************************

#include <stdio.h>
#define LEN 20
struct car{
    char brand[LEN];
    char type[LEN];
    int year;
    float price;
};

void CarInfo(struct car a);

int main()
{


    struct car car1={"Volkswagen", "sedan", 2017, 120000.0};
    struct car car2={"Volvo", "suv", 2018, 160000.0};
    
    CarInfo(car1);
    CarInfo(car2);

   return 0;
}


void CarInfo(struct car a){
    printf("------ Car Information ------\n");
    printf("Name: %s\n", a.brand);
    printf("Type: %s\n", a.type);
    printf("Year: %d\n", a.year);
    printf("Price: %f\n", a.price);
};

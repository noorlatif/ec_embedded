/*
 *  This code is developed for program exercise 6.2.
 *
 *  Created on: Sep 24, 2019
 *      Author: leanmaker
 */

#include <stdio.h>

float calprice(float netprices, int taxRate);//Declare calprice function

int main()
{
	float netprice;	//Declare net price and tax rate variables
	int rate, select;	// Declare tax rate and selection value in the menu
	float result;	// Declare the calculate float price

	printf("Type 1 to calculate price, EOF to abort: ");	//Display the menu

	while(1){

		// Condition to control if user wants to proceed
		if (scanf("%d", &select) != 1){
			break;
		}
		else{
			printf("Please input the net price: ");
			scanf("%f", &netprice);

			printf("Please input tax rate in percentage: ");
			scanf("%d", &rate);
			
			result = calprice(netprice, rate);

			printf("The final price with tax is: %.2f kr \n", result);
			}

		printf("Type 1 to calculate price, EOF to abort: ");

		}

	return(0);
}

/* calprice take a float parameter as the net price,
 * and integer parameter as the tax rate, and returns a float
 * as the final price.
 */

float calprice(float netprices, int taxRate)
{
    // Calculate the total price and return the result.

	return netprices*(1+(float)taxRate/100);
}
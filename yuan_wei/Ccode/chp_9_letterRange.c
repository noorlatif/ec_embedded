#include <stdio.h>

int main()
{
    char str;
    
    printf("Please your letter from [A-Z] to [a-z]: ");
    
    scanf("%c", &str);
    
    if ((str>64&&str<91) || (str>96&&str<123)){
        
        printf("Your letter is right and letter is %c", str);
    }
    else{
        printf("Your letter is not within this range!");
    }
    
    
    return 0;
}

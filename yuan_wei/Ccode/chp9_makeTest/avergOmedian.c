#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "func.h"


int main ( ){
	int a[] = {20,15,26,18,30,22,23,24};
	int b[] = {11,24,15,27,19,30,21,25};
	
	int all[4][2];
	
	all[0][0] = average(a, 8);
	all[0][1] = average(b, 8);
	all[1][0] = median(a, 8);
	all[1][1] = median(b, 8);
	
	printf("\tA\tB");
	printf("\n");
	
	printf("average\t");	
	for (int j=0; j< 2; j++){
		printf("%d\t", all[0][j]);
	}
	
	printf("\nmedian\t");	
	for (int j=0; j< 2; j++){
		printf("%d\t", all[1][j]);
	}
	printf("\n");
	
}

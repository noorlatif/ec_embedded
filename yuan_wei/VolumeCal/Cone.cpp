//
// Created by Yuan Wei on 2019-11-20.
//

#include "Cone.h"
#include <cmath>
#include <iomanip>
#define PI 3.14

Cone::Cone(float r, float h, std::string name) {
    this -> radius = r;
    this -> height = h;
    this -> name = name;
}
float Cone::area() {
    return PI * radius * ( sqrt(pow(radius,2 ) + pow(height, 2)) + radius );
}
float Cone::volume() {
    return (PI / 3.0) * pow(radius, 2) * height;
}
void Cone::setRadius(float r) {
    radius = r;
}
void Cone::setHeight(float h) {
    height = h;
}

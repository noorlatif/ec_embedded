//
// Created by Yuan Wei on 2019-11-23.
//

#include "Shape.h"

#ifndef VOLUMECAL_RECTANGLE_H
#define VOLUMECAL_RECTANGLE_H


class Rectangle: public Shape {
public:

public:
    Rectangle() = default;
    Rectangle(float l, float w, float h, std::string name);
    float area() override;
    float volume() override;
    void setLen(float l);
    void setWid(float w);
    void setHeight(float h);
};


#endif //VOLUMECAL_RECTANGLE_H

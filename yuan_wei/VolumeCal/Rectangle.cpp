//
// Created by Yuan Wei on 2019-11-23.
//

#include "Rectangle.h"
#define PI 3.14;


Rectangle::Rectangle(float l, float w, float h, std::string name){
    this -> length = l;
    this -> width = w;
    this -> height = h;
    this -> name = name;
}
float Rectangle::area(){
    return (width*length + width*height + length*height)*2;
}
float Rectangle::volume(){
    return length*width*height;
}
void Rectangle::setLen(float l){
    length = l;
}
void Rectangle::setWid(float w){
    width = w;
}
void Rectangle::setHeight(float h){
    height = h;
}



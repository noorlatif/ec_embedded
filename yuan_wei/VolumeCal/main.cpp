#include <iostream>
#include "Sphere.h"
#include "Cone.h"
#include "Rectangle.h"

using namespace std;

void menu();	// To display menu
void process();	// To perform the case when a case is selected



int main(){
    char cont;

    // The do-while to run process at least once
    do{
        menu();
        process();
        cout << "Do another operation? (y or n)" << endl;
        cin >> cont;

    }while(cont=='Y' || cont=='y');

    return 0;
}



// Menu function used for displaying the functions and operations

void menu(){
    cout << endl;
    cout << "¤¤¤¤¤¤¤¤¤  Volume Calculation Menu ¤¤¤¤¤¤¤¤¤" << endl;
    cout << " 1. Calculate a Sphere Volume" << endl;
    cout << " 2. Calculate a Sphere Area" << endl;
    cout << " 3. Calculate a Cone Volume" << endl;
    cout << " 4. Calculate a Cone Area" << endl;
    cout << " 5. Calculate a Box Volume" << endl;
    cout << " 6. Calculate a Box Area" << endl;
    cout << " 0. Exit the program " << endl;
    cout << "Please enter the number of the operation: ";
}

// Process function use switch to match the case when corresponding
// action is selected
void process() {
    int choice;
    float radius;
    float height;
    float length;
    float width;
    Sphere a1;
    Cone b1;
    Rectangle c1;

    cin >> choice;
    switch (choice) {
        case 0:
            exit(0);

            // To calculate the voltage
        case 1:
            cout << fixed;
            cout.precision(2);
            cout << "Enter radius: ";
            cin >> radius;
            a1.setRadius(radius);
            cout << "The sphere volume is: " << a1.volume() << endl;
            break;
        case 2:
            cout << fixed;
            cout.precision(2);
            cout << "Enter radius: ";
            cin >> radius;
            a1.setRadius(radius);
            cout << "The sphere area is: " << a1.area() << endl;
            break;
        case 3:
            cout << fixed;
            cout.precision(2);
            cout << "Enter radius: ";
            cin >> radius;
            cout << "Enter height: ";
            cin >> height;
            b1.setRadius(radius);
            b1.setHeight(height);
            cout << "The volume of the cone is: " << b1.volume() << endl;
            break;
        case 4:
            cout << fixed;
            cout.precision(2);
            cout << "Enter radius: ";
            cin >> radius;
            cout << "Enter height: ";
            cin >> height;
            b1.setRadius(radius);
            b1.setHeight(height);
            cout << "The volume of the cone is: " << b1.area() << endl;
            break;
        case 5:
            cout << fixed;
            cout.precision(2);
            cout << "Enter length: ";
            cin >> length;
            cout << "Enter width: ";
            cin >> width;
            cout << "Enter height: ";
            cin >> height;
            c1.setLen(length);
            c1.setWid(width);
            c1.setHeight(height);
            cout << "The volume of the box is: " << c1.volume() << endl;
            break;
        case 6:
            cout << fixed;
            cout.precision(2);
            cout << "Enter length: ";
            cin >> length;
            cout << "Enter width: ";
            cin >> width;
            cout << "Enter height: ";
            cin >> height;
            c1.setLen(length);
            c1.setWid(width);
            c1.setHeight(height);
            cout << "The area of the box is: " << c1.area() << endl;
            break;

            // A default case when nothing right is selected
        default:
            cout << "Invalid choice.";
    }
}
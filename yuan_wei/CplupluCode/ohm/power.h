/* power header file contains power class. The fields are declared as private.
 * The default contructor and another with parameteres are used to create
 * an object automatically. The setter set the fields and getter caculate
 * and return the calcualted effect.
 * 
 * Author: Yuan Wei
 */

#ifndef OHM_POWER_H
#define OHM_POWER_H


class power {
private:	// The class fields are declared as private
    float voltage;
    float ampere;
public:	// Constructors and functions ae declared as public
    power();
    power(float v, float i);	// Parametrized contructor

    void setV(float v);	// Setter to set voltage
    void setI(float i);	// Setter to set the current
    float getPower(float v, float i);	// Getter to get the power given parameters
};


#endif


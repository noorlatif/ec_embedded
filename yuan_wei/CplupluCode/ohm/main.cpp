/* Main file contains a menu and process function which perform the
 * choosed actions.
 *
 * Author: Yuan Wei
 */
#include <iostream>
#include <cstdlib>
#include "ohm.h"
#include "power.h"
using namespace std;

void menu();	// To display menu
void process();	// To perform the case when a case is selected

int main(){
    char cont;

    // The do-while to run process at least once	
    do{
        menu();
        process();
        cout << "Do another calculation? (y or n)" << endl;
        cin >> cont;

    }while(cont=='Y' || cont=='y');

    return 0;
}

	// Menu function used for displaying the functions and operations

void menu(){
    cout << "         Main Menu " << endl;
    cout << endl;
    cout << "======== Ohm's Law ========" << endl;
    cout << " 1. Voltage " << endl;
    cout << " 2. Resistance " << endl;
    cout << " 3. Current " << endl;
    cout << "======== Effect Law ========" << endl;
    cout << " 4. Wattage " << endl;
    cout << " 0. Exit the program " << endl;
    cout << "Please enter the number of the operation: ";
}

// Process functon use switch to match the case when corresponding
// action is selected
void process() {
    int choice;
    float current, volt, resis;
    ohm a1;
    power p1;

    cin >> choice;
    switch (choice) {
        case 0:
            exit(0);

	// To calculate the voltage
        case 1:
            cout << "Enter current: ";
            cin >> current;
            cout << "Enter resistance: ";
            cin >> resis;
            a1.setI(current);
            a1.setR(resis);
            cout << "The voltage is: " << a1.getV(current, resis) << " voltage" << endl;
            break;

	// To calculate the current
        case 2:
            cout << "Enter voltage: ";
            cin >> volt;
            cout << "Enter resistance: ";
            cin >> resis;
            a1.setV(volt);
            a1.setR(resis);
            cout << "The current is: " << a1.getI(volt, resis) << " ampere" << endl;
            break;

	// To calculate the resistance

        case 3:
            cout << "Enter voltage: ";
            cin >> volt;
            cout << "Enter current: ";
            cin >> current;
            a1.setV(volt);
            a1.setI(current);
            cout << "The resistance is: " << a1.getR(volt, current) << " ohm" << endl;
            break;

	// To calculate the effect
        case 4:
            cout << "Enter voltage: ";
            cin >> volt;
            cout << "Enter current: ";
            cin >> current;
            p1.setV(volt);
            p1.setI(current);
            cout << "The effect is: " << p1.getPower(volt, current) << " w" << endl;
            break;

	// A default case when nothing right is selected
        default:
            cout << "Invalid choice.";
    }
}

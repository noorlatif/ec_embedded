/* power file contains the implementation for the power class.
 *
 * Author: Yuan Wei
 */

#ifndef POWER_H
#define POWER_H


#include "power.h"

power::power(){}
power::power(float v, float i){
    voltage=v;
    ampere=i;
}

void power::setV(float v){
    voltage=v;
}
void power::setI(float i){
    ampere=i;
}
float power::getPower(float v, float i){
    return v*i;
}


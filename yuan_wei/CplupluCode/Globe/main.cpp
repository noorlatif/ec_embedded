/* Main file contains the menu and process function which control
 * and interact with inputs. 
 * Author: Yuan Wei
 */
#include <iostream>
#include "Sphere.h"

using namespace std;

void menu();	// To display menu
void process();	// To perform the case when a case is selected



int main(){
    char cont;

    // The do-while to run process at least once
    do{
        menu();
        process();
        cout << "Do another operation? (y or n)" << endl;
        cin >> cont;

    }while(cont=='Y' || cont=='y');

    return 0;
}



// Menu function used for displaying the functions and operations

void menu(){
    cout << "         Main Menu " << endl;
    cout << endl;
    cout << "¤¤¤¤¤¤¤¤¤¤¤ Globe Menu ¤¤¤¤¤¤¤¤¤¤¤" << endl;
    cout << " 1. Create a Globe " << endl;
    cout << " 2. Change the Radius " << endl;
    cout << " 3. Calculate the Volym " << endl;
    cout << " 4. Calculate the Area" << endl;
    cout << " 0. Exit the program " << endl;
    cout << "Please enter the number of the operation: ";
}

// Process function use switch to match the case when corresponding
// action is selected
void process() {
    int choice;
    static int radius1;
    static int radius2;
    static Sphere a1;


    cin >> choice;
    switch (choice) {
        case 0:
            exit(0);

            // To calculate the voltage
        case 1:
            cout << "Enter radius: ";
            cin >> radius1;
            a1.radius = radius1;
            cout << "The globe is created!: " << a1.radius << endl;
            break;
        case 2:
            cout << "Enter the changed radius: ";
            cin >> radius2;
            a1.setR(radius2);

            cout << "The old radius is: " << radius1 << "The new radius is: " << a1.radius << endl;
            break;
        case 3:
            cout << fixed;
            cout.precision(2);
            cout << "The volume of the globe is: " << a1.getVol() << endl;
            break;
	case 4:
            cout << fixed;
            cout.precision(2);
            cout << "The area of the globe is: " << a1.getAre() << endl;
            break;

            // A default case when nothing right is selected
        default:
            cout << "Invalid choice.";
    }
}

// Sphere class contains the class fields and funcions.
// Created by Yuan Wei on 2019-11-18.
//

#ifndef GLOBE_SPHERE_H
#define GLOBE_SPHERE_H


class Sphere {
public:
    int radius;
public:
    Sphere();
    Sphere(int r);


    void setR(int r);
    int getR();

    float getVol();

    float getAre();

};


#endif //GLOBE_SPHERE_H

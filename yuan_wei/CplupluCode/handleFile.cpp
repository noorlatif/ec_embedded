/* A simple code to demo using fstream class
 * and error handling in c++.
 * Author: Yuan Wei
 *
 */
#include<iostream>
#include<fstream>
#include<string>

using namespace std;

int main(){
    string text;
    string textToRead;
    try{

        ofstream myFile;    // Create file object to write
        myFile.open("handlefile.txt");
            // Error handling when file is not created
            if(myFile){
                ;
            }
            else{
                // Throw error message
                cout << "file doen't exist!";
                throw "file erorr";
            }

        // Write 2 line texts to file
        myFile << "First line to the file.";
        myFile.close();

        myFile.open("handlefile.txt", ios::app); // Append the file without overwriting
        myFile << "\nSecond line to the file.";
        myFile.close();

        // Read file contents with a bad file name and run the correct in catch block
        textToRead = "badfile.txt";
        ifstream readFile(textToRead);
        if(readFile.is_open()){
            while(getline(readFile, text)){
                cout << text << '\n';
            }
        readFile.close();
        }
        else{
            throw "Unable to open file!";
        }

    }catch(...){
        // Display error message
        cout << textToRead <<" file is not created! \n";
        // Provid correct file name
        ifstream readFile("handlefile.txt");
        //validate file object
        if(readFile.is_open()){
            // Validate text contents and display
            while(getline(readFile, text)){
                cout << text << '\n';
            }
        readFile.close();
        }
    }
}

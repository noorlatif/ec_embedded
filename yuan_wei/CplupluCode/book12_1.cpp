#include <iostream>

using namespace std;

typedef struct {
    string title;
    string author;
    int pageNum;
    float price;
} book;

void showBook (book* boken){
    cout << "The book's title is: " << boken->title <<"\n";
    cout << "The book's author is: " << boken->author<<"\n";
    cout << "The book's total pages are: " << boken->pageNum<<"\n";
    cout << "The book's price is: " << boken->price<< " kr." <<"\n\n";
}

int main(){
    book book1;
    book book2;

    book1={"A Jorney To the West", "Chenen Wu", 2000, 20.5};
    book2={"Harry Poter", "J. K. Rolling", 500, 10.3};

    book* ptr1 = &book1;
    book* ptr2 =&book2;

    showBook(ptr1);

    cout << "======================================\n\n";

    showBook(ptr2);

    return 0;
}

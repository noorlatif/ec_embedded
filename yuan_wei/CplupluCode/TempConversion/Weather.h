// Weather class header file.
// Created by Yuan Wei on 2019-11-26.
//
#include <string>

#ifndef TEMPCONVERSION_WEATHER_H
#define TEMPCONVERSION_WEATHER_H


class Weather {
private:
    std::string status;
    float temperature;
    float wind;
public:
    Weather() = default;

    Weather(std::string stat, float temp, float wind);

    void setStat(std::string stat);

    void setTemp(float temp);

    void setWind(float wid);

    float fahreToCel(float a);

    void showData();

};


#endif //TEMPCONVERSION_WEATHER_H

/* Funcion.cpp contains the overloaded functions.
 * Author: Yuan Wei
 */
 #include<string>
 #include"func.h"

public int calcu(int x, int y){
    return x+y;
 }

public float calcu(float x, float y){
    return x+y;
 }

public string calcu(string x, string y){
    return x + " " + y;
 }

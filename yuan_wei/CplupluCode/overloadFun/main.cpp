/* Main.cpp
 * This code makes use an overload calcu function to
 * return result for different data types.
 * Author: Yuan Wei
 */
#include(iostream)
#include(string)
#include"func.h"
using namespace std;

 int main (){
 
    int x=5, int y=6;
    float a=4.00, float b=7.00;
    string d="How are ";
    string e="you?";
    cout << "The result of 2 integers: " << calcu(x,y) << "n\";
    cout << "The result of 2 floats: " << calcu(a,b) << "n\";
    cout << "The result of 2 strings: " << calcu(d,e) << "n\";

    return 0;
 }

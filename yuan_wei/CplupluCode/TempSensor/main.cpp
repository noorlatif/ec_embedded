#include <iostream>
#include <string>
#include "TempSensor.h"

using namespace std;

void menu();	// To display menu
void process();	// To perform the case when a case is selected


int main() {
    char cont;
    // The do-while to run process at least once
    do{
        menu();
        process();
        cout << "Do another run? (y or n)" << endl;
        cin >> cont;

    }while(cont=='Y' || cont=='y');

    return 0;
}

void menu(){
    cout << "         Main Menu " << endl;
    cout << endl;
    cout << " 1. Create a temperature sensor with default values " << endl;
    cout << " 2. Create a temperature sensor with custom values " << endl;
    cout << " 3. Raise temperature " << endl;
    cout << " 4. Decrease temperature" << endl;
    cout << " 0. Exit the program " << endl;
    cout << "Please enter the number of the operation: ";
}

// Process function use switch to match the case when corresponding
// action is selected
void process() {
    int choice;
    int sensorID, battery;
    string modelName;
    TempSensor a1;
    TempSensor a2;
    TempSensor b1;
    TempSensor b2;

    cin >> choice;
    switch (choice) {
        case 0:
            exit(0);

            // To display default object values
        case 1:

            cout << "The default object is: " << endl;
            a1.showData();
            break;

            // To display customized object values
        case 2:

            cout << "Enter sensor ID number: ";
            cin >> sensorID;
            cout << "Enter sensor battery level: ";
            cin >> battery;
            cout << "Enter sensor model name: ";
            cin >> modelName;
            a2 = {sensorID, battery, modelName};
            cout << "The customized object is: " << endl;
            a2.showData();
            break;

            // To increase battery level and display info
        case 3:

            cout << "Enter battery level: ";
            cin >> battery;
            b1.setPower(battery);
            b1.increasePower();
            cout << "The increased battery level: " << endl;
            b1.showData();
            break;

            // To decrease battery level and display info
        case 4:

            cout << "Enter battery level: ";
            cin >> battery;
            b2.setPower(battery);
            b2.decreasePower();
            cout << "The decreased battery level: " << endl;
            b2.showData();
            break;

            // A default case when nothing right is selected
        default:
            cout << "Invalid choice.";
    }
}
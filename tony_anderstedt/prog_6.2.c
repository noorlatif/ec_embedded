#include <stdio.h>
#include <stdlib.h>

//Declare fucntion
float calcSalesTaxes(double price, double salesTaxInProcent);

int main() {
	
	// Declare some variables
	int price, salesTaxInProcent;

	// Tell the user what info you seek
	printf("Varans pris (exkl. moms)? ");

	// Get the value from user
	scanf("%d", &price);
	
	// Tell the user what info you seek
	printf("\nMomssats? ");

	// Get the value from user
	scanf("%d", &salesTaxInProcent);

	// Call function to calculate taxes
	double result = calcSalesTaxes(price, salesTaxInProcent);
	
	// Tell the user the result!
	printf("\nPriset med moms blir: %.2f", result);
	
	return 0;
}

float calcSalesTaxes(double price, double salesTaxInProcent) {
	
	// Calculate price with taxes
	return price * (1 + (salesTaxInProcent/100));
	
}

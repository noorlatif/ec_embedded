#include <iostream>
#include <string>

using namespace std;

class TempSensor
{
    public:
    TempSensor()
    {
        sensor_id += 1;
        this->consumption = 0;
        model = "favoritmodell";
    }
    private:
    int consumption;
    public:
    int sensor_id = 1000;
    string model;

    int read()
    {
        return this->consumption;
    }
    void changeConsumption(int con)
    {
        this->consumption = con;
    }
    void increaseCon()
    {
        this->consumption += 2;
    }
    void decreaseCon()
    {
        if(this->consumption >= 2)
        {
            this->consumption -= 2;
        }
        else
        {
            this->consumption = 0;
        }    
    }
    void print()
    {
        cout << "\nSensor ID = " << sensor_id << endl << "Model: " << model
             << endl << "Battery consumption: " << consumption << "\n\n";
    }
};

int main()
{
    TempSensor obj1;
    int choice;
    while(1)
    {
        cout << "Make your choice!\n\n";
        cout << "[1]Print out stats.\n";
        cout << "[2]Increase consumption.\n";
        cout << "[3]Decrease consumption.\n";
        cout << "[4]Change consumption.\n";
        cout << "[5]Exit.\n";
        cout << "Enter: ";   
        cin >> choice;
        switch(choice)
        {
            case 1:
                obj1.print();
                break;
            case 2:
                obj1.increaseCon();
                cout << "\nCurrent consumption = " << obj1.read() << endl;
                break;
            case 3:
                obj1.decreaseCon();
                cout << "\nCurrent consumption = " << obj1.read() << endl;
                break;
            case 4:
                cout << "\nEnter your choise of consumption: ";
                int con;
                cin >> con;
                obj1.changeConsumption(con);
                cout << "\nCurrent consumption = " << obj1.read() << endl;
                break;
            case 5:
                break;
            default:
                cout << "\nUnvalid Input! Try again." << endl;
        }
        if(choice == 5)
        {
            cout << "\n\nThank you for using this program!\n";
            break;
        }

    }
    return 0;
}
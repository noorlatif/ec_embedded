//Created by Alexander Broscheit, 19-11-25.
#include <iostream>
#include <iomanip>

using namespace std;
//Creating a class to convert fahrenheit to celsius, store the value of
class Fahren  //fahrenheit and celsius and print out value.
{
    private:
    double fahren = 0;
    double celsius = 0;
    public:
    void setFahren(double fahrenheit)
    {
        this->fahren = fahrenheit;
    }
    double convert()
    {
        this->celsius = (this->fahren - 32) * 5 / 9;
        
        return this->celsius;
    }
    void print()
    {
        cout << fixed << setprecision(2) << this->fahren << " degrees fahrenheit = "
             << fixed << setprecision(2) << this->celsius << " degrees celsius." << endl;
    }
};

int main()
{    
    while(1) //First loop to continue the program until
    {        //the user choses to exit.
        cout << ("Enter degrees in Fahrenheit: ");
        double fahrenheit = 0;
        cin >> fahrenheit;
        while(cin.fail())
        {  //Second loop to eliminate any chance for the program
            cin.clear();     //to crash by unvalid input.
            cin.ignore(INT_MAX, '\n');
            cout << "Numbers are allowed only! Try again." << "\n";
            cout << ("Enter degrees in Fahrenheit: ");
            cin >> fahrenheit; 
        }
        Fahren obj; //Creating an object
        obj.setFahren(fahrenheit);
        double celsius = obj.convert();
        obj.print();
        //Varible type double, in case of decimal input
        double choice;

        while(1) //Third loop to secure the input
        {
            cout << "\n[1]Continue\n[2]Exit\n";
            cout << "Enter: ";
            cin >> choice;
            if(choice == 1)
            {
                break;
            }
            else if(choice == 2)
            {
                cout << "You chosed to exit!";
                break;
            }
            else if(cin.fail()) //If input us unvalid
            {
                cin.clear();
                cin.ignore(INT_MAX, '\n');
                cout << "Only 1 or 2 is a valid input. Try again";
                continue;
            }
            else //If input is valid but not 1 or 2
            {
                cout << "Only 1 or 2 is a valid input. Try again";
                continue;
            }
        }   
        if(choice == 1) //continue the program
        {
            continue;
        } 
        else //exit program
        {
            break;
        }   
    }
    return 0;
}
#include <iostream>
#include <string>

using namespace std;

class Parent
{
    public:
    string name1;
    string name2;
    int age1;
    int age2;
    void set(string n1, string n2, int a1, int a2)
    {
        name1 = n1;
        name2 = n2;
        age1 = a1;
        age2 = a2;
    }

    void printParent()
    {
        cout << "Parents are " << name1 << " and " << name2 << ". They are " << age1
             << " and " << age2 << " years old." << endl; 
    }
};
class Child : public Parent
{
    public:
    Child(string n3, int a3)
    {
        name3 = n3;
        age3 = a3;
    }
    string name3;
    int age3;
    
    void printChild()
    {
        cout << "Childs name is " << name3 << " and he's " << age3 << " years old." << endl;
    }
};

int main()
{
    Child obj1("Alexander Broscheit", 22);
    obj1.set("Irmi Broscheit", "Lasse Broscheit", 54, 62);
    obj1.printChild();
    obj1.printParent();

    return 0;
}
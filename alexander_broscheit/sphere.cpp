#include <iostream>
#include <cmath>

using namespace std;

class Sphere
{
    private:
    int radius;
    double pi = 3.14159265359;
    public:    
    Sphere() {};
    Sphere(int r)
    {
        if(r < 0)
        {
            this->radius = 0;
        }
        else
        {
            this->radius = r;
        }    
    };
    void setradius(int r)
    {
        if(r < 0)
        {
            this->radius = 0;
        }
        else
        {
            this->radius = r;
        }    
    }
    double volume()
    {
        double v;
        v = 4 / 3 * this->pi * pow(this->radius, 3);
        return v;
    }

};

int main()
{
    Sphere sp1(-2);
    //sp1.setradius(4);
    double v = sp1.volume();
    cout << v << endl;

    return 0;
}
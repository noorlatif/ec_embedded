#include <stdio.h>
#include <time.h>
void delay(double number_of_seconds) 
{
    double milli_seconds = 1000 * number_of_seconds; 
  
    clock_t start_time = clock(); 
  
    while (clock() < start_time + milli_seconds); 
}
int main()
{
    while(1)
    {
        printf(".");
        delay(0.4);
        printf(".");
        delay(0.4);
        printf(".");
        delay(1.1);
        printf("_");
        delay(1.1);
        printf("_");
        delay(1.1);
        printf("_");
        delay(0.4);

    }
}
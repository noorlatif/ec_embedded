#include<stdio.h>
#include<stdlib.h>

/* 
Programmeringsuppgift 4.2 (sida 90)
Ett program som skriver ut en tabell för talen 1 till 12. På varje rad
skall talet,talet i kvadrat och talet i kubik skrivas ut.
*/

/* Psuedo kod
Skapa int som skall skrivas ut, samt värde för int i kvadrat och kubik.
Skapa for loop som itererar upp till 12 gånger och sätter tal till +1
loop = sätt  värde på root och kubik med nya tal++
loop = skriv ut int \n, int i kvadrat \n, och int i kubik \n.
 */

int main ()
{

//deklarationer
int tal,square,cubic = 0;

//tabell som skriver 1-12 samt rot&kvadrat för varje #
for (int i=1;i<=12;i++)
    {
        tal++;
        square = (tal*tal);
        cubic  = (tal*tal*tal);
        printf("Number: %d ",tal);
        printf("Square: %d  ",square);
        printf("Cubic: %d \n",cubic);
    }

return 0;
}
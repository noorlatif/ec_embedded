#include<stdio.h>
#include<stdlib.h>

/* Morese simuleringsuppgift
Uppgift om morsesignal i en oöndlig loop, ligger under filer
Uppgift simulering inbyggt system
Gör ett program som snurrar till strömmen bryts
Börja med att skriva det i pseduokod och visualisera flödet
Skriv kod som simulerar blinkande dioder (utskrifter just nu)
Få programmet att skriva ut SOS med morse . . ._ _ _ . . . _ _ _
*/

int main ()

//simple while som printar morse för evigt
{
   
	while (1) 
    {
		printf(". ");
		printf(". ");
		printf(". ");
		printf("_ ");
		printf("_ ");
		printf("_ ");
		printf(". ");
		printf(". ");
		printf(". \n");
	}

	return 0;



}
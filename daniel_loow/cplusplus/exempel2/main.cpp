/*
 *---------------------------------------------------------------
 * File: exempel 2
 * Summary: make a class.
 * default constructor and parameter constructor
 * sets value on two object with two different ways
 * Version: 1.0
 * Owner: Daniel lööw
 ---------------------------------------------------------------
*/
#include <iostream>
#include <string>
class Person
{
public:
    std::string name;
    int age;
    float salary;
    //default constructor
    Person(){};
    //parameter constructor
    Person(std::string cName, int cAge, float cSalary)
    {
        name = cName;
        age = cAge;
        salary = cSalary;

    }
    //method to set data
    void setName(std::string cName)
    {
        name = cName;
    }
    void setAge(int cAge)
    {
        age = cAge;
    }
    void setSalary(float cSalary)
    {
        salary = cSalary;
    }
    //method to show data
    void showData()
    {
        std::cout << "name is " << name << " age is " << age << " salary is " <<salary;
    }
};


int main()
{
    // define two object
    Person p1,p2;
    //call member function to set data
    p1.setName("daniel");
    p1.setAge(38);
    p1.setSalary(45000);
    //call member function to show data
    p1.showData();
    //make object and set data
    Person p3("peter",22,23000);
    std::cout << std::endl;
    p3.showData();
    return 0;
}
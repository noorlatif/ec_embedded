//
// Created by daniel on 2019-11-14.
//
#ifndef TELEFONBOK_PHONEBOOK_H
#define TELEFONBOK_PHONEBOOK_H
class PhoneBook {
private:
    std::string firstName, lastName, address;
    //city is hardcoded change if another region
    std::string city = "orebro";
    std::string phoneNumber;

public:
    //default constructor
    PhoneBook(){}
    //parameter constructor
    PhoneBook(std::string cFirstName, std::string cLastName, std::string cAddress, std::string cPhoneNumber)
    {
        firstName = cFirstName;
        lastName = cLastName;
        address = cAddress;
        phoneNumber = cPhoneNumber;
    }
    //member function setcity. default is orebro but if you are in anothor region call this function
    //in function.cpp under addNewPersonToPhoneBook()
    void setCity (std::string cCity);

    //member function show data
    void showData();
    //method print input on file. "phonebook.txt
    void WriteToFile();
};
#endif //TELEFONBOK_PHONEBOOK_H

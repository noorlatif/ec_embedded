//
// Created by Daniel Lööw. on 2019-11-29.
//
#include "Power.h"
#include "functions.h"
#include <cmath>

double Power::GetCurrent() {
    return this->current;
}
double Power::GetResistance() {
    return this->resistance;
}
double Power::GetVoltage(){
    return this->voltage;
}
double Power::GetCos(){
    return this->cos;
}

void Power::SetVoltage(double voltage){
    this->voltage = voltage;
}
//string takes what you want to calculate ex. "resistance"
//"current" "cos" and value is value for resistance,current or cos
void Power::inputCurrent() {
    do{
        current = secureInput();
    }while(!validatingValue("current",current));
}

void Power::inputResistance(){
    do {
        resistance = secureInput();
    }while(!validatingValue("resistance", resistance));
}
//this is special in two calculations you can only input volt >400 so it
//can use "current" validating
void Power::inputVoltage(){
    do{
        voltage = secureInput();
    }while(!validatingValue("current",voltage));
}

void Power::inputCos(){
    do{
        cos = secureInput();
    }while(!validatingValue("cos",cos));
}

//calculations, ohmslaw,effectlaw,,voltageAmpere
double Power::multiplyTwoValue(double Value1, double Value2) {
    return  Value1 * Value2;
}
//calculate totalresistans
double Power::TotalResistance(double Resistance1, double Resistance2, double Resistance3){
    double totalResistance=(1/Resistance1) + (1/Resistance2) + (1/Resistance3);
    totalResistance = 1/totalResistance;
    return totalResistance;
}
//calculate Active average power
double Power::ActiveAveragePower(double Voltage, double Current, double PowerFactor){
    return Voltage * Current * PowerFactor;
}
//calculate apparent effect
double Power::ApparentEffect3Phase(double Voltage, double Current){
    return Voltage * Current * sqrt(3);
}
//calculate
double Power::ActiveEffect3Phase(double Voltage, double Current, double Cos){
    return Voltage * Current * sqrt(3) * Cos;
}
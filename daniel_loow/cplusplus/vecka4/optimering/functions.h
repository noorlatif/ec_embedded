//
// Created by danie on 2019-11-28.
//

#ifndef OPTIMERING_FUNCTIONS_H
#define OPTIMERING_FUNCTIONS_H

#include <string>
//#include <iostream>
using namespace std;
//********************************************************************nya
// validating value for current(amp) <400, resistance(ohm) <200000 and
//power factor(cosinus) <0 & >0
//string takes what you want to calculate ex. "resistance" "current" "cos"
//value is value for resistance,current or cos
//validating value so its not exced 20000 in ohm and 400 in current and voltage
bool validatingValue(string check, double value);

//secure input so it only takes digits.
double secureInput();

//meny choice
void ohmsLaw();
void totalResistance();
void effectLaw();
void apparentEffect();
void activeAveragePower();
void apparentEffect3Phase();
void activeEffect3Phase();
#endif //OPTIMERING_FUNCTIONS_H

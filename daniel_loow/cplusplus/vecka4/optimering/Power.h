//
// Created by Daniel Lööw on 2019-11-29.
//
#ifndef OPTIMERING_POWER_H
#define OPTIMERING_POWER_H
class Power {
private:
    //variabels for calculating
    double resistance = 0;
    double current = 0;
    double voltage = 0;
    double cos = 0;

public:
    //default constructor
    Power() = default;
    //methods for getting variabels
    double GetCurrent();
    double GetResistance();
    double GetVoltage();
    double GetCos();
    //method for setting voltage when input can be higher than 400
    void SetVoltage(double voltage);
    //methods for input variabels and validating them from function.cpp
    void inputCurrent();
    void inputResistance();
    void inputVoltage();
    void inputCos();
    //methods for calculations
    static double multiplyTwoValue(double Value1, double Value2);
    static double TotalResistance(double Resistance1, double Resistance2, double Resistance3);
    static double ActiveAveragePower(double Voltage, double Current, double PowerFactor);
    static double ApparentEffect3Phase(double Voltage, double Current);
    static double ActiveEffect3Phase(double Voltage, double Current, double Cos);
};
#endif //OPTIMERING_POWER_H

cmake_minimum_required(VERSION 3.15)
project(sphere)

set(CMAKE_CXX_STANDARD 17)

add_executable(sphere main.cpp Sphere.cpp Sphere.h)
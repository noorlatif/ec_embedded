/*
 Files: main.cpp , Sphere.cpp, Sphere.h
 Created by Daniel Lööw
 Date 2019-11-18.
 Calculate program for volym and area on sphere
 Version 1.0
*/
#include "Sphere.h"
#include <cmath>
//metod för volymen
double Sphere::CalculateVolume(){
    double volume;
    volume = 4 * M_PI * pow(this->radius, 3) / 3;
    return volume;
}
//metod för arean
double Sphere::CalculateArea()
{
    double area;
    area = 4 * M_PI * pow(this->radius, 2);
    return area;
}
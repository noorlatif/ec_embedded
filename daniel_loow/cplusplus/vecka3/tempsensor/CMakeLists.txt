cmake_minimum_required(VERSION 3.15)
project(tempsensor)

set(CMAKE_CXX_STANDARD 17)

add_executable(tempsensor main.cpp TempSensor.cpp)
//
// Created by danie on 2019-11-20.
//
#include <iostream>
#include "TempSensor.h"
#include <string>

int TempSensor::SensorId =1001;

void TempSensor::IncreaseBatteryConsumption()
{
    this->batteryConsumption +=2;
}
void TempSensor::ReduceBatteryConsumption()
{
    if(GetBatteryConsumption() == 0)
    {
        this->batteryConsumption = 0;
    }
    else{
        this->batteryConsumption -=2;
    }
}

void TempSensor::ShowAllInfo()
{
    std::cout << "Model is:" << this->model << " Sensor id is:" << GetSensorId()
              << " Battery consumption is:" << this->batteryConsumption <<std::endl;
}

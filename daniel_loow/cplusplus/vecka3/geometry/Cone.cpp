//
// Created by danie on 2019-11-20.
//

#include "Geometry.h"
#include "Cone.h"
#include <cmath>
//function to calculate volume in cone
double Cone::CalcVolumeCone(){
    double volume = (M_PI * pow(radius,2) * height) /3;
    return volume;
}
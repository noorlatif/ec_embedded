//
// Created by danie on 2019-11-20.
//

#ifndef GEOMETRY_SPHERE_H
#define GEOMETRY_SPHERE_H

//inharitance from geometry
class Sphere: public Geometry{
public:

    double CalcVolumeSphere();
};


#endif //GEOMETRY_SPHERE_H

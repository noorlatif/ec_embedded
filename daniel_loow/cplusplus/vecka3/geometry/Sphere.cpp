//
// Created by danie on 2019-11-20.
//

#include "Geometry.h"
#include <iostream>
#include "Sphere.h"
#include <cmath>
// calculate volume for sphere
double Sphere::CalcVolumeSphere(){
    double volume;
    volume = 4 * M_PI * pow(radius, 3) / 3;
    return volume;
}


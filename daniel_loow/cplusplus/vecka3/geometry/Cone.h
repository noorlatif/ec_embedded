//
// Created by danie on 2019-11-20.
//

#ifndef GEOMETRY_CONE_H
#define GEOMETRY_CONE_H

//inharitance from geometry
class Cone: public Geometry{
public:
    double CalcVolumeCone();
};


#endif //GEOMETRY_CONE_H

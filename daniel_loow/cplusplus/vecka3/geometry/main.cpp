/*File: main.cpp,cone.ccp/.h, geometry.cpp/.h , sphere.cpp/h.
 * description: Calculate volume on sphere and cone.
 * Created: 2019-11-20
 * By Daniel Lööw
 * Version 1.0
 *
 */

#include <iostream>
#include "Geometry.h"
#include "Sphere.h"
#include "Cone.h"

int main() {
    int radius_cm,height_cm;
    Sphere calcSphere;
    //calculate sphere
    std::cout << "Enter radius to calculate volym of sphere: ";
    std::cin >> radius_cm;
    calcSphere.SetRadius(radius_cm);
    std::cout <<"volume in sphere is: "<< calcSphere.CalcVolumeSphere();
    std::cout <<std::endl;
    //calculate cone
    Cone calcCone;
    std::cout << "Enter radius to calculate volym of cone: ";
    std::cin >> radius_cm;
    std::cout <<std::endl;
    std::cout << "Enter height to calculate volym of cone: ";
    std::cin >> height_cm;
    calcCone.SetRadius(radius_cm);
    calcCone.SetHeight(height_cm);
    std::cout << "volume in cone is: "<<calcCone.CalcVolumeCone();









     return 0;
}
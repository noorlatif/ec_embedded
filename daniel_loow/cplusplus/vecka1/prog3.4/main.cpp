//**************************************************
// File: prog3.4
// Summary: program to check if personal number and
// gender is correct
// Version: 2.0 /c++
// Owner: Daniel Lööw
//**************************************************
#include <iostream>
using namespace std;

//bool function compare if hender and personal number match
bool checkIfWoman(int gender, long long int personNumber)
{
    int result = (personNumber/10);
    result = result % 2;
    if(gender == result)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int main()
{   //initialization variables for gender and personal number
    long long int personNumber;
    int gender;
    cout << "Enter if you are a 0 for man and 1 for woman: ";
    cin >> gender;
    //checks if user input if 0 or 1.
    while(1)
    {   //check if input is a int.
        if(cin.fail() || gender < 0 || gender > 1)
        {
            cin.clear();
            cin.ignore(1000,'\n');
            cout<<"You have entered wrong input\n"<< "0 for man and 1 for woman: " <<endl;
            cin>>gender;
        }//breaks loop if gender = int or 1 or 0
        if((gender == 0 || gender == 1) && !cin.fail() )
            break;
    }
    //question for user to input personal number check if it is a long long int.
    cout << "Enter personal number, 10 digits, without -: ";
    while(!(cin >> personNumber))
    {
        cin.clear();
        cin.ignore(1000,'\n');
        cout << "invalid input\n" << "Enter personal number, 10 digits, without -: ";
        cin >> personNumber;
    }
    //call function for check if user input from gender and personal number is equal.
    int result = checkIfWoman(gender,personNumber);
    if(result)
    {
        cout << "right!";
    }
    else
        cout << "not right";
    return 0;
}
//Lööw
#include<stdio.h>
#include<math.h>
#include "prog7.3.h"

//definierade funktioner för att räkna area på en cirkel.
double areaCircle(double radie)
{
	return PI * pow(radie,2);
}
//definierade funktioner för att räkna area triangel.
double areaTriangel(double hight, double base)
{
	return (base * hight)/2;;
}
//definierade funktioner för att räkna area rektangel.
double areaRectangle(double hight, double base)
{
	return base * hight;
}
#include<stdio.h>
#include "funchead.h"

//*****************************************************************************************

//function counting average value from students in iot,cad and server course.
float average(int col, int points[10][3])
{
   float result;
   for (int i = 0; i < 10; i++)
   {
       result = points[i][col] + result;
   }
   return result / 10;
}

//*****************************************************************************************

//function for sorting point in array and calculate median and printout sorted array and median.
void median(int col, int points[10][3])
{   //sorting algoritm for sorting students points on test. Smalest first biggest last.
    int temp;
	int size =10;
	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if(points[i][col] > points[j][col])
			{
				temp = points[i][col];
				points[i][col] = points[j][col];
				points[j][col] = temp;
			}	
		}
	}
	//printing sorted array. smallest first biggest last. 
    int i;
    printf("Sorted list of points in ascending order are.\n");
	for (i = 0; i < size; i++)
	{
		printf("%d\t", points[i][col]);
	}
    printf("\n");
    //calculating and printing median
    float median1 = points[4][col];
    float median2 = points[5][col];
    float resultMedian = (median1 + median2)/2;
    printf("Median is %0.2f points\n",resultMedian);
}

//*****************************************************************************************

//Function for printing result list and adds name of course, not sorted list
void printresult(char str[], int col, int points[10][3])
{
    for (int i = 0; i < 10; i++)
    {
        printf("Result for student%2d, %s %d points.\n",i+1,str,points[i][col]); 
    }
}
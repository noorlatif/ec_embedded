#include<stdio.h>
#include<math.h>

int main()
{
    double square,cubic;
    for (int i = 1; i < 13; i++)
    {
        square = pow(i,2);
        cubic = pow(i,3);
        printf("Number: %2d is in square: %5.1f and in cubic: %6.1f\n",i,square,cubic);
    }
}
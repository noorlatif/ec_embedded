#include "phone_book.h"
extern int array_position;

// This function will add a contact to the last position in passed array
void add_contact(contact *phone_book, string name, string phone_number) {
  contact *pContact = phone_book + array_position++;
  pContact->set_name(name);
  pContact->set_phone_num(phone_number);
}

// This function will remove the contact at passed number and move the rest of
// the contacts to fill in the gap in the array
void remove_contact(contact *phone_book, int number) {
  for (int i = number; i < array_position; i++) {
    phone_book[i] = phone_book[i + 1];
  }
  array_position--;
}

int print_phone_book(contact *phone_book) {
  if(array_position <= 0){
    cout << "Phone book is empty!\n";
    return 0;
  }
  for (int i = 0; i < array_position; i++) {
    // Print number
    printf("%3d ", i + 1);
    // Print contact
    cout << phone_book[i].get_name() << "\t" << phone_book[i].get_phone_num()
         << endl;
  }
  return 1;
}

void save_to_file(string file_name, contact *phone_book) {
  // Open file
  ofstream out_file;
  out_file.open(file_name, ios::out | ios::trunc);

  // Set array_position
  out_file << array_position << endl;

  // Write to file
  for (int i = 0; i < array_position; i++) {
    out_file << phone_book[i].get_name() << endl;
    out_file << phone_book[i].get_phone_num() << endl;
  }

  // Close file
  out_file.close();
}

void read_from_file(string file_name, contact *phone_book) {
  // Open file
  ifstream in_file;
  in_file.open(file_name, ios::in);

  // Set array_position
  in_file >> array_position;
  in_file.ignore();

  char name[100], number[100];
  // Read from file
  for (int i = 0; i < array_position; i++) {
    in_file.getline(name, 100);
    in_file.getline(number, 100);
    phone_book[i].set_name(name);
    phone_book[i].set_phone_num(number);
  }

  // Close file
  in_file.close();
}
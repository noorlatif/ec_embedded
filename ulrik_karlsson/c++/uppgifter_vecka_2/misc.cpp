#include "misc.h"

bool valid_input(string input, string *valid_inputs,
                 int number_of_valid_inputs) {
  // Loop through valid_inputs
  for (int i = 0; i < number_of_valid_inputs; i++) {
    // Return true if input is equal to current valid_inputs
    if (input.compare(valid_inputs[i]) == 0) {
      return true;
    }
  }
  return false;
}

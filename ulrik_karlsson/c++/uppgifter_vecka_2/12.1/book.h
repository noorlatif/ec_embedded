#include <iostream>
#include <string>

using namespace std;

class book {
public:
  string title, author;
  int pages, price;

  void print() {
    cout << "Title: " << title << endl;
    cout << "Author: " << author << endl;
    cout << "Pages: " << pages << endl;
    cout << "Price: " << price << " kr" << endl;
  };
};

#include "book.h"
#include <stdlib.h>

using namespace std;

int main() {
  system("chcp 1252");
  system("cls");

  // Crerate two books
  book c_fran_borjan;
  c_fran_borjan.title = "C fr�n B�rjan";
  c_fran_borjan.author = "Jan Skansholm";
  c_fran_borjan.pages = 408;
  c_fran_borjan.price = 249;

  book robinson = {"Robinson Cruse", "Daniel Defo", 500, 299};

  // Print books
  c_fran_borjan.print();
  cout << "=====================" << endl;
  robinson.print();

  return 0;
}
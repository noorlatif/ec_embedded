// ---------------------------------------------------------------
// File:    phone_book_app.cpp
// Summary: This program is storing a phone book with names and numbers
// Version: 1.1
// Owner:   Ulrik Karlsson
// ---------------------------------------------------------------
// Log: 2019-11-13 Created the file. Ulrik
//
// ---------------------------------------------------------------

#include <iostream>
#include <string>
#include "phone_book.h"
#include "misc.h"
#include <stdlib.h>
#include <fstream>
using namespace std;
int array_position = 0;

int main() {
  contact phone_book[100];
  int number_of_contacts;
  char name[100];
  char phone_number[100];
  char input[100];
  char action;
  string file_name = "phone_book.txt";
  
  // Load phone book
  read_from_file(file_name, phone_book);

  // Display welcome message
  cout << "\nWelcome to the phone book!\n";

  while (1) {
    // Display options
    cout << "\n0 = Save and exit\n1 = Add contact\n2 = remove contact\n3 = "
            "Show all contacts\n4 = Exit without saving\n";

    // Get input from user and store firs character in action
    cout << "\nEnter a number: ";
    cin.getline(input, 100);
    action = input[0];

    if (action == '0') {
      // Save and exit program
      save_to_file(file_name, phone_book);
      cout << "Save and exit program\n";

      exit(0);
    }

    switch (action) {

    case '1': // Add Contact
      cout << "Name: ";
      cin.getline(name, 100);
      cout << "Phone number: ";
      cin.getline(phone_number, 100);
      add_contact(phone_book, name, phone_number);
      cout << "\nAdded \"" << name << "\" to the phone book.\n";
      break;

    case '2': // Remove contact
      cout << "Which contact do you want to remove?\n";
      cout << "Enter 0 to list all contacts: ";
      while (1) {
        int contact_nr;
        cin >> contact_nr;
        cin.ignore();

        if (contact_nr == 0) {
          print_phone_book(phone_book);
        } else if (contact_nr > 0) {
          cout << "\nRemoved " << phone_book[contact_nr - 1].get_name() << endl;
          remove_contact(phone_book, contact_nr - 1);
          break;
        } else {
          cout << "Invalid input!";
        }
        cout << "Which contact do you want to remove?\n";
      }
      break;

    case '3': // Show all contacts
      // Clear screen and print contacts
      system("cls");
      print_phone_book(phone_book);
      break;

    case '4':
      cout << "Exit without saving";
      exit(1);

    default: // Invalid input
      // Clear screen and print error message
      system("cls");
      cout << "Invalid input! Please enter 0, 1, 2 or 3\n";
      break;
    }
  }
  return 0;
}
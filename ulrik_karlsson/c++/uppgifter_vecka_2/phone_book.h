#include "contact.h"
#include <stdio.h>
#include <fstream>

void add_contact(contact *pContact, string name, string phone_number);
void remove_contact(contact *phone_book, int number);
int print_phone_book(contact *phone_book);
void save_to_file(string file_name, contact *phone_book);
void read_from_file(string file_name, contact *phone_book);

#include <iostream>
#include <string>
using namespace std;

class contact {
private:
  string name;
  string phone_number;

public:
  void set_name(string set_name);
  void set_phone_num(string set_phone_num);
  string get_name();
  string get_phone_num();
};

#include "electricity.h"

// Constructor will set all values to 0
ohms_law::ohms_law() { u = r = i = p = 0; }

void ohms_law::reset() { u = r = i = p = 0; }

int ohms_law::calc_u() {
  if (r && i) {
    u = r * i;
    return 1;
  }
  if (p && i) {
    u = p / i;
    return 1;
  }
  if (r && p) {
    u = sqrt(r * p);
    return 1;
  }
  return 0;
}

int ohms_law::calc_i() {
  if (u && r) {
    i = u / r;
    return 1;
  }
  if (p && u) {
    i = p / u;
    return 1;
  }
  if (p && r) {
    i = sqrt(p / r);
    return 1;
  }
  return 0;
}

int ohms_law::calc_r() {
  if (u && i) {
    r = u / i;
    return 1;
  }
  if (p && i) {
    r = p / (i * i);
    return 1;
  }
  if (u && p) {
    r = (u * u) / p;
    return 1;
  }
  return 0;
}

int ohms_law::calc_p() {
  if (r && i) {
    p = r * (i * i);
    return 1;
  }
  if (u && r) {
    p = (u * u) / r;
    return 1;
  }
  if (u && i) {
    p = u * i;
    return 1;
  }
  return 0;
}

int ohms_law::set_u(string message) {
  cout << message;
  cin >> u;
  cin.ignore();
  return 0;
}

int ohms_law::set_r(string message) {
  cout << message;
  cin >> r;
  cin.ignore();
  return 0;
}

int ohms_law::set_i(string message) {
  cout << message;
  cin >> i;
  cin.ignore();
  return 0;
}

int ohms_law::set_p(string message) {
  cout << message;
  cin >> p;
  cin.ignore();
  return 0;
}

// void get_values(string message) {
//   // Print message
//   cout << "Du beh�ver ange tv� k�nda v�rden f�r att r�kna ut" << message
//        << ".\nL�mna f�ltet tomt och tryck Enter om du inte k�nner till "
//           "v�rdet.\n";

//   cout << "\nAnge resistansen (R): ";
//   cin >> r;
//   cin.ignore();
//   cout << "Ange str�mmen (I): ";
//   cin >> i;
//   cin.ignore();
//   cout << "Ange effekten (P): ";
//   cin >> p;
//   cin.ignore();
// }

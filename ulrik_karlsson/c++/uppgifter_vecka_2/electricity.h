#include <cmath>
#include <iostream>
#include "misc.h"
using namespace std;

class ohms_law {
public:
  double u, r, i, p;
  ohms_law();
  void reset();
  int calc_u();
  int calc_i();
  int calc_r();
  int calc_p();
  int set_u(string message);
  int set_r(string message);
  int set_i(string message);
  int set_p(string message);
};

// ---------------------------------------------------------------
// File:    ohms_law_app.cpp
// Summary: This is a program that can calculate electricity using Ohm's law
// Version: 1.1
// Owner:   Ulrik Karlsson
// ---------------------------------------------------------------
// Log: 2019-11-15 Created the file. Ulrik
//
// ---------------------------------------------------------------

/*
Ni ska g�ra ett program som r�knar ut ohms lag och effektlagen
Programmet ska fr�ga efter input, programmet v�ljer d�refter r�tt s�tt att r�kna
ut
det som �nskas
Programmet ska vara objektorienterat, det �r l�mpligt att ohms lag g�rs som en
klass
med sina metoder och effektlagen som en annan klass
Det ska finnas metoder f�r alla ber�kningar i respektive klass
Den h�r uppgiften �r fr�mst till f�r att ni ska forts�tta utveckla er och �va
men
detta m�ste man beh�rska f�r examinationen av denna kurs
K�nner ni att ni vill g�ra det riktigt utmanande s� kan ni �ven spara klasserna
i egna filer
*/

#include <stdlib.h>
#include "electricity.h"
using namespace std;

int main() {
  system("chcp 1252");
  system("cls");

  double result;
  string input, text, unit;
  string error_message = "F�r f� v�rden f�r att g�ra utr�kningen";
  const string U = "Sp�nningen (U)";
  const string R = "Resistansen (R)";
  const string I = "Str�mmen (I)";
  const string P = "Effekten (P)";
  char action;
  bool calc_success;

  // Create a new object
  ohms_law obj;

  while (1) {
    // Reset object
    obj.reset();

    system("cls");
    cout << "\n"
            "1 = Sp�nning\n"
            "2 = Resistans\n"
            "3 = Str�m\n"
            "4 = Effekt\n"
            "0 = Avsluta program\n";

    cout << "\nVad vill du r�kna ut?\nAnge ett nummer: ";
    getline(cin, input);
    action = input[0];

    // Exit program
    if (action == '0') {
      exit(0);
    }

    system("cls");
    cout << "Du beh�ver ange tv� k�nda v�rden f�r att g�ra utr�kningen.\nAnge "
            "0 om du inte k�nner till v�rdet.\n\n";

    switch (action) {
    // Voltage (U)
    case '1':
      // Set unit and text
      text = U;
      unit = "volt";

      obj.set_r(R + ": ");
      obj.set_i(I + ": ");
      obj.set_p(P + ": ");

      // Calculate u and store it in result
      if ((obj.calc_u()) == 1) {
        calc_success = true;
        result = obj.u;
      } else {
        calc_success = false;
      }
      break;

    // Resistance (R)
    case '2':
      // Set unit and text
      text = R;
      unit = "ohm";

      obj.set_u(U + ": ");
      obj.set_i(I + ": ");
      obj.set_p(P + ": ");

      // Calculate r and store it in result
      if (obj.calc_r()) {
        calc_success = true;
        result = obj.r;
      } else {
        calc_success = false;
      }
      break;

    // Current (I)
    case '3':
      // Set unit and text
      text = I;
      unit = "ampere";

      obj.set_u(U + ": ");
      obj.set_r(R + ": ");
      obj.set_p(P + ": ");

      // Calculate i and store it in result
      if (obj.calc_i()) {
        calc_success = true;
        result = obj.i;
      } else {
        calc_success = false;
      }
      break;

    // Effect (P)
    case '4':
      // Set unit and text
      text = P;
      unit = "watt";

      obj.set_u(U + ": ");
      obj.set_r(R + ": ");
      obj.set_i(I + ": ");

      // Calculate p and store it in result
      if (obj.calc_p()) {
        calc_success = true;
        result = obj.p;
      } else {
        calc_success = false;
      }
      break;

    default:
      system("cls");
      cout << "\nOgiltigt val!\n";
      system("pause");
      continue;
    }

    // Print result
    // ex.: Sp�nningen (U) �r 12 volt.
    if (calc_success) {
      cout << text << " �r " << result << " " << unit << "." << endl;
    } else {
      cout << error_message;
    }
    system("pause");
  }
  return 0;
}
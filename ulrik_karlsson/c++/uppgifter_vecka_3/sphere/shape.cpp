//
// Created by ulrik on 2019-11-21.
//

#include "shape.h"

void shape::print() {
    printName();
    printColor();
    printHeight();
    printLength();
    printWidth();
    printRadius();
}

void shape::setHeight(double height) {
    m_height = height;
    calcVolume();
    calcArea();
}

void shape::setLength(double length) {
    m_length = length;
    calcVolume();
    calcArea();
}

void shape::setWidth(double width) {
    m_width = width;
    calcVolume();
    calcArea();
}

void shape::setRadius(double radius)  {
    m_radius = radius;
    calcVolume();
    calcArea();
}


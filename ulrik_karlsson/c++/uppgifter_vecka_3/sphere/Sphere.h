//
// Created by ulrik on 2019-11-21.
//

#ifndef SPHERE_SPHERE_H
#define SPHERE_SPHERE_H

#include "shape.h"
#include <iostream>
#include <cmath>

class Sphere : public shape {
public:
//    Constructors
    Sphere() = default;
    Sphere(double radius, std::string name, std::string color);

//    Public methods
    void setSphere(double radius, std::string name, std::string color);
    void calcVolume();
    void calcArea();
    void print();
};


#endif //SPHERE_SPHERE_H

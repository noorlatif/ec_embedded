//
// Created by ulrik on 2019-11-21.
//

#include "Box.h"
// Constructor
Box::Box(double height, double length, double width,
         std::string name,std::string color) {
    setBox(height, length, width, name, color);
}

void Box::setBox(double height, double length, double width,
                 std::string name, std::string color) {
    m_height = height;
    m_length = length;
    m_width = width;
    m_name = name;
    m_color = color;
    calcVolume();
    calcArea();
}

void Box::calcVolume() {
    m_volume = m_height * m_length * m_width;
}

void Box::calcArea() {
    m_area = 2 * (m_length * m_width + m_length * m_height + m_width * m_height);
}

void Box::print() {
    printName();
    printColor();
    printHeight();
    printLength();
    printWidth();
    printVolume();
    printArea();
}


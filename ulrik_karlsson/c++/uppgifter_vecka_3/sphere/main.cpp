/*
1. Skapa ett program och l�gg till en ny klass till det med namnet Sphere.
a. Sphere-klassen ska ha en privat egenskap/attribut av typen heltal som
representerar sf�rens radie.
b. Skriv en defaultkonstruktor(tom), och �ven en konstruktor som tar en
parameter, sf�rens radie.
c. Skriv en egenskap som g�r det m�jligt f�r klassanv�ndaren att �ndra radien.
Validera
v�rde s� att negativa v�rdena f�r radien ska st�lla in radien till noll.
d. Skriv en metod som ber�knar sf�rens volym.
e. Testa din klass utifr�n Main-metoden. F�r att testa klassen kan du skapa en
eller fler objekt i klassen (i huvudmetoden) och �ndra radien b�da
med konstrukt�ren och med fastigheten och skriv ut det modifierade v�rdet p�
konsolf�nster. Ring din metod och ber�kna och skriva ut volymen p�
sf�r. G�r testet med minst tv� olika v�rden f�r radie. Du har
frihet att v�lja om du vill be anv�ndaren om inmatningsv�rden eller dig
vill testa det med n�gra f�rdefinierade, h�rdkodade v�rden.

*/

#include <iostream>
#include "Sphere.h"
#include "Box.h"
#include "Cone.h"

using namespace std;

int main() {
//    Create new sphere
    Sphere sphere;
//    Set sphere radius
    sphere.setRadius(11.5);
//    Print sphere
    sphere.print();
    cout<<endl;

//    Create another sphere
    Sphere sphere2(5, "Sphere 2", "Red");
//    Change sphere color
    sphere2.setColor("Blue");
//    Print sphere
    sphere2.print();
    cout<<endl;

//    Create new box and print info
    Box box(5,3,8,"Box 1", "Red");
    box.print();
    cout<<endl;

//    Create another box
    Box box2;
//    Print box
    box2.print();
    cout<<endl;
//    Set box values
    box2.setBox(6, 8, 3, "Box 2", "Purple");
//    Print box again
    box2.print();
    cout<<endl;
//    Change box height
    box2.setHeight(4);
//    Print box again
    box2.print();
    cout<<endl;

//    Create new cone and print info
    Cone cone(5, 9, "Cone 1", "Yellow");
    cone.print();

    return 0;
}
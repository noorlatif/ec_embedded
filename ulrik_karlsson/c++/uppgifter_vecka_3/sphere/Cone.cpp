//
// Created by ulrik on 2019-11-21.
//

#include "Cone.h"

Cone::Cone(double radius, double height, std::string name, std::string color) {
    setCone(radius, height, name, color);
}

void Cone::setCone(double radius, double height, std::string name, std::string color) {
    m_radius = radius;
    m_height = height;
    m_name = name;
    m_color = color;
    calcVolume();
    calcArea();

}

void Cone::calcVolume() {
    m_volume = (1.0 / 3.0) * M_PI * pow(m_radius, 2) * m_height;
}

void Cone::calcArea() {
    m_area = M_PI * m_radius * (m_radius + sqrt(pow(m_radius, 2) + pow(m_height, 2)));
}

void Cone::print() {
    printName();
    printColor();
    printRadius();
    printHeight();
    printVolume();
    printArea();
}

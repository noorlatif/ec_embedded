//
// Created by ulrik on 2019-11-21.
//

#ifndef SPHERE_SHAPE_H
#define SPHERE_SHAPE_H

#include <string>
#include <iostream>

class shape {
protected:
    std::string m_name = "Untitled";
    std::string m_color = "White";
    double m_height{};
    double m_length{};
    double m_width{};
    double m_radius{};
    double m_volume{};
    double m_area{};
//    Constructor
    shape() = default;

public:
//    Methods
    std::string getName() { return m_name; }
    std::string getColor() { return m_color; }
    double getHeight() { return m_height; }
    double getLength() { return m_length; }
    double getWidth() { return m_width; }
    double getRadius() { return m_radius; }
    double getVolume() { return m_volume; }
    double getArea() { return m_area; }

    void setName(std::string name) { m_name = name; }
    void setColor(std::string color) {m_color = color; }
    void setHeight(double height);
    void setLength(double length);
    void setWidth(double width);
    void setRadius(double radius);
    virtual void calcVolume() {}
    virtual void calcArea() {}

    void printName() {std::cout<<"Name: "<<m_name<<std::endl; }
    void printColor() {std::cout<<"Color: "<<m_color<<std::endl; }
    void printHeight() {std::cout<<"Height: "<<m_height<<std::endl; }
    void printLength() {std::cout<<"Length: "<<m_length<<std::endl; }
    void printWidth() {std::cout<<"Width: "<<m_width<<std::endl; }
    void printRadius() {std::cout<<"Radius: "<<m_radius<<std::endl; }
    void printVolume() {std::cout<<"Volume: "<<m_volume<<std::endl; }
    void printArea() {std::cout<<"Area: "<<m_area<<std::endl; }
    void print();
};


#endif //SPHERE_SHAPE_H

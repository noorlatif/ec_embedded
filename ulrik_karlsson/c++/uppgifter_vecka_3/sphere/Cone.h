//
// Created by ulrik on 2019-11-21.
//

#ifndef SPHERE_CONE_H
#define SPHERE_CONE_H

#include "shape.h"
#include <iostream>
#include <cmath>

class Cone : public shape {
public:
//    Constructors
    Cone() = default;
    Cone(double radius, double height, std::string name, std::string color);

//    Methods
    void setCone(double radius, double height, std::string name, std::string color);
    void calcVolume();
    void calcArea();
    void print();
};


#endif //SPHERE_CONE_H

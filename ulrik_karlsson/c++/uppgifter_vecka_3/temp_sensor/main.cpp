/*
2.Din uppgift �r att skriva en klass TempSensor enligt f�ljande beskrivning:

Anta att du vill skapa ett antal TempSensorobjekt av den klassen och tilldela
var och en av dem, ett serienummer (Id), b�rjar med 1001 f�r det f�rsta objektet.

Beskrivning av klassen:

Din klass inneh�ller information, inklusive ID, sensorns batterif�rbrukning
(ett heltal) och modell (kan vara en str�ng).
V�rdet p� instansvariabeln, str�mf�rbrukning initieras till noll i konstruktorn
f�r din klass. Initiera v�rdet p� modellen till din ?favoritmodell?.
Ge en egenskap f�r att l�sa och �ndra batterif�rbrukningen.
Ge en skrivskyddad egenskap som bara kan l�sa modellen (ingen �ndringsmetod).
Ge tv� metoder, �ka str�mf�rbrukning och minska batterif�rbrukning. �ka-metoden
l�gger till 2 till f�rbrukningen och minska metoden drar 2 fr�n f�rbrukningen.
(Om f�rbrukningen �r noll ska minska metoden inte p�verka f�rbrukningen).
Testa din klass i ett program. (Som f�rklarats h�gre upp, skapa n�gra objekt i
din klass, anropa olika metoder och skriv ut all information (inklusive ID-nummer)
i konsolf�nstret, eller helt enkelt ange en meny f�r anv�ndaren att v�lja mellan
olika alternativ (menyn �r inte ett krav!)).
(Tips: F�rklara en statisk variabel i din klass som sesor_Id och �ka id med ett i
din konstruktor. P� det s�ttet kan du h�lla reda p� antalet objekt som har
skapats, och du vet vilket ID du ska tilldela n�sta objekt)
*/


#include <iostream>
#include "TempSensor.h"
#include <cstdlib>

using namespace std;

int main() {
//    Set start id for TempSensor
    TempSensor::setGlobalId(1001);

//    Create array of TempSensors
    TempSensor sensorArr[10];

//    Create some more sensors
    TempSensor sensor2("SuperExtreme");

    TempSensor sensor3("SuperUltra");

    TempSensor sensor4;

    int input;
    while (1) {
//        Print sensors
        for (int i = 0; i < 10; ++i) {
            sensorArr[i].print();
            cout << endl;
        }
        sensor2.print();
        cout << endl;
        sensor3.print();
        cout << endl;
        sensor4.print();
        cout << endl;

        cout << "\n2 = Increase battery usage\n";
        cout << "1 = Decrease battery usage\n";
        cout << "0 = Exit\n\n";
        cout << "Modify sensor " << sensor4.getId() << endl;
        cin >> input;
        if (input == 2) { sensor4.increaseBatteryUsage(); }
        else if (input == 1) { sensor4.decreaseBatteryUsage(); }
        else if (input == 0) { break; }
        else {
            cout << "Invalid input!\n";
            system("pause");
        }
    }

    return 0;
}

/*Detta program �r en r�knare som kan anv�ndas f�r ell�ra med enbart v�xelsp�nningar och v�xelstr�mmar. R�knaren
tar upp r�kning med sp�nningar i volt(U), resistanser upp till 20 000/ohm(R).
Str�m upp till 440 Ampere(I), effekter P i watt(W). 3 faser upp till 400V mellan faserna.
�ven tar denna upp Skenbar effekt vid 3-fas och enfas, aktiv effekt vid 3-fas och enfas d�r cosinus fi/cosinus() anv�nds
som effektfaktorn som �r mindre �n 1 och inte mindre �n 0.
Frekvenser i (Hz):  och totala motst�ndet i parallellkopplade kretsar med max 3 motst�nd.

50 Hertz(Hz) Finns det i v�ra uttag i sverige Vid 50 Hz byter �sp�nningen polaritet och str�mmen riktning 100 g�nger per
sekund. Sp�nningen i svenska eluttag pendlar upp och ner fr�n -325 V till +325 V. Att vi talar om 230 V beror p� att det
�r sp�nningens(v�xelstr�m ~) effektivv�rde eller roten ur. Sp�nningen V(U)=Toppv�rdet/sqrt(2)=325V/sqrt(2).

OHMS LAG: Sp�nning i volt(U)=Resistans i ohm(R)*Str�m i ampere(I)
RESISTANSTOTAL PARALLELLA RESISTANSER: Rtot=1/R1R2R3
EFFEKTLAGEN ENKEL f�r likstr�m: Effekt i watt(P)=U*I
SKENBAR EFFEKT ENFAS ~: Skenbar(S/VA)=U*I
AKTIV EFFEKT/MEDELEFFEKT ENFAS ~:P=U*I*cos()
SKENBAR EFFEKT 3-FAS ~: Skenbar S/VA=U*I*sqrt(3)
AKTIV EFFEKT 3-FAS ~: P=U*I*sqrt(3)*cos()

*/
#include <cstdio>
#include <cstdlib>
#include "ElectricCalc.h"

using namespace std;

int main() {
  system("chcp 1252");
  system("cls");
  
  while (true) {
    printf("\n");
    int val;

    printf("V�lj vilka storheter du vill ber�kna:\n");
    printf("V�lj 1 f�r: OHMS LAG\n");
    printf("V�lj 2 f�r: Rtot\n");
    printf("V�lj 3 f�r: EFFEKTLAGEN ENKEL\n");
    printf("V�lj 4 f�r: SKENBAR EFFEKT ENFAS\n");
    printf("V�lj 5 f�r: AKTIV EFFEKT/MEDELEFFEKT ENFAS\n");
    printf("V�lj 6 f�r: SKENBAR EFFEKT 3-FAS\n");
    printf("V�lj 7 f�r: AKTIV EFFEKT 3-FAS\n");
    printf("V�lj 0 f�r: F�R ATT AVSLUTA\n");

//    H�mta val fr�n anv�ndaren
    val = getInt();

    switch (val) {
      case 0:     // AVSLUTA
        return 0;

      case 1: {   // OHMS LAG
        printf("Ohms lag sp�nningen(volt/V) bet�ckning U lika med Resistansen(Ohm) bet�ckning R \n");
        printf("g�nger Str�mmen(Ampere) med bet�ckningen I. Kort U=R*I. \n\n");
        cout << ElectricCalc::getVoltage() << " V\n";
        break;
      }
      case 2: {   // Rtot
        printf("Resistans sammankopplade i parallella kretsar �r lika med 1 delat Resistans R total �r lika med\n");
        printf("Resistans 1/R1 + 1/R2 + 1/R3 d� vi h�gst anv�nder tre resistanser.\n\n");
        cout << ElectricCalc::getTotalResistance() << " Ohm\n";
        break;
      }
      case 3: {   // EFFEKTLAGEN ENKEL
        printf("Effektlagen enkel f�r likstr�m �r effekten P i Watt (W) lika med sp�nningen U i volt(V)\n");
        printf("g�nger str�mmen I i Ampere(A): \n\n");
        cout << ElectricCalc::getEffect() << " W\n";
        break;
      }
      case 4: {   // SKENBAR EFFEKT ENFAS
        printf("Skenbar effekt enfas r�knas med storheten VA(VoltAmpere) som �r lika med sp�nningen U i volt(V)\n");
        printf("g�nger str�mmen I i ampere(A)\n\n");
        cout << ElectricCalc::getEffect() << " VA\n";
        break;
      }
      case 5: {   // AKTIV EFFEKT/MEDELEFFEKT ENFAS
        printf("Aktiv medeleffekt enfas �r lika med effekt P i watt(W) lika med sp�nningen U i volt(V) g�nger str�mmen I \n");
        printf("i Ampere g�nger cosinus fi/effektfaktor < 1:\n\n");
        cout << ElectricCalc::getEffectAverageSingle() << " W\n";
        break;
      }
      case 6: {   // SKENBAR EFFEKT 3-FAS
        printf("3-fas skenbar effekt �r v�xelsp�nning �r skenbar effekt S i voltampere(VA) lika med sp�nningen U i volt(V) \n");
        printf("g�nger str�mmen I i ampere(A) g�nger roten ur 3 SQRT(3).\n\n");
        cout << ElectricCalc::getEffect3Phase() << " VA\n";
        break;
      }
      case 7: {   // AKTIV EFFEKT 3-FAS
        printf("3-fas aktiv effekt �r effekten P i Watt(W) lika med sp�nningen U i volt(V) g�nger str�mmen I i ampere(A)\n");
        printf("g�nger cos < 1 && cos > 0 g�nger roten ur 3 SQRT(3).\n\n");
        cout << ElectricCalc::getEffectAverage3Phase() << " W\n";
        break;
      }
      default:
        printf("Fel alternativ f�rs�k igen! \n");
        break;
    }
  }
  return 0;
}

//
// Created by ulrik on 2019-11-29.
//

#include "ElectricCalc.h"

double ElectricCalc::res_tot(double r1, double r2, double r3) {
  double rtot = (1 / r1) + (1 / r2) + (1 / r3);
  return 1 / rtot;
}

// Prints message to screen and
// keeps asking for input til it gets a suitable value.
double ElectricCalc::askResistance(){
  printf("Skriv resistans R < 20 000ohm: \n ");
  return getValueRange(0, 20000);
}
double ElectricCalc::askCurrent(){
  printf("Skriv str�m I < 440 Ampere: \n");
  return getValueRange(0, 440);
}
double ElectricCalc::askVoltage(){
  printf("Skriv sp�nningen U i volt(V): \n ");
  return getDouble();
}
double ElectricCalc::askVoltageLimit(){
  printf("Skriv sp�nning U i volt(V) < 400V: \n ");
  return getValueRange(0, 400);
}
double ElectricCalc::askCosinus(){
  printf("Skriv in effektfaktorn cos > 0 && cos < 1:\n");
  return getValueRange(0,1);
}

// Prints message to screen and
// keeps asking for input til it gets a suitable value.
// Calculates and returns corresponding value.
double ElectricCalc::getVoltage() {
  return askResistance() * askCurrent();
}
double ElectricCalc::getTotalResistance() {
  return res_tot(askResistance(), askResistance(), askResistance());
}
double ElectricCalc::getEffect() {
  return askVoltage() * askCurrent();
}
double ElectricCalc::getEffectAverageSingle() {
  return getEffect() * askCosinus();
}
double ElectricCalc::getEffect3Phase() {
  return askVoltageLimit() * askCurrent() * sqrt(3);
}
double ElectricCalc::getEffectAverage3Phase() {
  return getEffect3Phase() * askCosinus();
}

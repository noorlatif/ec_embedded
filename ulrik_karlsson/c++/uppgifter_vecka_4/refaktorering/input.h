//
// Created by ulrik on 2019-11-28.
//

#ifndef REFAKTORRERING_INPUT_H
#define REFAKTORRERING_INPUT_H

#include <iostream>
#include <fstream>
#include <climits>

// This function takes an input from the keyboard and
// throws an exception if input is not a number.
void getNum(double* num);
void getNum(int* num);

// This funktion keeps asking for input until it gets a number.
double getDouble();
int getInt();

// This funktion keeps asking for input until it gets a number.
// It throws an exception if the number is not
// within specified range.
void getNumRange(double *num, double min, double max);

// This funktion keeps asking for input until it gets a number
// within specified range.
double getValueRange(double min, double max);

#endif //REFAKTORRERING_INPUT_H

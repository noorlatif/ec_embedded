//
// Created by ulrik on 2019-11-28.
//

#include "input.h"

// This function takes an input from the keyboard and
// throws an exception if input is not a number.
void getNum(double *num) {
  double input;
  std::cin >> input;
  //    Throw exception if cin fails
  if (std::cin.fail()) {
    std::cin.clear();
    std::cin.ignore(INT_MAX, '\n');
    throw "Du m�ste ange ett nummer!";

  }
  *num = input;
  std::cin.ignore(INT_MAX, '\n');
}
void getNum(int *num) {
  double dbl = *num;
  getNum(&dbl);
  *num = (int) dbl;
}

// This funktion keeps asking for input until it gets a number.
double getDouble(){
  double value;
  while (1){
    try {
      getNum(&value);
    }
    catch (const char *error) {
//    User didn't input a number
      std::cout << error << std::endl;
      continue;
    }
    return value;
  }
}
int getInt() {
  return (int) getDouble();
}

// This funktion keeps asking for input until it gets a number.
// It throws an exception if the number is not
// within specified range.
void getNumRange(double *num, double min, double max) {
  double value = getDouble();
//  Throw exception if num is not within specified range
  if (value < min) {
    throw "F�r l�gt v�rde, f�rs�k igen: \n";
  }
  if (value > max) {
    throw "F�r h�gt v�rde, f�rs�k igen: \n";
  }
  *num = value;
}

// This funktion keeps asking for input until it gets a number
// within specified range.
double getValueRange(double min, double max) {
  double value;
  while (true) {
    try {
      getNumRange(&value, min, max);
    } catch (const char *error) {
      std::cout << error << std::endl;
      continue;
    }
    return value;
  }
}

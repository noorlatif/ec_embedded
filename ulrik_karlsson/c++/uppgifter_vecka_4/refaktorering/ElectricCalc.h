//
// Created by ulrik on 2019-11-29.
//

#ifndef REFAKTORRERING_ELECTRICCALC_H
#define REFAKTORRERING_ELECTRICCALC_H

#include <iostream>
#include <cmath>
#include <cstdio>
#include "input.h"


class ElectricCalc {
private:
    static double res_tot(double r1, double r2, double r3);

// Prints message to screen and
// keeps asking for input til it gets a suitable value.
    static double askResistance();
    static double askCurrent();
    static double askVoltage();
    static double askVoltageLimit();
    static double askCosinus();
public:
    ElectricCalc() = default;

// Prints message to screen and
// keeps asking for input til it gets a suitable value.
// Calculates and returns corresponding value.
    static double getVoltage();
    static double getTotalResistance();
    static double getEffect();
    static double getEffectAverageSingle();
    static double getEffect3Phase();
    static double getEffectAverage3Phase();

};

#endif //REFAKTORRERING_ELECTRICCALC_H

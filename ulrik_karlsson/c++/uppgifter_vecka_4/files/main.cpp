/*
G�r ett program som f�rst skapar en fil med n�gon information ni sj�lva v�ljer.
https://www.w3schools.com/cpp/cpp_files.asp
L�t sedan anv�ndaren av programmet i n�sta steg v�lja vad denne vill g�ra med filen,
t.ex. skriva dit mer text eller visa det som finns.

Trots att ni precis skapat filen kan det bli fel, disken ni sparar p� kanske �r
skrivskyddad. F�rs�k fundera ut alla t�nkbara saker som kan st�lla till problem, t.ex.
filen finns inte eller har blivit skadad.

Skriv kod som �r s� s�ker som m�jligt, dvs s�kra upp input, s�kra upp filhantering,
anv�nd b�de metoderna vi testat med cin som fail och ignore och exception med try
catch f�r filhanteringen. Det r�cker allts� inte med att kolla s� filen finns, den
kan ju vara skadad eller tom.

*/

#include "functions.h"

using namespace std;

int main() {
    const char FILE_NAME[] = "file001.txt";
    int choice;
    char menu[] = "[1] Display content\n"
                  "[2] Add content\n"
                  "[0] Exit";

//    Create and open a text file
    ofstream file(FILE_NAME);

//    Write to the file
    file << "This line is auto generated!\n"
            "This line too.\n";

//    Close the file
    file.close();

    while (true) {
//    Display menu
        cout << menu << endl;
//    Get input
        cout << "Enter a number: ";
        try {
            getNum(&choice);
        } catch (const char *error) {
            cout << error << endl;
            continue;
        }

        switch (choice) {
            case 0:     // Exit
                cout << "Exit\n";
                return 0;
            case 1:     // Display content
                cout << "Display content\n"
                        "****************************************\n\n";
                try {
                readFile(FILE_NAME);
                }catch (const char* error){
                    cout<<error<<endl;
                }
                cout << "\n****************************************\n";
                break;
            case 2:     // Add content
                cout << "Add content\n";
                try {
                    appendFile(FILE_NAME);
                }catch (const char* error){
                    cout<<error<<endl;
                }
                break;
            default:
                cout << "Please enter another number!\n";
                break;
        }
    }
    return 0;
}
//
// Created by ulrik on 2019-11-27.
//

#include "functions.h"

void getNum(int *num) {
    int input;
    std::cin >> input;
//    Throw an error if cin fails
    if (std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(INT_MAX, '\n');
        throw "Input is not a number!";
    }
    *num = input;
    std::cin.ignore(INT_MAX, '\n');
}

void readFile(const char *FILE_NAME) {
//    Create a text string, which is used to output the text file
    std::string line;
//    Open the file, throw an error if it fails
    std::ifstream file(FILE_NAME);
    if (!file) {
        throw "Couldn't read the file!";
    }
//    Read the file line by line and output to console
    while (std::getline(file, line)) {
        std::cout << line << std::endl;
    }
//    Close the file
    file.close();
}

void appendFile(const char *FILE_NAME) {
    std::ofstream file;
//    Open the file, throw an error if it fails
    file.open(FILE_NAME, std::ios::app);
    if (!file) {
        throw "Couldn't open file for reading!";
    }
//    Write to the file
    int c = getchar();
    do {
        file.put(c);
    } while ((c = getchar()) != '\n');
//    Close the file
    file.close();
}



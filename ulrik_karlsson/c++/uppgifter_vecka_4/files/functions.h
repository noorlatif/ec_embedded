//
// Created by ulrik on 2019-11-27.
//

#ifndef FILES_FUNCTIONS_H
#define FILES_FUNCTIONS_H
#include <iostream>
#include <fstream>

void getNum(int* num);
void readFile(const char* FILE_NAME);
void appendFile(const char* FILE_NAME);

#endif //FILES_FUNCTIONS_H

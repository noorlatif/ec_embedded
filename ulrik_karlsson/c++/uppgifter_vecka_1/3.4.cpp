#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
   system("chcp 1252");
   system("cls");

   int isMale;
   long long int num;

   cout << "0 = Kvinna\n1 = Man\nAnge om du �r man eller kvinna: ";
   cin >> isMale;
   cout << "Ange ditt personnummer utan bindestreck: ";
   cin >> num;
   
   // Remove last digits
   num = num / 10;
   // See if the number can be evenly divided by 2
   if (num % 2 == isMale)
   {
      cout << "St�mmer!\n";
   }
   else
   {
      cout << "St�mmer inte!\n";
   }

   return 0;
}
#include <iostream>

using namespace std;

double inkl_vat(double price, double vat)
{
    return price * (1 + vat / 100);
}

int main()
{
   cout << "Ange pris exklusive moms: ";
   double price;
   cin >> price;
   cout << "Ange momssatsen: ";
   double vat;
   cin >> vat;
   cout << "Priset blir " << inkl_vat(price, vat) << " kr inklusive moms.";

   return 0;
}
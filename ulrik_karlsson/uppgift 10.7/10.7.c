#include <stdio.h>
#include <stdlib.h>
#include "mystring.h"
#include "translate.h"

void main()
{
   system("chcp 1252");

   char line[LENGTH];
   int choise;
   int lines;

   while (1)
   {
      lines = 0;

      printf("\n\
Vad vill du g�ra?\n\
1 = �vers�tt vanlig text till r�varspr�k.\n\
2 = �vers�tt r�varspr�k till vanlig text.\n\
3 = Avsluta\n\
V�lj ett alternativ: ");

      scanf("%d", &choise);
      skip_line();
      if (choise == 1)
      {
         // Open the file and make a pointer to it
         FILE *pointerIn = open_file("Vilken fil vill du �vers�tta till r�varspr�k?", "r");

         // Create a file and make a pointer to it
         FILE *pointerOut = open_file("Vilken fil vill du skriva till?", "w");

         // Loop through file.
         // Read one line and store it to line.
         while (fgets(line, LENGTH, pointerIn) != NULL)
         {
            // Translate the line and write it to the file
            translate_to_rovarsprak(line);
            fprintf(pointerOut, translated);
            lines++;
         }
         printf("\n�versatte %d rader.\n", lines);
         fclose(pointerIn);
         fclose(pointerOut);
      }
      else if (choise == 2)
      {
         // Open the file and make a pointer to it
         FILE *pointerIn = open_file("Vilken fil vill du �vers�tta till vanlig text?", "r");

         // Create a file and make a pointer to it
         FILE *pointerOut = open_file("Vilken fil vill du skriva till?", "w");

         // Loop through file.
         // Read one line and store it to line.
         while (fgets(line, LENGTH, pointerIn) != NULL)
         {
            // Translate the line and write it to the file
            translate_from_rovarsprak(line);
            fprintf(pointerOut, translated);
            lines++;
         }
         printf("\n�versatte %d rader.\n", lines);
         fclose(pointerIn);
         fclose(pointerOut);
      }
      else if (choise == 3)
      {
         break;
      }
   }
}
#include <Arduino.h>
#include "Display.h"

// Create new display
Display dsp;

void setup() {
  dsp.setPins(1, 2, 3, 4, 5, 6, 7, 8);
  dsp.commonCatode();
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
}

void loop() {
  // Print some numbers
  dsp.print(3);
  delay(1000);
  dsp.print(5);
  delay(1000);
  dsp.print(7);
  delay(1000);
  
  // Print the dot
  dsp.dot();
  delay(1000);
  
  // Count from 0 to 9
  for (int i = 0; i <= 9; i++) {
    dsp.print(i);
    delay(500);
  }

  // Clear display
  dsp.clear();
  delay(1000);
}

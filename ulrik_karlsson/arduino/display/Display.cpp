#include "Display.h"
// Constructor
Display::Display(int a, int b, int c, int d, int e, int f, int g, int dp) {
  setPins(a, b, c, d, e, f, g, dp);
  init();
}

// Methods
void Display::setPins(int a, int b, int c, int d, int e, int f, int g, int dp) {
  this->a = a;
  this->b = b;
  this->c = c;
  this->d = d;
  this->e = e;
  this->f = f;
  this->g = g;
  this->dp = dp;
}

void Display::init() {
  pin_connections[0] = this->a;
  pin_connections[1] = this->b;
  pin_connections[2] = this->c;
  pin_connections[3] = this->d;
  pin_connections[4] = this->e;
  pin_connections[5] = this->f;
  pin_connections[6] = this->g;
  pin_connections[7] = this->dp;

  // 2d array defining how to print numbers
  //                      A  B  C  D  E  F  G  DP
  int temp_num[12][8] = {{h, h, h, h, h, h, l, l}, // 0 
                         {l, h, h, l, l, l, l, l}, // 1
                         {h, h, l, h, h, l, h, l}, // 2
                         {h, h, h, h, l, l, h, l}, // 3
                         {l, h, h, l, l, h, h, l}, // 4
                         {h, l, h, h, l, h, h, l}, // 5
                         {h, l, h, h, h, h, h, l}, // 6
                         {h, h, h, l, l, l, l, l}, // 7
                         {h, h, h, h, h, h, h, l}, // 8
                         {h, h, h, h, l, h, h, l}, // 9
                         {l, l, l, l, l, l, l, h}, // DP
                         {l, l, l, l, l, l, l, l}};// CLEAR
  //                      A  B  C  D  E  F  G  DP

  // Copy temp_num to numbers
  for (int i = 0; i < 11; i++) {
    for (int j = 0; j < 8; j++) {
      numbers[i][j] = temp_num[i][j];
    }
  }
}

void Display::commonCatode() {
  this->h = 1, this->l = 0;
  init();
}

void Display::commonAnode() {
  this->h = 0, this->l = 1;
  init();
}

void Display::print(int num) {
  // Loop through connected outputs
  for (int i = 0; i < 8; i++) {
    // Switch on or off current output
    digitalWrite(pin_connections[i], numbers[num][i]);
    // num determines what number we want to print
  }
}

void Display::dot(){
  print(10);
}

void Display::clear(){
  print(11);
}
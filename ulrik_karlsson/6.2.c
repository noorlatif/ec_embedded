#include <stdio.h>

double inkl_moms(double price, double vat)
{
    return price * (1 + vat / 100);
}

int main()
{
    printf("Ange pris exklusive moms: ");
    double price;
    scanf("%lf", &price);
    printf("Ange momssatsen: ");
    double vat;
    scanf("%lf", &vat);
    printf("Priset blir %.1f kr inklusive moms.", inkl_moms(price, vat));

    return 0;
}
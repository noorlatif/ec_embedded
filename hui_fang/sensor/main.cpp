#include <iostream>
#include "TempSensor.h"
using namespace std;
int main() {
    TempSensor a1;
    TempSensor a2;

    a2 = {1002, 100, "Rockwell"};

    cout << "Sensor a1 is: " << endl;
    a1.showData();

    cout << "Sensor a2 is: " << endl;
    a2.showData();

    a2.increaseBattery();
    cout << "Now the battery of a2 after increase is: " << endl;
    a2.getBattery();

    a2.decreaseBattery();
    a2.decreaseBattery();
    cout << "Now the battery of a2 after decrease twice is: " << endl;
    a2.getBattery();
    return 0;
}
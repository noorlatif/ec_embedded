//
// Created by Hui Fang on 2019/11/23.
//
#include <string>
#include <iostream>
#ifndef TEMPSENSOR_TEMPSENSOR_H
#define TEMPSENSOR_TEMPSENSOR_H


// Create a class including the characteristics of the sensor.
class TempSensor {
public:
    static int sensor_ID;
private:
    int senID;
    std::string model;
    int batteryInfo;
public:
    TempSensor();   // Default constructor
    TempSensor(int senID, int power, std::string name);

    void setBattery(int power);
    void setID(int idNum);
    std::string getModel();
    int getBattery();
    int increaseBattery();
    int decreaseBattery();
    void showData();
};


#endif //TEMPSENSOR_TEMPSENSOR_H

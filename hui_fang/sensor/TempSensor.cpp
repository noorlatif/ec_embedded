//
// Created by Hui Fang on 2019/11/23.
//

#include "TempSensor.h"
#include <iostream>
#include <string>
using namespace std;

// Create a default constructor
TempSensor::TempSensor(){
    this -> senID = 1001;
    this -> batteryInfo = 0;
    this -> model = "favorite model";

    sensor_ID ++;
}


TempSensor::TempSensor(int senID, int power, std::string name) {
    this -> senID = senID;
    this -> batteryInfo = power;
    this -> model = name;
    sensor_ID ++;
}

int TempSensor::sensor_ID = 0;

void TempSensor::setBattery(int power){
    this -> batteryInfo = power;
}


void TempSensor::setID(int idNum) {
    this ->senID = idNum;
}


std::string TempSensor::getModel() {
    return this -> model;
}

int TempSensor::getBattery() {
    return batteryInfo;
}

int TempSensor::increaseBattery(){
    return this -> batteryInfo += 2;
}

int TempSensor::decreaseBattery(){
    if (this -> batteryInfo <= 0)
        return this -> batteryInfo = 0;
    else
        return this -> batteryInfo -= 2;
}

void TempSensor::showData(){
    cout << "Sensor ID: " << senID << "\tBattery: " << batteryInfo << "%"<< endl;
    cout << "Sensor Model: " << model << "\tTotal object number: " << sensor_ID << endl;
}
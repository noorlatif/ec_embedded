/* This script is designed to print the converted SOS
 * morse pattern all the time.
 */
 
#include <stdio.h>

int main() {
    
    char sos_to_morse[]=". . . _ _ _ . . .";   
    
    
    while(1){
        printf("%s\n", sos_to_morse);    // Print out the morse
    }

    return 0;
}

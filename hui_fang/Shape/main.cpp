#include <iostream>
#include "Rectangle.h"
#include "Sphere.h"

int main() {
    Rectangle box1(2,2,2, "box");
    Sphere globe1(2, "ball");
    float volBox = box1.calVol();
    float areaBox = box1.calArea();
    float volGlobe = globe1.calVol();
    float areaGlobe = globe1.calArea();
    std::cout << "The volume of the " + box1.name << "is: " << volBox << std::endl;
    std::cout << "The area of the " + box1.name << "is: " << areaBox << std::endl;
    std::cout << "The volume of the " + globe1.name << "is: " << volGlobe << std::endl;
    std::cout << "The area of the " + globe1.name << "is: " << areaGlobe << std::endl;
    return 0;
}
#include <fstream>
#include <iostream>
using namespace std;

int main () {
   int age;

   // open a file in write mode.
   ofstream myfile;
   myfile.open("myfile.data");

   cout << "Writing to the file" << endl;

   cout << "Enter your age: ";
   cin >> age;
    // clear and ignore if the user enter wrong information.
   while(cin.fail())
    {
    cin.clear();
    cin.ignore();
    cout << "you can only enter numbers \n";
   }

   // again write inputted data into the file.
   myfile << age << endl;

   // close the opened file.
   myfile.close();

   // open a file in read mode.
   ifstream readfile;
   readfile.open("myfile.data");

   cout << "Reading from the file" << endl;
   readfile >> age;

   // show the data at the screen.
   cout << age << endl;

   // close the opened file.
   readfile.close();

   return 0;
}

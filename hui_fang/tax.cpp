#include <iostream>
#define TAXRATE 0.25

using namespace std;

int
main ()
{
  float costbeforetax, costaftertax;
  cout << "Please enter the cost before tax: ";
  cin >> costbeforetax;
  costaftertax = costbeforetax + costbeforetax * TAXRATE;
  cout << "The cost after tax is: " << costaftertax << endl;
  return 0;
}
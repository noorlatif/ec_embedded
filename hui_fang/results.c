#include <stdio.h>

int
main ()
{
  int results[2][10] =
    { {20, 21, 21, 22, 22, 23, 23, 24, 24, 24}, {19, 19, 20, 20, 20, 21, 21,
						 22, 23, 24} };
  int i, j;
  printf ("results: \n");
  for (i = 0; i < 2; i++)
    {
      for (j = 0; j < 10; j++)
	{
	  printf ("%d ", results[i][j]);

	}
      printf ("\n");

    }

  return 0;
}

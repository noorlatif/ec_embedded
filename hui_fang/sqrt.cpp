/* File: sqrt
* Author: Hui Fang
* Time: 2019.11.25
* Description: This function is to get square root of a number larger than 0.
*/
#include<iostream>
#include<cmath>
using namespace std;

int main()
{
    double a, s;
    cout << "Please enter any number lager than 0" << endl;
    cin >> a;
    if (a>0)
	{
       s = sqrt(a);
       cout << "The square root of " << a << " is " << s << endl;}
    else
       cout << "This is not a valid number, please try again!";
}

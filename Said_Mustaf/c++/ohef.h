//flytat här deklarations fil(h-filen)

class Lagen {         // klass namn
  public:          	// åtkommistspecifakation
    double spn;  	// Attribute
    double stm;  	// Attribute(data klass)
    double rst;  	// Attribute(data klass)
    double eff;		// Attribute(data klass)
    double spng;	// Attribute(data klass)
    double strm;	// Attribute(data klass)

    Lagen(double u, double i, double r, double p, double v, double a) ;
};
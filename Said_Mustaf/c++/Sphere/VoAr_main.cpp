#include"VoAr.h"
#define _USE_MATH_DEFINES
#include <cmath>

//main fuction with definitions.

   void sphere::setArea(double r) {
      Area = 4 * M_PI * r * r ;

    }

    
   double sphere::getArea() {
     return Area;
    }


    
  void sphere::setVolume(double r) {
      Volume = 4* M_PI * r * r * r /3;

    }

    
  double sphere::getVolume() {
     return Volume;
    }

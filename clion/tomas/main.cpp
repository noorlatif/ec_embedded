#include <iostream>
#include <list>
#include <string>

using namespace std;

class Car
{
public:
    Car() = default;
    Car(string b, string m)
    {
        brand = b;
        model = m;
    }
private:
    string brand;
    string model;
public:
    void print()
    {
        cout << brand << " " << model << ".\n";
    }
    void setInfo(string br, string mo)
    {
        this->brand = br;
        this->model = mo;
    }
};

int main()
{
    list <Car> mylist;

    Car car1("Mercedes", "E63s AMG 4MATIC+");
    Car car2("Volvo", "V60 T8");
    Car car3("Audi", "RS6");
    Car car4("BMW", "M4");

    mylist.push_back(car1);
    mylist.push_back(car2);
    mylist.push_back(car3);
    mylist.push_back(car4);

    //int s = mylist.max_size();

    //cout << s << endl;

    for (Car obj : mylist)
    {
        obj.print();
    }

    cout << "Ange en siffra: "; //siffra för plats i listan
    int choice;
    cin >> choice;
    int i = 0;  //räknare för plats i listan
    for (Car obj : mylist)
    {
        if (choice == i) //om user input == räknare
        {
            cout << "Enter brand: ";
            string newBrand;
            cin >> newBrand;
            cout << "Enter model: ";
            string newModel;
            cin >> newModel;
            obj.setInfo(newBrand, newModel);
            mylist.push_back(obj);
        }      //Funktion som ändrar variabler i objektet
        i++;
    }

    for (Car obj : mylist)
    {
        obj.print();
    }

    return 0;
}

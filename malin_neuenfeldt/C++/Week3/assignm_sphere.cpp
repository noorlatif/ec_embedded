/* File: assignm_sphere.cpp
 * Summary: Program that calculate volume and area for sphere
 * Version: 1.1
 * Owner: Malin Neuenfelt
 * Date: 2019-11-18
 * TEST!!
 * -------------------------------*/

#include <iostream>
#include <cmath>
using namespace std;


class Sphere {
private:
    int radius;

public:
    Sphere() {}

    Sphere (int radius) {
        if (radius < 0) {
            radius = 0;
        }
        this->radius = radius;
    }
    void setRadius (int radius) {
        if (radius < 0) {
            radius = 0;
        }
        this->radius = radius;
    }
    double calculateVolume();
    double calculateArea();
};
double Sphere::calculateVolume() {
    double volume;
    volume = 4 * M_PI * pow(this->radius, 3) / 3;

    return volume;
}
double Sphere::calculateArea() {
    double area;

    area = 4 * M_PI * pow(this->radius, 2);

    return area;
}
int main() {

    Sphere newSphere, newSphere2;

    newSphere.setRadius(2);
    double volume = newSphere.calculateVolume();
    newSphere2.setRadius(2);
    double area = newSphere2.calculateArea();
    cout << "Volume is " << volume << " area is " << area << endl;

    newSphere.setRadius(-2);
    double volume2 = newSphere.calculateVolume();
    newSphere2.setRadius(-2);
    double area2 = newSphere2.calculateArea();
    cout << "Volume is " << volume2 << " area is " << area2 << endl;

    return 0;
}
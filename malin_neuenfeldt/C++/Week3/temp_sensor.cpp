/* File: temp_sensor.cpp
 * Summary: Program that creates a number of temp sensor objects
 * and give each object id number, battery consumption and model.
 * Version: 1.1
 * Owner: Malin Neuenfelt
 * Date: 2019-11-22
 */
#include <iostream>
using namespace std;

//Class for temp sensors
class TempSensor {
private:
    int id;
    int batteryConsumption;
    string model;

public:
    //Constructor
    TempSensor () {
        static int idNumber = 1001;
        this->id = idNumber;
        this->model = "My favorite model";
        this->batteryConsumption = 0;
        idNumber++;
    }
    //Method that read sensor model
    void setModel (string modelName) {
        this->model = modelName;
    }
    //Method that return battery consumption
    int getBatteryConsumption (int battery) {
        return this->batteryConsumption;
    }
    //Method that increase battery consumption
    void increaseBatteryConsumption () {
        this->batteryConsumption += 2;
    }
    //Method that decrease battery consumption, but not less than zero
    void decreaseBatteryConsumption () {
        if (batteryConsumption < 2) {
            this->batteryConsumption = 0;
        }
        else {
            this->batteryConsumption -= 2;
        }
    }
    //Method that print out temp sensor data
    void printOutData () {
        cout << "Temp sensor id: " << this->id << "\nModel: " << this->model <<
             "\nBattery consumption: " << this->batteryConsumption << endl << endl;
    }
};
int main()
{
    //Create objects
    TempSensor t1;
    TempSensor t2;

    //Print out 2 temp sensor data
    t1.printOutData();
    t2.printOutData();

    //Change battery consumption on temp sensor 1
    t1.increaseBatteryConsumption();
    t1.printOutData(); //Print out change
    t1.decreaseBatteryConsumption();
    t1.decreaseBatteryConsumption();
    t1.printOutData(); //Print out change

    return 0;
}

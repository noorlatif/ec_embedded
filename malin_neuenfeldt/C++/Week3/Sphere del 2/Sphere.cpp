//
// Created by malin.neuenfeldt on 2019-11-24.
//
#include "Sphere.h"
#include <cmath>

//Methods that calculate sphere volume and area
double Sphere::calculateVolume() {
    double volume;
    volume = 4 * M_PI * pow(this->radius, 3) / 3;

    return volume;
}
double Sphere::calculateArea() {
    double area;
    area = 4 * M_PI * pow(this->radius, 2);

    return area;
}
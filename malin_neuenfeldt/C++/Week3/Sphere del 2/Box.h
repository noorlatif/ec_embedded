//
// Created by malin.neuenfeldt on 2019-11-24.
//
#ifndef ASSIGNM_SPHERE_BOX_H
#define ASSIGNM_SPHERE_BOX_H

#include "Shape.h"

class Box : public Shape {
public:
    //Declare variables
    double width;
    double height;
    double length;

    //Constructor declaration
    Box () = default;
    Box (double height, double length, double width, std::string name);

    //Method declaration
    double calculateVolume ();
    double calculateArea ();
};

#endif //ASSIGNM_SPHERE_BOX_H
//
// Created by malin.neuenfeldt on 2019-11-24.
//

#ifndef ASSIGNM_SPHERE_SHAPE_H
#define ASSIGNM_SPHERE_SHAPE_H

#include <string>
using namespace std;

class Shape {
public:
    string name;
    string color;
    Shape () = default;
    double getArea () {
        return 0;
    }
    double getVolume () {
        return 0;
    }

};

#endif //ASSIGNM_SPHERE_SHAPE_H
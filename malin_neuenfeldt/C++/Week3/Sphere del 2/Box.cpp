//
// Created by malin.neuenfeldt on 2019-11-24.
//
#include "Box.h"
#include <string>

//Box constructor
Box::Box (double height, double length, double width, std::string name) {
    this->height = height;
    this->length = length;
    this->width = width;
    this->name = name;
}
//Methods to calculate box volume and area
double Box::calculateVolume() {
    return this->width * this->length * this->height;
}
double Box::calculateArea() {
    return 2 * (this->width * this->length + this->height * this-> length + this->width * this->height);
}

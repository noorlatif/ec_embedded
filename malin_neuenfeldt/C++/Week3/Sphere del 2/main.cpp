/* File: assignm_sphere.cpp
 * Summary: Program that calculate volume and area for sphere and box shapes
 * Version: 1.1
 * Owner: Malin Neuenfelt
 * Date: 2019-11-24
 * -------------------------------*/

#include <iostream>
#include "Box.h"
#include "Sphere.h"
using namespace std;

int main () {
    Box box (2, 2, 2, "box");
    double boxVolume = box.calculateVolume();
    double boxArea = box.calculateArea();
    cout << "Box volume: " << boxVolume << "\nBox area: "
        << boxArea << endl;

    Sphere sphere;

    sphere.setRadius(2);
    double sphereVolume = sphere.calculateVolume();
    sphere.setRadius(2);
    double sphereArea = sphere.calculateArea();
    cout << "Sphere volume: " << sphereVolume << "\n Sphere area: "
        << sphereArea << endl;

    return 0;
}
#include <iostream>
#include <list>
using namespace std;

//Base class
class Animals {
public:
    void animalSound () {
        cout << "Animal makes a sound." << endl;
    }
};

//Derived classes
class Cat : public Animals {
public:
    void animalSound () {
        cout << "The cat says meow meow!" << endl;
    }
};
class Dog : public Animals {
public:
    void animalSound () {
        cout << "The dog says bow wow!" << endl;
    }
};
class Mouse : public Animals {
public:
    void animalSound () {
        cout << "The mouse says squeek squeek!" << endl;
    }
};

int main() {
    //Create animal list
    list <Animals> animalList;

    //Declare objects
    Animals animal;
    Cat cat;
    Dog dog;
    Mouse mouse;

    animalList.push_front(cat); //Add cat first on list
    animalList.push_front(dog); //Add dog first on list
    animalList.push_back(mouse); //Add mouse at the end of list

    //Call methods
    animal.animalSound();
    cat.animalSound();
    dog.animalSound();
    mouse.animalSound();

    //Print out how many objects there are on list
    cout << "There are " << animalList.size() << " animals on list." << endl;

    return 0;
}
/* progamming assignment 3.4 by Malin Neuenfeldt 2019-11-06
Program ask user to print in gender and social security number.
The program check if the social security number match with gender.
*/

#include <iostream>
using namespace std;

int main ()
{

	int gender;
	long long int number;
	long long int socialSecurityNumber;

	//Print out question for which gender, save value in variable gender
	cout << "  " << endl;
	cout << "Enter your gender. Enter 0 for male and 1 for female: ";
	cin >> gender;

	//Print question to fill out social secutity number, save value in variable socialSecurityNumber
	cout << "  " << endl;
	cout << "Enter your social security number (only numbers): ";
	cin >> socialSecurityNumber;

	//Divide social security number with 10 to remove last digit
	number = socialSecurityNumber / 10;

	//Controls social security number and gender to see if it's correct
	//Prints out if it's correct or not
	if (gender == 0)
	{
		if (number % 2 == 1)
			cout << "Your social security number are correkt!";
		else
			cout << "Your social security number are NOT correkt!";
	}
	else if (gender == 1)
	{
		if (number % 2 != 1)
			cout << "Your social security number are correkt!";
		else
			cout << "Your social security number are NOT correkt!";
	}

}
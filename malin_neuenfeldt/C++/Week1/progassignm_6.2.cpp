#include <iostream>
using namespace std;

//Function that add tax to price
double price (double cost) {
	return (cost*1.25);
}

int main () {

	//Ask user to print price without tax
	double cost;
	cout << "Write price excluding sales tax: ";
	cin >> cost;

	//Print out price including tax and also tax amount
	cout << "Price including sales tax: " << price(cost) << "kr" << endl;
	cout << "Value added tax is: " << price(cost*0.20) << "kr (25%)" << endl;

	return 0;
}
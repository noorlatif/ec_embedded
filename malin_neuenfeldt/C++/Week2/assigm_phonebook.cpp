/* File: assignm_phonebook.cpp
* Summary: Program that ask user to enter 3 names and 3 phone numbers.
* Version: 1.1
* Owner: Malin Neuenfelt
* Date: 2019-11-17 */

#include <iostream>
#include <string>
using namespace std;

class phoneBook
{
public:
    string name;
    string phoneNumber;

    void setname(string name2) {
        name = name2;
    }
    void setPhoneNumber(string number) //member function to set data
    { phoneNumber = number; }

    void showdata() //member function to display data
    { cout << "\nName: " << name << "\nPhone number: " << phoneNumber << endl; }
};

int main()
{

    phoneBook p1, p2, p3;

    //Declared variables
    string name1, name2, name3;
    string number1, number2, number3;

    //Ask user to enter name 1
    cout << "Enter name 1: ";
    getline(cin, name1);
    p1.setname(name1);

    //Ask user to enter phone number 1
    cout << "Enter number 1: ";
    getline(cin, number1);
    p1.setPhoneNumber(number1);

    //Ask user to enter name 1
    cout << "Enter name 2: ";
    getline(cin, name2);
    p2.setname(name2);

    //Ask user to enter phone number 1
    cout << "Enter number 2: ";
    getline(cin, number2);
    p2.setPhoneNumber(number2);

    //Ask user to enter name 1
    cout << "Enter name 3: ";
    getline(cin, name3);
    p3.setname(name3);

    //Ask user to enter phone number 1
    cout << "Enter number 3: ";
    getline(cin, number3);
    p3.setPhoneNumber(number3);

    cout << "\nPhone book:";
    //Print out data that has been collected
    p1.showdata();
    p2.showdata();
    p3.showdata();

    return 0;
}
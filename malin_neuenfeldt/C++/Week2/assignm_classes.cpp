/* File: assignm_phonebook.cpp
* Summary: Program with 2 house objects
* Version: 1.1
* Owner: Malin Neuenfelt
* Date: 2019-11-17 */

#include <iostream>
#include <string>
using namespace std;

//New class house
class House {
public:
    string name;
    string color;
    int rooms;
    int floors;

    //House constructor for name, color, rooms and floors.
    House (string aName, string aColor, int aRooms, int aFloors) {
        name = aName;
        color = aColor;
        rooms = aRooms;
        floors = aFloors;
    }
    //Method that set namne
    void setName(string nameA) {
        name = nameA;
    }
    //Method that set color
    void setColor(string colorA) {
        color = colorA;
    }
    //Methos that set number of rooms
    void setRooms(int roomsA) {
        rooms = roomsA;
    }
    //Method that set number of floors
    void setFloors(int floorsA) {
        floors = floorsA;
    }
    //Method that print out data
    void showdata() {
        cout << "House: " << name << "\nColor: " << color << "\nrooms: "
            << rooms << "\nFloors: " << floors << endl << endl;
    }
};
//Main function that runs program
int main() {
    //Define house attributes
    House h1 ("Bettan", "Black", 5, 2);
    House h2 ("Margareta", "Red", 2, 1);

    //Call method that print out data
    h1.showdata();
    h2.showdata();

    return 0;
}
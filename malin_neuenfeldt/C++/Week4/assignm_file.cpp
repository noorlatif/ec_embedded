#include <iostream>
#include <fstream>
#include <string>
using namespace std;


int main() {

    //Create and open text file
    ofstream myFile ("test.txt");

    //Write to file
    myFile << "This is the first text row in file.\n";
    string userInput;

    //Close file
    myFile.close();

    //Declare variable
    int menuOption;
    char again;
    string myText;

    //Print out instructions for user
    cout << "Do you wish to read or write to file?" << endl << endl;

    do {
        cout << "Menu:\n1: Read file\n2: Write to file" << endl;
        cout << "Enter number of option: ";
        cin >> menuOption;

        //Menu to read or write file
        switch (menuOption) {
            case 1: {
                try {
                    //Read from text file
                    ifstream myFile("test.txt");

                    if (myFile) {
                        cout << "\nifs is good\n";
                    }
                    else {
                        cout << "\nifs is bad - deal with it\n";
                        throw "File error";
                    }
                    //Loop that reads text file line by line
                    while (getline (myFile, myText)) {
                        cout << myText << endl; //Print out text in file
                    }
                    myFile.close();
                } catch (...) {
                    cout << "There was an error!" << endl;
                }
                break;
            }
            case 2: {
                cin.ignore(); //Clear buffer before taking new line

                try {
                    //Open file in write mode
                    ofstream myFile ("test.txt", ofstream::app);

                    if (myFile) {
                        cout << "\nifs is good\n";
                    }
                    else {
                        cout << "\nifs is bad - deal with it\n";
                        throw "File error";
                    }
                    //Ask user to enter text string
                    cout << "Enter text to file: ";
                    getline(cin, userInput);
                    myFile << userInput << endl;

                    myFile.close();

                } catch (...) {
                    cout << "There was an error" << endl;
                }
                break;
            }
            default:
                cout << "You can only enter number 1-2, try again!" << endl;
        }
    }
    while (again == 'Y' || again == 'y');

    cout << "\nGood bye!";

    return 0;
}
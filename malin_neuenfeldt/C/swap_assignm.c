/*Swap assignment by Malin Neuenfeldt 2019-10-24
Program print out values of two int variables.
Then a swap function switch the two variable values and
then the program prints out the switched values.*/

#include <stdio.h>

//Function that swap two int variables
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

//Main function that runs program
int main() {

    //Declare int variables
    int x = 8;
    int y = 13;

    //Print out variable values
    printf("Variable X is: %d\nVariable Y is: %d\n", x, y);
    printf("\n");
    swap(&x, &y); //Call on swap function
    //Print out values after they've been switched
    printf("Now we have used our swap function.\n");
    printf("Variable X is now: %d\nAnd variable Y is: %d", x, y);

    return 0;
}
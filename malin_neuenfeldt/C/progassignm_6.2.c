#include <stdio.h>


//Function that add tax to price
double price (double cost) {
	return (cost*1.25);
}

int main () {

	//Ask user to print price without tax
	double cost;
	printf("Write price excluding sales tax: ");
	scanf("%lf", &cost);

	//Print out price including tax and also tax amount
	printf("Price including sales tax: %.2f kr\n", price(cost));
	printf("Value added tax is: %.2f kr (25%%)", price(cost*0.20));

	return 0;
}
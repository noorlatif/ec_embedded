/* prog4_2.c

   jacek

   Write a program that list numbers 1 to 12 on every line and it's power and qubic.

*/

#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>

int main(int argc, char* argv[argc+1]) {

  printf("table with \tnumbers, \tpowers and \tqubics.\n");

  for (int i = 1; i < 13; i++) {
    printf("\t\t%d\t\t%.0f\t\t%.0f\n", i, pow(i,2.0), pow(i,3.0));
  }
  return EXIT_SUCCESS;
}

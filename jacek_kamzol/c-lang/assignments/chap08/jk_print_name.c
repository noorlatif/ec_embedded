/*  jk_print_name.c

    jacek

    to print my name in all the ways its posible in c

*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[argc+1]) {

  // print name with printf
  printf("jacek.\n");
  char j = 'j';
  char a = 'a';
  char c = 'c';
  char e = 'e';
  char k = 'k';
  printf("%c%c%c%c%c.\n", j, a, c, e, k);
  char name[6] = "jacek";
  printf("%s.\n", name);

  return EXIT_SUCCESS;
}

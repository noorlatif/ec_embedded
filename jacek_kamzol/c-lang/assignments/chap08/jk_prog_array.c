/*  jk-prog-arrays.c

    jacek

    given test results, max 10 per array, for two or three classes, calculate the avarage and median for the results.

*/

#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>

double avarage_calc(int given_array[][2], int given_index);



int main(int argc, char* argv[argc+1]) {

  int results[10][2] = {{ 45, 56},
                        { 24, 45},
                        { 23, 68},
                        { 56, 24},
                        { 38, 39},
                        { 45, 54},
                        { 37, 41},
                        { 28, 31},
                        { 65, 60},
                        { 21, 22}};

  double class_1_avarage = 0.0;
  double class_2_avarage = 0.0;


  class_1_avarage = avarage_calc(results, 0);
  class_2_avarage = avarage_calc(results, 1);

  printf("the avarage for the first class is: %.2lf.\n", class_1_avarage);
  printf("the avarage for the second class is: %.2lf.\n", class_2_avarage);


  return EXIT_SUCCESS;
}


double avarage_calc(int given_array[][2], int given_index) {

  double temp = 0.0;
  double avarage = 0.0;

  for (int i = 0; i < 10 ; i++) {
    temp = temp + given_array[i][given_index];
  }
  avarage = temp/10;
  return avarage;
}

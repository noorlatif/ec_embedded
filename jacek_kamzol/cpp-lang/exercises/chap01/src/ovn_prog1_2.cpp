/*  ovn_prog1_2.cpp

    jacek

    use puts from cstdio lib

*/

#include <iostream>
#include <cstdio>
#include <cstdlib>

int main() {

  std::cout << "the name of the user " << std::getenv("USERNAME")
            << std::endl
            << "the name of the host " << std::getenv("HOSTNAME")
            << std::endl;

  //  printf("running on %s\n", getenv("USER"));

  return 0;
}

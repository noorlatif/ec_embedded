/* prog12_1.cpp

   jacek

   define a struct book. initialize the struct with atleast two books from your collection.

*/

#include <iostream>
#include <string>

// create a struct describing my book collection
typedef struct {
  std::string book_name;
  std::string book_autor;
  int pages_count;
  double price;
} Books;

/* the main function */
int main(void) {

  // declare the my_book struct with direct value insertion
  Books my_books = {"Just going home", "Jim Khan", 234, 45.59};
  // declare the my_book2 struct with indirect value passing
  Books my_books2;
  my_books2.book_name = "Home";
  my_books2.book_autor = "Jenifer Halm";
  my_books2.pages_count = 456;
  my_books2.price = 34.65;

  // write out the collections
  std::cout << "first book: " <<  my_books.book_name
            << " , author " << my_books.book_autor
            << " , pages " << my_books.pages_count
            << " , price " << "$" << my_books.price << std::endl;

  std::cout << "second book: " <<  my_books2.book_name
            << " , author " << my_books2.book_autor
            << " , pages " << my_books2.pages_count
            << " , price " << "$" << my_books2.price << std::endl;

  return 0;
}

/*  second_oop.cpp

    jacek

    second program with oop. put methods in class.

*/

#include <iostream>
#include <string>

class my_little_class {
private:
public:
  std::string name;
  std::string place;
  int age;
  double number;
  void setName(std::string the_name)  {  // member function to set data
    name = the_name;
  }
  void setPlace(std::string the_place) {
    place = the_place;
  }
  void setAge(int the_age) {
    age = the_age;
  }
  void setNumber(double the_number) {
    number = the_number;
  }
  void showData(void) {
    std::cout << "name is " << name
              << " place is " << place
              << " age is " << age
              << " number is " << number << std::endl;
  }
};

int main(void) {

  my_little_class  just1, just2;
  just1.setName("justin");
  just1.setPlace("there");
  just1.setAge(234);
  just1.setNumber(345.567);
  just1.showData();

  just2.name = "bobby";
  just2.place = "here";
  just2.age = 456;
  just2.number = 56.65;
  std::cout << just2.name << " " << just2.place << std::endl;

  return 0;
}

/*  first_oop.cpp

    jacek

    first program with oop.

*/

#include <iostream>
#include <string>

class my_little_class {
private:
public:
  std::string name;
  std::string place;
  int age;
  double number;
};

int main(void) {

  my_little_class  just1, just2;
  just1.name = "bobby";
  just1.place = "here";
  just1.age = 456;
  just1.number = 56.65;
  std::cout << just1.name << " " << just1.place << std::endl;

  return 0;
}

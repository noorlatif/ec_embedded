//
// Created by Jacek Kamzol on 2019-11-26.
//

#include <cmath>
#include "TheMathClass.h"

TheMathClass::TheMathClass() {}

TheMathClass::~TheMathClass() {}

double TheMathClass::calcThePower(double anyNumber) {
    return pow(anyNumber, 2.0);
}

double TheMathClass::calcTheSquareRoot(double d) {
    return sqrt(d);
}

double TheMathClass::calcTheCubicRoot(double d) {
    return cbrt(d);
}

double TheMathClass::calcHypotenus(double d, double d1) {
    return hypot(d, d1);
}

double TheMathClass::calcFMax(double d, double d1) {
    return fmax(d, d1);
}



/*
 * file:    cmath-assign.cpp
 * author:  jacek
 * description
 *  simple program to test more functions from the cmath library
 */

#include <iostream>
#include <cmath>
#include "TheMathClass.h"

int main() {

    TheMathClass my_math_functions;
    // a heather
    std::cout << std::endl << "cmath library test" << std::endl;
    std::cout << "******************" << std::endl << std::endl;

    double firstNumber = 0, secondNumber = 0;
    double answareNumber = 0;
    // double* ptr_firstNumber = &firstNumber;

    // ask the user for two numbers
    std::cout << "give me first number:  ";
    std::cin >> firstNumber;
    std::cout << "give me second number: ";
    std::cin >> secondNumber;

    // the powers
    answareNumber = my_math_functions.calcThePower(firstNumber);
    std::cout << "the power of the first number is: " << answareNumber << std::endl;
    answareNumber = my_math_functions.calcThePower(secondNumber);
    std::cout << "the power of the second number is: " << answareNumber << std::endl;

    // the sqare roots
    answareNumber = my_math_functions.calcTheSquareRoot(firstNumber);
    std::cout <<"the sqr root of the first number is: " << answareNumber << std::endl;
    answareNumber = my_math_functions.calcTheSquareRoot(secondNumber);
    std::cout <<"the sqr root of the first number is: " << answareNumber << std::endl;

    // the cubic roots
    answareNumber = my_math_functions.calcTheCubicRoot(firstNumber);
    std::cout <<"the cubic sqr root of the first number is: " << answareNumber << std::endl;
    answareNumber = my_math_functions.calcTheCubicRoot(secondNumber);
    std::cout <<"the cubic sqr root of the second number is: " << answareNumber << std::endl;

    // the hypotenus
    answareNumber = my_math_functions.calcHypotenus(firstNumber, secondNumber);
    std::cout <<"the hypotenus of a right triangle is: " << answareNumber << std::endl;

    // the fmax
    answareNumber = my_math_functions.calcFMax(firstNumber, secondNumber);
    std::cout <<"the maximum number is: " << answareNumber << std::endl;



    return 0;
}
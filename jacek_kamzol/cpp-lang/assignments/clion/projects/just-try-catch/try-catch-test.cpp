#include <iostream>

double division(int var1, int var2) {
    if (var2 == 0) {
        throw "you just made a doodoo, division by zero!";
    }
    return (var1/var2);
}

int main() {
    int a = 30;
    int b = 0;
    double d = 0;

    try {
        d = division(a,b);
        std::cout << d << std::endl;
    }
    catch(const char* error) {
        std::cout << error << std::endl;
    }
    // std::cout << "Hello, World!" << std::endl;
    return 0;
}
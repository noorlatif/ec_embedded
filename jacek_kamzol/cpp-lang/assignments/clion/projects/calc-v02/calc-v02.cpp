/*
 * file:   calc-v02.cpp
 * author: jacek
 * description:
 *  calculator
 */

#include <iostream>

int main() {
    std::cout << "my second calculator" << std::endl;
    std::cout << "********************" << std::endl << std::endl;

    std::cout << "the idea is to test the calculator with classes." << std::endl;
    return 0;
}
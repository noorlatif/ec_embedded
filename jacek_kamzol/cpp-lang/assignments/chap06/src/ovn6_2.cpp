/*  ovn6_2.cpp

    jacek

    create function nfact to calculate factorials of numbers (n!). read number from cin.

*/

#include <iostream>

// nfact function prototype
double nfact(int a);

int main(void) {

  int n; // declaration of the factor operand
  std::cout << "give me a number. i give you a factorial: ";
  std::cin >> n;

  // call nfact and get back an double
  double the_fact = nfact(n);
  std::cout << "the fact of " << n << " is " << the_fact << std::endl;

  return 0;
}

// function nfact declaration
double nfact(int a) {
  // declare the value to return
  double to_return;
  if (a==0) { // if n = 0, return 1
    to_return = 1;
   } else {
    to_return = 1.0;
    while (1<a) {
      to_return = to_return * a--;
    }
  }
  return to_return;
}

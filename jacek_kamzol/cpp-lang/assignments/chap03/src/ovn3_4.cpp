/*  ovn3_4.cpp

    jacek

    tha swedish post office have rules about the minimum and maximum sizes of envelopes for mail.
    read in the lenght (length), width (width) and thicknes (thick).

*/

#include <iostream>
// #include <stdio.h>

// #include <stdlib.h>

int main(void) {

  // declaration of variables
  int length{ };
  int width{ };
  int thick{ };

  std::cout << "give me the length: ";
  std::cin >> length;

  std::cout << "give me the width: ";
  std::cin >> width;

  std::cout << "give me the thicknes: ";
  std::cin >> thick;

  if ((length >= 140 && width >= 90) && (length <= 600 && thick <= 100 && ((width + length + thick) <= 900 ) )) {
    std::cout << "rigth size!" << std::endl;
  } else {
    std::cout << "wrong size!!" << std::endl;
  }

  return 0;
}
